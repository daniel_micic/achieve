package com.judge.achieve.utils;

import android.content.SharedPreferences;

import com.judge.achieve.AppController;
import com.judge.achieve.common.C;

/**
 * Created by Daniel on 02.08.2016.
 */
public class PrefUtils {

    private final static String keyword = "com.judge.achieve";

    static SharedPreferences sharedpreferences = AppController.getContext().getSharedPreferences(keyword, AppController.MODE_PRIVATE);

    public static void saveString(String key, String value) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveBool(String key, Boolean value) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void saveInt(String key, Integer value) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void saveLong(String key, Long value) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }


    public static String getString(String key) {
        return sharedpreferences.getString(key, "");
    }

    public static Boolean getBool(String key) {
        return sharedpreferences.getBoolean(key, false);
    }

    public static Integer getInt(String key) {
        return sharedpreferences.getInt(key, -1);
    }

    public static Long getLong(String key) {
        return sharedpreferences.getLong(key, -1);
    }

    public static Boolean isFirstRun() {
        return sharedpreferences.getBoolean(C.PREF_FIRST_RUN, true);
    }


}

