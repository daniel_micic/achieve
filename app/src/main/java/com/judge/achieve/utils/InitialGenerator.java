package com.judge.achieve.utils;

import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.common.enums.Recurrence;
import com.judge.achieve.common.enums.Scope;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.ExerciseHistory;
import com.judge.achieve.model.Goal;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.model.Profile;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.model.AttributeDatabase;
import com.judge.achieve.persistence.model.ExerciseDatabase;
import com.judge.achieve.persistence.model.ExerciseHistoryDatabase;
import com.judge.achieve.persistence.model.GoalDatabase;
import com.judge.achieve.persistence.model.PlannerEntryDatabase;
import com.judge.achieve.persistence.model.PlannerEntryIdsDatabase;
import com.judge.achieve.persistence.model.ProfileDatabase;
import com.judge.achieve.persistence.model.SkillDatabase;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.GoalQuery;
import com.judge.achieve.persistence.query.PlannerEntryQuery;
import com.judge.achieve.persistence.query.ProfileQuery;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Daniel on 23.07.2016.
 */
public class InitialGenerator {

    static int attributeId;
    static int exhId;

    public static Profile generateProfile(Realm realm) {
        realm.executeTransaction(realm1 -> {
            realm.delete(ProfileDatabase.class);
            realm.delete(AttributeDatabase.class);
            realm.delete(SkillDatabase.class);
            realm.delete(ExerciseDatabase.class);
            realm.delete(ExerciseHistoryDatabase.class);
            realm.delete(GoalDatabase.class);
            realm.delete(PlannerEntryDatabase.class);
            realm.delete(PlannerEntryIdsDatabase.class);
        });

        Profile profile = new Profile();
        profile.setName("Unsung Hero");
        profile.setLevel(1);
        profile.setPoints(0);

        ProfileQuery.copyToRealm(realm, profile);





        /*profile.setCategories(new ArrayList<Attribute>());

        profile.getCategories().add(generatePhysical());
        profile.getCategories().add(generateIntelligence());
        profile.getCategories().add(generateSpirit());*/

        return profile;
    }

    public static void generateExample(Realm realm) {
        attributeId = -1;
        exhId = 0;

        AttributeQuery.copyToRealm(realm, generatePhysical(realm, false));
        AttributeQuery.copyToRealm(realm, generateIntelligence());
        AttributeQuery.copyToRealm(realm, generateSpirit());

        GoalQuery.copyToRealm(realm, generateGoals());
    }

    public static void generateTemplate(Realm realm) {
        attributeId = -1;
        exhId = 0;

        AttributeQuery.copyToRealm(realm, generateAttribute1(realm, false));
        AttributeQuery.copyToRealm(realm, generateAttribute2());
        AttributeQuery.copyToRealm(realm, generateAttribute3());

        GoalQuery.copyToRealm(realm, generateGoals());
    }

    public static void generateEmpty(Realm realm) {
        attributeId = -1;
        exhId = 0;

        GoalQuery.copyToRealm(realm, generateGoals());
    }

    public static Attribute generatePhysical(Realm realm, boolean history) {
        Profile profile = ProfileQuery.get(realm);
        Attribute attributePhysical = new Attribute();
        attributePhysical.setId(attributeId++);
        attributePhysical.setTitle("Physical");
        attributePhysical.setPoints(0);
        attributePhysical.setDifficulty(Difficulty.NORMAL);
        attributePhysical.setSkills(new ArrayList<>());

        Skill cardio = new Skill();
        cardio.setId(0);
        cardio.setTitle("Cardio");
        cardio.setPoints(0);
        cardio.setExercises(new ArrayList<>());

        Exercise running = new Exercise();
        running.setId(0);
        running.setTitle("Running");
        running.setDifficulty(Difficulty.HARD);
        running.setScope(Scope.MEDIUM);
        running.setCountForDay(0);
        running.setStatistics(new ArrayList<>());
        running.setDescription("Let's go for a run, how about 3 kilometres for now.");

        if (history) {
            DateTime date = DateTime.now();
            date = date.withTimeAtStartOfDay();
            date = date.minusDays(10);
            for (int i = 0; i <= 10; i++) {
                ExerciseHistory eh = new ExerciseHistory();
                eh.setId(exhId++);
                eh.setDate(date);
                int count = (int) (Math.random() * 5);
                eh.setCountForDay(count);
                running.setCountForDay(count);
                eh.setPoints(running.getPointsForExercise() * count);
                attributePhysical.addPoints(running.getPointsForExercise() * count);
                cardio.addPoints(running.getPointsForExercise() * count);
                profile.addPoints(running.getPointsForExercise() * count);
                running.getStatistics().add(eh);

                date = date.plusDays(1);
            }
        }

        Exercise swimming = new Exercise();
        swimming.setId(1);
        swimming.setTitle("Swimming");
        swimming.setDifficulty(Difficulty.NORMAL);
        swimming.setScope(Scope.MEDIUM);
        swimming.setCountForDay(0);
        swimming.setStatistics(new ArrayList<>());
        swimming.setDescription("Hey running is boring, seems swimming might be the thing for me - swim at least 20 lengths");

        if (history) {
            DateTime date = DateTime.now();
            date = date.withTimeAtStartOfDay();
            date = date.minusDays(30);
            for (int i = 0; i <= 30; i++) {
                ExerciseHistory eh = new ExerciseHistory();
                eh.setId(exhId++);
                eh.setDate(date);
                int count = (int) (Math.random() * 5);
                eh.setCountForDay(count);
                swimming.setCountForDay(count);
                eh.setPoints(eh.getCountForDay() * swimming.getPointsForExercise());
                swimming.getStatistics().add(eh);
                attributePhysical.addPoints(swimming.getPointsForExercise() * count);
                cardio.addPoints(swimming.getPointsForExercise() * count);
                profile.addPoints(running.getPointsForExercise() * count);
                date = date.plusDays(1);
            }
        }

        cardio.getExercises().add(running);
        cardio.getExercises().add(swimming);

        Skill hit_the_gym = new Skill();
        hit_the_gym.setId(1);
        hit_the_gym.setTitle("Hit the Gym");
        hit_the_gym.setPoints(0);
        hit_the_gym.setExercises(new ArrayList<>());

        Exercise chest_bicep = new Exercise();
        chest_bicep.setId(2);
        chest_bicep.setTitle("Chest & Bicep");
        chest_bicep.setDifficulty(Difficulty.NORMAL);
        chest_bicep.setScope(Scope.MEDIUM);
        chest_bicep.setCountForDay(0);
        chest_bicep.setStatistics(new ArrayList<>());
        chest_bicep.setDescription("4 reps of 8 with 50 kilos on bench and barbell bicep curl with 25kgs, plus add some dumbell exercises.");

        if (history) {
            DateTime date = DateTime.now();
            date = date.withTimeAtStartOfDay();
            date = date.minusDays(100);
            for (int i = 0; i <= 100; i++) {
                ExerciseHistory eh = new ExerciseHistory();
                eh.setId(exhId++);
                eh.setDate(date);
                int count = (int) (Math.random() * 5);
                eh.setCountForDay(count);
                chest_bicep.setCountForDay(count);
                eh.setPoints(eh.getCountForDay() * chest_bicep.getPointsForExercise());
                attributePhysical.addPoints(chest_bicep.getPointsForExercise() * count);
                hit_the_gym.addPoints(chest_bicep.getPointsForExercise() * count);
                profile.addPoints(running.getPointsForExercise() * count);
                chest_bicep.getStatistics().add(eh);
                date = date.plusDays(1);
            }
        }

        Exercise tricep_back = new Exercise();
        tricep_back.setId(3);
        tricep_back.setTitle("Tricep & Back");
        tricep_back.setDifficulty(Difficulty.NORMAL);
        tricep_back.setScope(Scope.MEDIUM);
        tricep_back.setCountForDay(0);
        tricep_back.setStatistics(new ArrayList<>());
        tricep_back.setDescription("4 reps of 8 with 50 kilos - barbell row, tricep dips plus add some dumbell or machine exercises.");

        /*Exercise legs_shoulders = new Exercise();
        legs_shoulders.setId(4);
        legs_shoulders.setTitle("Legs & Shoulders");
        legs_shoulders.setDifficulty(Difficulty.NORMAL);
        legs_shoulders.setScope(Scope.MEDIUM);
        legs_shoulders.setCountForDay(0);
        legs_shoulders.setStatistics(new ArrayList<>());
        legs_shoulders.setDescription("Sqwuats - 5 reps of 10 with 50 kilos, barbell 4x8 with 35 kilos for shoulders, add some machine and/or dumbells.");
*/
        hit_the_gym.getExercises().add(chest_bicep);
        hit_the_gym.getExercises().add(tricep_back);
//        hit_the_gym.getExercises().add(legs_shoulders);

        attributePhysical.getSkills().add(cardio);
        attributePhysical.getSkills().add(hit_the_gym);

        ProfileQuery.edit(realm, profile);

        return attributePhysical;
    }

    public static Attribute generateIntelligence() {

        Attribute attributeIntelligence = new Attribute();
        attributeIntelligence.setId(attributeId++);
        attributeIntelligence.setTitle("Intelligence");
        attributeIntelligence.setPoints(0);
        attributeIntelligence.setDifficulty(Difficulty.NORMAL);
        attributeIntelligence.setSkills(new ArrayList<>());

        Skill french = new Skill();
        french.setId(2);
        french.setTitle("French Language");
        french.setPoints(0);
        french.setExercises(new ArrayList<>());

        Exercise vocabulary = new Exercise();
        vocabulary.setId(5);
        vocabulary.setTitle("Vocabulary");
        vocabulary.setDifficulty(Difficulty.NORMAL);
        vocabulary.setScope(Scope.SMALL);
        vocabulary.setCountForDay(0);
        vocabulary.setStatistics(new ArrayList<>());
        vocabulary.setDescription("Learn 10 new words, try to use them in sentence with what you already know, just don't forget them right away ...");

        Exercise reading = new Exercise();
        reading.setId(6);
        reading.setTitle("Reading");
        reading.setDifficulty(Difficulty.NORMAL);
        reading.setScope(Scope.SMALL);
        reading.setCountForDay(0);
        reading.setStatistics(new ArrayList<>());
        reading.setDescription("Read a page in french. Doesn't matter whether it's news article, book or anything else, and feel free to use translator.");

       /* Exercise listening = new Exercise();
        listening.setId(7);
        listening.setTitle("Listening");
        listening.setDifficulty(Difficulty.EASY);
        listening.setScope(Scope.SMALL);
        listening.setCountForDay(0);
        listening.setStatistics(new ArrayList<>());
        listening.setDescription("One french youtube video a day might take you quite far, you'll see.");*/

        french.getExercises().add(vocabulary);
        french.getExercises().add(reading);
        //french.getExercises().add(listening);

        Skill school = new Skill();
        school.setId(3);
        school.setTitle("School");
        school.setPoints(0);
        school.setExercises(new ArrayList<>());

        Exercise study = new Exercise();
        study.setId(8);
        study.setTitle("Study");
        study.setDifficulty(Difficulty.NORMAL);
        study.setScope(Scope.MEDIUM);
        study.setCountForDay(0);
        study.setStatistics(new ArrayList<>());
        study.setDescription("Let's give it an hour, no more. Maybe the test will be easy ...");

        Exercise project = new Exercise();
        project.setId(9);
        project.setTitle("Study");
        project.setDifficulty(Difficulty.NORMAL);
        project.setScope(Scope.MEDIUM);
        project.setCountForDay(0);
        project.setStatistics(new ArrayList<>());
        project.setDescription("Come on, do something for that stupid project, deadline is dangerously close");

        school.getExercises().add(study);
        school.getExercises().add(project);

        /*Skill crime = new Skill();
        crime.setId(4);
        crime.setTitle("Crime and Punishment");
        crime.setPoints(0);
        crime.setExercises(new ArrayList<>());

        Exercise pages10 = new Exercise();
        pages10.setId(9);
        pages10.setTitle("Read 10 pages");
        pages10.setDifficulty(Difficulty.NORMAL);
        pages10.setScope(Scope.MEDIUM);
        pages10.setCountForDay(0);
        pages10.setStatistics(new ArrayList<>());

        crime.getExercises().add(pages10);*/

        attributeIntelligence.getSkills().add(french);
        //attributeIntelligence.getSkills().add(crime);
        attributeIntelligence.getSkills().add(school);

        return attributeIntelligence;
    }

    public static Attribute generateSpirit() {

        Attribute attributeSpirit = new Attribute();
        attributeSpirit.setId(attributeId++);
        attributeSpirit.setTitle("Spirit");
        attributeSpirit.setPoints(0);
        attributeSpirit.setDifficulty(Difficulty.NORMAL);
        attributeSpirit.setSkills(new ArrayList<>());

        Skill saxophone = new Skill();
        saxophone.setId(4);
        saxophone.setTitle("Saxophone");
        saxophone.setPoints(0);
        saxophone.setExercises(new ArrayList<>());

        Exercise exercise10 = new Exercise();
        exercise10.setId(10);
        exercise10.setTitle("Exercise 10");
        exercise10.setDifficulty(Difficulty.NORMAL);
        exercise10.setScope(Scope.MEDIUM);
        exercise10.setCountForDay(0);
        exercise10.setStatistics(new ArrayList<>());
        exercise10.setDescription("I need to get over this exercise, it's been keeping me in place for way too long.");

        /*Exercise exercise11 = new Exercise();
        exercise11.setId(11);
        exercise11.setTitle("Exercise 11");
        exercise11.setDifficulty(Difficulty.NORMAL);
        exercise11.setScope(Scope.MEDIUM);
        exercise11.setCountForDay(0);
        exercise11.setStatistics(new ArrayList<>());*/

        Exercise aMinorScale = new Exercise();
        aMinorScale.setId(11);
        aMinorScale.setTitle("A Minor Scale exercises");
        aMinorScale.setDifficulty(Difficulty.NORMAL);
        aMinorScale.setScope(Scope.MEDIUM);
        aMinorScale.setCountForDay(0);
        aMinorScale.setStatistics(new ArrayList<>());
        aMinorScale.setDescription("Enaough of G minor, time to move on ...");

        saxophone.getExercises().add(exercise10);
        //saxophone.getExercises().add(exercise11);
        saxophone.getExercises().add(aMinorScale);

        Skill painting = new Skill();
        painting.setId(5);
        painting.setTitle("Painting");
        painting.setPoints(0);
        painting.setExercises(new ArrayList<>());

        Exercise paint = new Exercise();
        paint.setId(12);
        paint.setTitle("Paint the blue-eyed girl");
        paint.setDifficulty(Difficulty.HARD);
        paint.setScope(Scope.MEDIUM);
        paint.setCountForDay(0);
        paint.setStatistics(new ArrayList<>());
        paint.setDescription("Do you remember the blue-eyed girl wrapped in a hooded coat? work on your painting before you forget her.");

        painting.getExercises().add(paint);

        attributeSpirit.getSkills().add(saxophone);
        attributeSpirit.getSkills().add(painting);

        return attributeSpirit;
    }

    public static List<Goal> generateGoals() {

        ArrayList<Goal> goals = new ArrayList<>();

        Goal firstGoal = new Goal();
        firstGoal.setId(0);
        firstGoal.setTitle("First Goal");
        firstGoal.setDescription("Create your first own personal goal");

        firstGoal.setDifficulty(Difficulty.EASY);

        goals.add(firstGoal);

        /*Goal secondGoal = new Goal();
        secondGoal.setId(0);
        secondGoal.setTitle("Second Goal");
        secondGoal.setDescription("Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate. Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.");
        secondGoal.setAttributeId(2);
        secondGoal.setSkillId(2);
        secondGoal.setDifficulty(Difficulty.HARD);

        goals.add(secondGoal);

        Goal thirdGoal = new Goal();
        thirdGoal.setId(0);
        thirdGoal.setTitle("Third Goal");
        thirdGoal.setDescription("");
        thirdGoal.setAttributeId(3);
        thirdGoal.setSkillId(5);
        thirdGoal.setDifficulty(Difficulty.EASY);

        goals.add(thirdGoal);

        Goal fourthGoal = new Goal();
        fourthGoal.setId(0);
        fourthGoal.setTitle("Fourth Goal");
        fourthGoal.setDescription("Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate. ");
        fourthGoal.setAttributeId(4);
        fourthGoal.setSkillId(7);
        fourthGoal.setDifficulty(Difficulty.NORMAL);

        goals.add(fourthGoal);*/

        return goals;
    }

    public static Attribute generateAttribute1(Realm realm, boolean history) {
        Profile profile = ProfileQuery.get(realm);
        Attribute attributePhysical = new Attribute();
        attributePhysical.setId(attributeId++);
        attributePhysical.setTitle("Attribute 1");
        attributePhysical.setPoints(0);
        attributePhysical.setDifficulty(Difficulty.NORMAL);
        attributePhysical.setSkills(new ArrayList<>());

        Skill cardio = new Skill();
        cardio.setId(0);
        cardio.setTitle("Skill A");
        cardio.setPoints(0);
        cardio.setExercises(new ArrayList<>());

        Exercise running = new Exercise();
        running.setId(0);
        running.setTitle("Exercise I");
        running.setDifficulty(Difficulty.NORMAL);
        running.setScope(Scope.MEDIUM);
        running.setCountForDay(0);
        running.setStatistics(new ArrayList<>());
        running.setDescription("Description for Exercise I");

        /*if (history) {
            DateTime date = DateTime.now();
            date = date.withTimeAtStartOfDay();
            date = date.minusDays(10);
            for (int i = 0; i <= 10; i++) {
                ExerciseHistory eh = new ExerciseHistory();
                eh.setId(exhId++);
                eh.setDate(date);
                int count = (int) (Math.random() * 5);
                eh.setCountForDay(count);
                running.setCountForDay(count);
                eh.setPoints(running.getPointsForExercise() * count);
                attributePhysical.addPoints(running.getPointsForExercise() * count);
                cardio.addPoints(running.getPointsForExercise() * count);
                profile.addPoints(running.getPointsForExercise() * count);
                running.getStatistics().add(eh);

                date = date.plusDays(1);
            }
        }*/

        Exercise swimming = new Exercise();
        swimming.setId(1);
        swimming.setTitle("Exercise II");
        swimming.setDifficulty(Difficulty.NORMAL);
        swimming.setScope(Scope.MEDIUM);
        swimming.setCountForDay(0);
        swimming.setStatistics(new ArrayList<>());
        swimming.setDescription("Description for Exercise II");

        /*if (history) {
            DateTime date = DateTime.now();
            date = date.withTimeAtStartOfDay();
            date = date.minusDays(30);
            for (int i = 0; i <= 30; i++) {
                ExerciseHistory eh = new ExerciseHistory();
                eh.setId(exhId++);
                eh.setDate(date);
                int count = (int) (Math.random() * 5);
                eh.setCountForDay(count);
                swimming.setCountForDay(count);
                eh.setPoints(eh.getCountForDay() * swimming.getPointsForExercise());
                swimming.getStatistics().add(eh);
                attributePhysical.addPoints(swimming.getPointsForExercise() * count);
                cardio.addPoints(swimming.getPointsForExercise() * count);
                profile.addPoints(running.getPointsForExercise() * count);
                date = date.plusDays(1);
            }
        }*/

        cardio.getExercises().add(running);
        cardio.getExercises().add(swimming);

        Skill hit_the_gym = new Skill();
        hit_the_gym.setId(1);
        hit_the_gym.setTitle("Skill B");
        hit_the_gym.setPoints(0);
        hit_the_gym.setExercises(new ArrayList<>());

        Exercise chest_bicep = new Exercise();
        chest_bicep.setId(2);
        chest_bicep.setTitle("Exercise III");
        chest_bicep.setDifficulty(Difficulty.NORMAL);
        chest_bicep.setScope(Scope.MEDIUM);
        chest_bicep.setCountForDay(0);
        chest_bicep.setStatistics(new ArrayList<>());
        chest_bicep.setDescription("Description for Exercise III");

        /*if (history) {
            DateTime date = DateTime.now();
            date = date.withTimeAtStartOfDay();
            date = date.minusDays(100);
            for (int i = 0; i <= 100; i++) {
                ExerciseHistory eh = new ExerciseHistory();
                eh.setId(exhId++);
                eh.setDate(date);
                int count = (int) (Math.random() * 5);
                eh.setCountForDay(count);
                chest_bicep.setCountForDay(count);
                eh.setPoints(eh.getCountForDay() * chest_bicep.getPointsForExercise());
                attributePhysical.addPoints(chest_bicep.getPointsForExercise() * count);
                hit_the_gym.addPoints(chest_bicep.getPointsForExercise() * count);
                profile.addPoints(running.getPointsForExercise() * count);
                chest_bicep.getStatistics().add(eh);
                date = date.plusDays(1);
            }
        }*/

        Exercise tricep_back = new Exercise();
        tricep_back.setId(3);
        tricep_back.setTitle("Exercise IV");
        tricep_back.setDifficulty(Difficulty.NORMAL);
        tricep_back.setScope(Scope.MEDIUM);
        tricep_back.setCountForDay(0);
        tricep_back.setStatistics(new ArrayList<>());
        tricep_back.setDescription("Description for Exercise IV");

        Exercise legs_shoulders = new Exercise();
        legs_shoulders.setId(4);
        legs_shoulders.setTitle("Exercise V");
        legs_shoulders.setDifficulty(Difficulty.NORMAL);
        legs_shoulders.setScope(Scope.MEDIUM);
        legs_shoulders.setCountForDay(0);
        legs_shoulders.setStatistics(new ArrayList<>());
        legs_shoulders.setDescription("Description for Exercise V");

        hit_the_gym.getExercises().add(chest_bicep);
        hit_the_gym.getExercises().add(tricep_back);
        hit_the_gym.getExercises().add(legs_shoulders);

        attributePhysical.getSkills().add(cardio);
        attributePhysical.getSkills().add(hit_the_gym);

        ProfileQuery.edit(realm, profile);

        return attributePhysical;
    }

    public static Attribute generateAttribute2() {

        Attribute attributeIntelligence = new Attribute();
        attributeIntelligence.setId(attributeId++);
        attributeIntelligence.setTitle("Attribute 2");
        attributeIntelligence.setPoints(0);
        attributeIntelligence.setDifficulty(Difficulty.NORMAL);
        attributeIntelligence.setSkills(new ArrayList<>());

        Skill french = new Skill();
        french.setId(2);
        french.setTitle("Skill C");
        french.setPoints(0);
        french.setExercises(new ArrayList<>());

        Exercise vocabulary = new Exercise();
        vocabulary.setId(5);
        vocabulary.setTitle("Exercise VI");
        vocabulary.setDifficulty(Difficulty.NORMAL);
        vocabulary.setScope(Scope.MEDIUM);
        vocabulary.setCountForDay(0);
        vocabulary.setStatistics(new ArrayList<>());
        vocabulary.setDescription("Description for Exercise VI");

        Exercise reading = new Exercise();
        reading.setId(6);
        reading.setTitle("Exercise VII");
        reading.setDifficulty(Difficulty.NORMAL);
        reading.setScope(Scope.MEDIUM);
        reading.setCountForDay(0);
        reading.setStatistics(new ArrayList<>());
        reading.setDescription("Description for Exercise VII");

        Exercise listening = new Exercise();
        listening.setId(7);
        listening.setTitle("Exercise VIII");
        listening.setDifficulty(Difficulty.NORMAL);
        listening.setScope(Scope.MEDIUM);
        listening.setCountForDay(0);
        listening.setStatistics(new ArrayList<>());
        listening.setDescription("Description for Exercise VIII");

        french.getExercises().add(vocabulary);
        french.getExercises().add(reading);
        french.getExercises().add(listening);

        Skill school = new Skill();
        school.setId(3);
        school.setTitle("Skill D");
        school.setPoints(0);
        school.setExercises(new ArrayList<>());

        Exercise study = new Exercise();
        study.setId(8);
        study.setTitle("Exercise IX");
        study.setDifficulty(Difficulty.NORMAL);
        study.setScope(Scope.MEDIUM);
        study.setCountForDay(0);
        study.setStatistics(new ArrayList<>());
        study.setDescription("Description for Exercise IX");

        Exercise project = new Exercise();
        project.setId(9);
        project.setTitle("Exercise X");
        project.setDifficulty(Difficulty.NORMAL);
        project.setScope(Scope.MEDIUM);
        project.setCountForDay(0);
        project.setStatistics(new ArrayList<>());
        project.setDescription("Description for Exercise X");

        school.getExercises().add(study);
        school.getExercises().add(project);

        /*Skill crime = new Skill();
        crime.setId(4);
        crime.setTitle("Crime and Punishment");
        crime.setPoints(0);
        crime.setExercises(new ArrayList<>());

        Exercise pages10 = new Exercise();
        pages10.setId(9);
        pages10.setTitle("Read 10 pages");
        pages10.setDifficulty(Difficulty.NORMAL);
        pages10.setScope(Scope.MEDIUM);
        pages10.setCountForDay(0);
        pages10.setStatistics(new ArrayList<>());

        crime.getExercises().add(pages10);*/

        attributeIntelligence.getSkills().add(french);
        //attributeIntelligence.getSkills().add(crime);
        attributeIntelligence.getSkills().add(school);

        return attributeIntelligence;
    }

    public static Attribute generateAttribute3() {

        Attribute attributeSpirit = new Attribute();
        attributeSpirit.setId(attributeId++);
        attributeSpirit.setTitle("Attribute 3");
        attributeSpirit.setPoints(0);
        attributeSpirit.setDifficulty(Difficulty.NORMAL);
        attributeSpirit.setSkills(new ArrayList<>());

        Skill saxophone = new Skill();
        saxophone.setId(4);
        saxophone.setTitle("Skill E");
        saxophone.setPoints(0);
        saxophone.setExercises(new ArrayList<>());

        Exercise exercise10 = new Exercise();
        exercise10.setId(10);
        exercise10.setTitle("Exercise XI");
        exercise10.setDifficulty(Difficulty.NORMAL);
        exercise10.setScope(Scope.MEDIUM);
        exercise10.setCountForDay(0);
        exercise10.setStatistics(new ArrayList<>());
        exercise10.setDescription("Description for Exercise XI");

        /*Exercise exercise11 = new Exercise();
        exercise11.setId(11);
        exercise11.setTitle("Exercise 11");
        exercise11.setDifficulty(Difficulty.NORMAL);
        exercise11.setScope(Scope.MEDIUM);
        exercise11.setCountForDay(0);
        exercise11.setStatistics(new ArrayList<>());*/

        Exercise aMinorScale = new Exercise();
        aMinorScale.setId(11);
        aMinorScale.setTitle("Exercise XII");
        aMinorScale.setDifficulty(Difficulty.NORMAL);
        aMinorScale.setScope(Scope.MEDIUM);
        aMinorScale.setCountForDay(0);
        aMinorScale.setStatistics(new ArrayList<>());
        exercise10.setDescription("Description for Exercise XII");

        saxophone.getExercises().add(exercise10);
        //saxophone.getExercises().add(exercise11);
        saxophone.getExercises().add(aMinorScale);

        Skill painting = new Skill();
        painting.setId(5);
        painting.setTitle("Skill F");
        painting.setPoints(0);
        painting.setExercises(new ArrayList<>());

        Exercise paint = new Exercise();
        paint.setId(12);
        paint.setTitle("Exercise XIII");
        paint.setDifficulty(Difficulty.NORMAL);
        paint.setScope(Scope.MEDIUM);
        paint.setCountForDay(0);
        paint.setStatistics(new ArrayList<>());
        paint.setDescription("Description for Exercise XIII");

        painting.getExercises().add(paint);

        attributeSpirit.getSkills().add(saxophone);
        attributeSpirit.getSkills().add(painting);

        return attributeSpirit;
    }

    public static void generatePlannerEntries(Realm realm) {

        realm.executeTransaction(realm1 -> {
            realm1.delete(PlannerEntryDatabase.class);
            realm1.delete(PlannerEntryIdsDatabase.class);
        });


        //          DAILY - Physical 1 - Cardio 0 - Swimming 0

        PlannerEntry pe1 = new PlannerEntry();
        pe1.setId(0);
        pe1.setStartDate(DateTime.now().minusDays(10).withTime(12,30,0,0));
        pe1.setRecurrence(Recurrence.DAILY);
        pe1.setForever(true);
        //pe1.setLimit(7);
        //pe1.setEndDate(DateTime.now().plusDays(10).withTime(23,59,59,59));
        pe1.setEvery(2);

        List<Integer> list = new ArrayList<>();
        list.add(0);

        LinkedHashMap skill1 = new LinkedHashMap();
        skill1.put(0, list);

        pe1.setExercises(new LinkedHashMap<>());
        pe1.getExercises().put(1, skill1);

        PlannerEntryQuery.copyToRealm(realm, pe1);

        //      WEEKLY - Physical 1 - Arms & Chest 1 - ALL -1
        //                            Cardio 0       - Swimming 1

        PlannerEntry pe2 = new PlannerEntry();
        pe2.setId(1);
        pe2.setStartDate(DateTime.now().withTime(7,0,0,0));
        pe2.setRecurrence(Recurrence.WEEKLY);
        boolean weekdays[] = new boolean[] {true, false, true, false, true, false, true};
        pe2.setWeekdays(weekdays);
        pe2.setForever(true);
        //pe2.setEndDate(DateTime.now().plusMonths(3).withTime(23,59,59,59));
        pe2.setEvery(2);

        List<Integer> list2 = new ArrayList<>();
        list2.add(C.ALL);
        List<Integer> list2a = new ArrayList<>();
        list2a.add(1);

        LinkedHashMap skill2 = new LinkedHashMap();
        skill2.put(1, list2);
        skill2.put(0, list2a);

        Logcat.d(skill2.size() + " " + skill2.get(0));

        pe2.setExercises(new LinkedHashMap<>());
        pe2.getExercises().put(1, skill2);

        PlannerEntryQuery.copyToRealm(realm, pe2);

        //      HOURLY - Intelligence 2 - French 2 - ALL -1

        PlannerEntry pe3 = new PlannerEntry();
        pe3.setId(2);
        pe3.setStartDate(DateTime.now().minusDays(25).withTime(23,0,0,0));
        pe3.setRecurrence(Recurrence.HOURLY);
        //pe3.setLimit(20);

        //pe3.setForever(true);
        pe3.setEvery(5);
        pe3.setEndDate(DateTime.now().withTime(20,0,0,0));

        List<Integer> list3 = new ArrayList<>();
        list3.add(C.ALL);

        LinkedHashMap skill3 = new LinkedHashMap();
        skill3.put(2, list3);

        pe3.setExercises(new LinkedHashMap<>());
        pe3.getExercises().put(2, skill3);

        PlannerEntryQuery.copyToRealm(realm, pe3);

        // MONTHLY - Spirit 3 - Saxophone 5 - Exercise 10,11 10,11

        PlannerEntry pe4 = new PlannerEntry();
        pe4.setId(3);
        pe4.setStartDate(DateTime.now().withTime(17,0,0,0));
        pe4.setRecurrence(Recurrence.MONTHLY);
        pe4.setDayOfMonth(false);
        pe4.setForever(true);
        pe4.setEvery(1);

        List<Integer> list4 = new ArrayList<>();
        list4.add(10);
        list4.add(11);

        LinkedHashMap skill4 = new LinkedHashMap();
        skill4.put(5, list4);

        pe4.setExercises(new LinkedHashMap<>());
        pe4.getExercises().put(3, skill4);

        PlannerEntryQuery.copyToRealm(realm, pe4);

        // YEARLY - Spirit 3 - Painting 6 - Mona 13
        //          Intelligence 2 - Crime 4 - Read 8

        PlannerEntry pe5 = new PlannerEntry();
        pe5.setId(4);
        pe5.setStartDate(DateTime.now().withTime(17,0,0,0));
        pe5.setRecurrence(Recurrence.YEARLY);
        pe5.setForever(true);
        pe5.setEvery(1);

        List<Integer> list5 = new ArrayList<>();
        list5.add(13);

        LinkedHashMap skill5 = new LinkedHashMap();
        skill5.put(6, list5);

        List<Integer> list5a = new ArrayList<>();
        list5a.add(8);

        LinkedHashMap skill5a = new LinkedHashMap();
        skill5a.put(4, list5a);

        pe5.setExercises(new LinkedHashMap<>());
        pe5.getExercises().put(3, skill5);
        pe5.getExercises().put(2, skill5a);

        PlannerEntryQuery.copyToRealm(realm, pe5);

    }

}
