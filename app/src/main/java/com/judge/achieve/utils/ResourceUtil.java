package com.judge.achieve.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

import com.judge.achieve.AppController;
import com.judge.achieve.common.C;

/**
 * Created by Daniel on 05.01.2017.
 */

public class ResourceUtil {

    public static Resources getResources() {
        return AppController.getContext().getResources();
    }

    public static String getString(int resId) {
        return AppController.getContext().getResources().getString(resId);
    }

    public static float getDimension(int resId) {
        return AppController.getContext().getResources().getDimension(resId);
    }

    public static int getDimensionInPixel(int resId) {
        return AppController.getContext().getResources().getDimensionPixelSize(resId);
    }

    public static Drawable getDrawable(int resId) {
        return AppController.getContext().getResources().getDrawable(resId);
    }

    public static DisplayMetrics getDisplayMetrics() {
        return AppController.getContext().getResources().getDisplayMetrics();
    }

    public static int getColor(int resId) {
        return AppController.getContext().getResources().getColor(resId);
    }

    public static int getCategoryColor(int position) {
        return AppController.getContext().getResources().getColor(C.CATEGORY_COLORS[position % 4]);
    }

}
