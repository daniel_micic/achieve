package com.judge.achieve.utils;

import android.widget.Toast;

import com.judge.achieve.AppController;

/**
 * Created by Daniel on 24.07.2016.
 */
public class ToastUtils {

    public static void makeToast(String text) {
        Toast.makeText(AppController.getContext(), text, Toast.LENGTH_SHORT).show();
    }
}
