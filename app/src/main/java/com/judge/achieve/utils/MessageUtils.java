package com.judge.achieve.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.judge.achieve.AppController;

/**
 * Created by Daniel on 24.08.2016.
 */
public class MessageUtils {

    public static void show(View root,String message) {
        Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show();
    }

    public static void show(View root,int id) {
        String message = AppController.getContext().getResources().getString(id);
        Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show();
    }
}
