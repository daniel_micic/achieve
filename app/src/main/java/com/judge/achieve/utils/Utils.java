package com.judge.achieve.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.judge.achieve.AppController;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.ExerciseHistory;

import org.joda.time.DateTime;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Daniel on 21.08.2016.
 */
public class Utils {

    public static int getCategoryColor(int position) {

        return AppController.getContext().getResources().getColor(C.CATEGORY_COLORS[position % 4]);
    }

    public static boolean isLollipop() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            return true;
        } else{
            return false;
        }
    }

    public static void showSoftInput(EditText et, Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideInput(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Drawable getDrawable(int id) {
        return AppController.getContext().getResources().getDrawable(id);
    }

    public static int getColor(int id) {
        return AppController.getContext().getResources().getColor(id);
    }

    public static Pair<Boolean, String> getExerciseHistoryString(Exercise exercise, int cycleNo) {

        if (exercise == null) {
            return null;
        }

        DateTime now = DateTime.now().withTimeAtStartOfDay();

        List<String> historyStrings = new ArrayList<>();

        int today = 0;
        int thisweek = 0;
        int thismonth = 0;
        int thisyear = 0;

        ListIterator<ExerciseHistory> listIter = exercise.getStatistics().listIterator(exercise.getStatistics().size());
        while (listIter.hasPrevious()) {
            ExerciseHistory prev = listIter.previous();
            if (prev.getDate().equals(now)) {
                today += prev.getCountForDay();
            }
            if (prev.getDate().isAfter(now.minusDays(now.getDayOfWeek()))) {
                thisweek += prev.getCountForDay();
            }
            if (prev.getDate().isAfter(now.minusDays(now.getDayOfMonth()))) {
                thismonth += prev.getCountForDay();
            }
            if (prev.getDate().isAfter(now.minusDays(now.getDayOfYear()))) {
                thisyear += prev.getCountForDay();;
            }
        }


        historyStrings.add(today + " TODAY");

        if (thisweek > 0) {
            if (thisweek > today)
                historyStrings.add(thisweek + " THIS WEEK");
        }
        if (thismonth > 0) {
            if (thismonth > thisweek)
                historyStrings.add(thismonth + " THIS MONTH");
        }
        if (thisyear > 0) {
            if (thisyear > thismonth)
                historyStrings.add(thisyear + " THIS YEAR");
        }

        /*if (historyStrings.size() >= 0) {
            Logcat.d(cycleNo + " % "  +historyStrings.size());
            Logcat.d(cycleNo % historyStrings.size());
        }*/

        if (historyStrings.size() == 1) {
            return new Pair<>(false, historyStrings.get(0));
        } else {
            return new Pair<>(true, historyStrings.get(cycleNo % historyStrings.size()));
        }
    }

    public static Drawable getAttrBackgroundChips(int position) {
        switch (position % 4) {
            default:
            case 0:
                return ResourceUtil.getDrawable(R.drawable.background_entry_attr_skill_1);
            case 1:
                return ResourceUtil.getDrawable(R.drawable.background_entry_attr_skill_2);
            case 2:
                return ResourceUtil.getDrawable(R.drawable.background_entry_attr_skill_3);
            case 3:
                return ResourceUtil.getDrawable(R.drawable.background_entry_attr_skill_4);

        }
    }

    public static File getOutputMediaFile() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/Achieve");

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                Logcat.d("mkdir failed");
                return null;
            }
        }

        //take the current timeStamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        //and make a media file:
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_Profile.jpg");

        Logcat.d("getting media file " + mediaFile.getAbsolutePath());
        return mediaFile;
    }
}
