package com.judge.achieve.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.DialogEditProfileBinding;
import com.judge.achieve.databinding.DialogMotivationalPopupBinding;
import com.judge.achieve.ui.main.MainActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 04.10.2016.
 */

public class DialogUtils {
    public interface DialogListener {
        void onPositiveClicked();
        void onNegativeClicked();
    }

    public interface AvatarChangeListener {
        void onAvatarChanged(Uri uri);
        void onPermissionGranted();
    }



    public static AlertDialog getOkCancelDialog(final Context context,
                                              final String text,
                                              final DialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(text);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onPositiveClicked();
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onNegativeClicked();
                dialogInterface.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }
        });
        return dialog;
    }

    public static AlertDialog getYesNoDialog(final Context context,
                                              final String text,
                                              final int color,
                                              final DialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(text);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onPositiveClicked();
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onNegativeClicked();
                dialogInterface.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(color);
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(color);
            }
        });
        return dialog;
    }

    public static AlertDialog getLevelUpDialog(final Activity context, int level) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = context.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        DialogMotivationalPopupBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_motivational_popup, null, false);
        binding.popupTitle.setVisibility(View.GONE);
        binding.popupCircleBackground.getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        binding.popupPrimaryText.setText(ResourceUtil.getString(R.string.level_up));
        //String.format(res.getString(R.string.welcome_messages), username, mailCount);
        if (level > 2) {
            binding.popupSecondaryText.setText(String.format(ResourceUtil.getResources().getString(R.string.msg_level_up), level));
        } else {
            binding.popupSecondaryText.setText(ResourceUtil.getResources().getString(R.string.msg_first_level_up));
        }
        builder.setView(binding.getRoot());

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        binding.popupOkButton.setOnClickListener(view -> dialog.dismiss());
        return dialog;
    }

    public static AlertDialog getNewSkillPointDialog(final Activity context, String skill) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = context.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        DialogMotivationalPopupBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_motivational_popup, null, false);
        binding.popupTitle.setVisibility(View.GONE);
        binding.popupCircleBackground.getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        binding.popupTitle.setText(skill);
        binding.popupPrimaryText.setText(ResourceUtil.getString(R.string.skill_point_gained));
        binding.popupSecondaryText.setText(ResourceUtil.getResources().getString(R.string.msg_skill_point_gained));
        builder.setView(binding.getRoot());

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        binding.popupOkButton.setOnClickListener(view -> dialog.dismiss());
        return dialog;
    }

    public static AlertDialog getEditProfileDialog(final MainActivity context, Uri profilePic, String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = context.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        DialogEditProfileBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_profile, null, false);

        binding.dialogName.setText(name);
        if (profilePic != null) {
            binding.dialogAvatar.setImageURI(profilePic);
        }
        binding.dialogAvatar.setOnClickListener(view -> {
// Determine Uri of camera image to save.

            if (!context.checkPermissions()) {
                return;
            }
            final File imageFile = Utils.getOutputMediaFile();
            Uri outputFileUri = Uri.fromFile(imageFile);
            context.setOutputFileUri(outputFileUri);

            AvatarChangeListener listener = new AvatarChangeListener() {
                @Override
                public void onAvatarChanged(Uri uri) {
                    binding.dialogAvatar.setImageURI(uri);
                }

                @Override
                public void onPermissionGranted() {
                    binding.dialogAvatar.performClick();
                }
            };
            context.setAvatarChangeListener(listener);

            // Camera.
            final List<Intent> cameraIntents = new ArrayList<Intent>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = context.getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for(ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                cameraIntents.add(intent);
            }

            // Filesystem.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_PICK);

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            context.startActivityForResult(chooserIntent, MainActivity.AVATAR_REQUEST_CODE);
        });

        /*binding.popupCircleBackground.getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        binding.popupTitle.setText(skill);
        binding.popupPrimaryText.setText(ResourceUtil.getString(R.string.skill_point_gained));
        binding.popupSecondaryText.setText(ResourceUtil.getResources().getString(R.string.msg_skill_point_gained));*/
        builder.setView(binding.getRoot());

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        binding.popupCancelButton.setOnClickListener(view -> {
            dialog.dismiss();

        });
        binding.popupOkButton.setOnClickListener(view -> {
            context.onEditProfile(binding.dialogName.getText().toString());
            dialog.dismiss();

        });
        return dialog;
    }

    public static AlertDialog getLockedDialog(final Context context, String type, DialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        switch (type) {
            case C.iap_statistics:
                builder.setMessage(ResourceUtil.getString(R.string.msg_locked_statistics));
                break;
            case C.iap_planner:
                builder.setMessage(ResourceUtil.getString(R.string.msg_locked_planner));
                break;
            case C.iap_attributes:
                builder.setMessage(ResourceUtil.getString(R.string.msg_locked_attributes));
                break;
            case C.iap_exercises:
                builder.setMessage(ResourceUtil.getString(R.string.msg_locked_exercises));
                break;
        }
        builder.setPositiveButton(R.string.go_to_shop, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onPositiveClicked();
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onNegativeClicked();
                dialogInterface.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ResourceUtil.getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ResourceUtil.getColor(R.color.colorPrimary));
            }
        });
        return dialog;
    }
}
