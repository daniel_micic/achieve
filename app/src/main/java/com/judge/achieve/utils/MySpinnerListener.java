package com.judge.achieve.utils;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Daniel on 09.02.2017.
 */

public abstract class MySpinnerListener implements AdapterView.OnItemSelectedListener {

    public abstract void onItemSelected(int position);

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        onItemSelected(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
