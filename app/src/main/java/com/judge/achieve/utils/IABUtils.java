package com.judge.achieve.utils;

import com.judge.achieve.common.C;

/**
 * Created by aa on 14. 4. 2017.
 */

public class IABUtils {

    public static boolean hasFullAccess() {
        setFulLAccess(true);
        return PrefUtils.getBool(C.iap_full_access);
    }

    public static boolean hasAttributtes() {
        return PrefUtils.getBool(C.iap_full_access) || PrefUtils.getBool(C.iap_attributes);
    }

    public static boolean hasExercises() {
        return PrefUtils.getBool(C.iap_full_access) || PrefUtils.getBool(C.iap_exercises);
    }

    public static boolean hasStatistics() {
        return PrefUtils.getBool(C.iap_full_access) || PrefUtils.getBool(C.iap_statistics);
    }

    public static boolean hasPlanner() {
        return PrefUtils.getBool(C.iap_full_access) || PrefUtils.getBool(C.iap_planner);
    }

    public static void setFulLAccess(boolean is) {
        PrefUtils.saveBool(C.iap_full_access, is);
    }
    public static void setAttributes(boolean is) {
        PrefUtils.saveBool(C.iap_attributes, is);
    }
    public static void setExercises(boolean is) {
        PrefUtils.saveBool(C.iap_exercises, is);
    }
    public static void setStatistics(boolean is) {
        PrefUtils.saveBool(C.iap_statistics, is);
    }
    public static void setPlanner(boolean is) {
        PrefUtils.saveBool(C.iap_planner, is);
    }



}
