package com.judge.achieve.utils;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.view.View;
import android.view.ViewAnimationUtils;

/**
 * Created by Daniel on 05.09.2016.
 */
public class AnimationUtils {

    public interface AnimationCallback {
        void onAnimationEnd();
    }

    @TargetApi(21)
    public static void makeCircularReveal(View view, int x, int y, int radius, final AnimationCallback callback) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(view, x, y, 0, radius);
            circularReveal.setDuration(300);
            circularReveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (callback != null) {
                        callback.onAnimationEnd();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }
            });
            circularReveal.start();
        } else {
            if (callback != null)
                callback.onAnimationEnd();
        }
    }

    @TargetApi(21)
    public static void makeCircularReveal(View view,
                                          int x, int y, int radius,
                                          final AnimationCallback callback,
                                          int duration) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(view, x, y, 0, radius);
            circularReveal.setDuration(duration);
            circularReveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) { }
                @Override
                public void onAnimationEnd(Animator animator) {
                    if (callback != null) {
                        callback.onAnimationEnd();
                    }
                }
                @Override
                public void onAnimationCancel(Animator animator) { }
                @Override
                public void onAnimationRepeat(Animator animator) { }
            });
            circularReveal.start();
        } else {
            callback.onAnimationEnd();
        }
    }


}
