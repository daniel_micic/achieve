package com.judge.achieve.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;

/**
 * Created by Daniel on 24.08.2016.
 */
public class ImageUtils {

    @SuppressLint("NewApi")
    @TargetApi(21)
    public static Drawable getPressedColorRippleDrawable(int normalColor, int pressedColor)
    {
        //TODO Fix for Pre Lollipop
        if (Utils.isLollipop()) {

        } else {

        }

        RippleDrawable drawable = new RippleDrawable(
                getPressedColorSelector(pressedColor),
                getColorDrawableFromColor(normalColor),
                getColorDrawableFromColor(pressedColor));
        //drawable.setRadius(5);
        return drawable;
    }

    public static ColorStateList getPressedColorSelector(int pressedColor)
    {
        return new ColorStateList(
                new int[][]
                        {
                                new int[]{}
                        },
                new int[]
                        {
                                pressedColor
                        }
        );
    }

    public static ColorDrawable getColorDrawableFromColor(int color)
    {
        return new ColorDrawable(color);
    }

}
