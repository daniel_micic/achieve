package com.judge.achieve.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.judge.achieve.broadcast.PlannerScheduleReceiver;
import com.judge.achieve.common.C;
import com.judge.achieve.model.PlannerEntry;

import org.joda.time.DateTime;


/**
 * Created by aa on 15. 4. 2017.
 */

public class AlarmUtils {

    public static void scheduleAlarm(Context context, PlannerEntry entry) {
        Intent alarmIntent = new Intent(context, PlannerScheduleReceiver.class);
        alarmIntent.putExtra(C.EXTRA_ENTRY_ID, entry.getTitle());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int interval = 8000;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, entry.getStartDate().getMillis(), interval, pendingIntent);
        Logcat.d("alarm set " + entry.getStartDate() + " " + DateTime.now());
    }
}
