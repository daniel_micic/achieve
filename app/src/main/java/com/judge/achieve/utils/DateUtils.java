package com.judge.achieve.utils;

import com.judge.achieve.common.C;

import java.util.Calendar;

/**
 * Created by Daniel on 21.08.2016.
 */
public class DateUtils {

    public static boolean isNewDay() {

        Calendar c = Calendar.getInstance();
        int thisDay = c.get(Calendar.DAY_OF_YEAR);
        long todayMillis = c.getTimeInMillis();

        long last = PrefUtils.getLong(C.PREF_DATE);
        c.setTimeInMillis(last);
        int lastDay = c.get(Calendar.DAY_OF_YEAR);

        if( last == -1 || lastDay != thisDay ){
            PrefUtils.saveLong(C.PREF_DATE, todayMillis);
            return true;
        }

        return false;

    }
}
