package com.judge.achieve.utils;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by aa on 28. 1. 2017.
 */

public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.85f;
    private static final float MIN_ALPHA = 0.5f;

    private Float zeroPosition = null;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (zeroPosition == null) {
            zeroPosition = position;
        }

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.

        } else if (position <= 1) { // [-1,1]
            // Modify the default slide transition to shrink the page as well
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position - zeroPosition));

            // Scale the page down (between MIN_SCALE and 1)
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

            // Fade the page relative to its size.

        } else { // (1,+Infinity]
            view.setScaleX(0.85f);
            view.setScaleY(0.85f);
            // This page is way off-screen to the right.
        }
    }
}