package com.judge.achieve.utils;

import android.animation.Animator;

/**
 * Created by Daniel on 26.01.2017.
 */

public abstract class MyAnimationListener implements Animator.AnimatorListener {

    public abstract void onMyAnimationFinished();


    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {
        onMyAnimationFinished();
    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }
}
