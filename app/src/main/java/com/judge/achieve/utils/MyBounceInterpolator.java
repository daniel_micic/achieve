package com.judge.achieve.utils;

import android.view.animation.Interpolator;

/**
 * Created by aa on 29. 1. 2017.
 */

public class MyBounceInterpolator implements Interpolator {

    double Amplitude = 1;
    double Frequency = 10;

    MyBounceInterpolator(double amp, double freq) {
        Amplitude = amp;
        Frequency = freq;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ Amplitude) * Math.cos(Frequency * time) + 1);
    }

}
