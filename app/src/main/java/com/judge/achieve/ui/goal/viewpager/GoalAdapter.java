package com.judge.achieve.ui.goal.viewpager;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.judge.achieve.model.Goal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 24.01.2017.
 */

public class GoalAdapter extends FragmentStatePagerAdapter {

    private boolean entryAnimation = true;

    private ArrayList<Fragment> fragments = new ArrayList<>();

    private GoalProvider provider;

    public interface GoalProvider {
        int getCount();
        Goal getGoal(int position);
        List<Goal> getGoals();
    }

    public GoalAdapter(FragmentManager fragmentManager, GoalProvider provider, boolean shouldAnimate) {
        super(fragmentManager);
        entryAnimation = shouldAnimate;
        this.provider = provider;

        for (Goal goal : provider.getGoals()) {
            GoalFragment goalFragment = GoalFragment.newInstance(goal.getId(), entryAnimation);
            fragments.add(goalFragment);

            entryAnimation = false;
        }

        AddGoalFragment addGoalFragment = AddGoalFragment.newInstance(entryAnimation);
        fragments.add(addGoalFragment);
    }
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
        /*if (position == getCount() - 1) {
            AddGoalFragment addGoalFragment = AddGoalFragment.newInstance(entryAnimation);
            if (fragments.size() > position)
                fragments.remove(position);
            fragments.add(position, addGoalFragment);
            entryAnimation = false;
            return addGoalFragment;
        } else {
            TutorialFragment goalFragment = TutorialFragment.newInstance(provider.getGoal(position).getId(), entryAnimation);
            if (fragments.size() > position)
                fragments.remove(position);
            fragments.add(position, goalFragment);
            entryAnimation = false;
            return goalFragment;
        }*/
    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return provider.getCount() + 1;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void removeFragment(int position) {
        fragments.remove(position);
    }

    public Fragment getFragment(int position) {
        return fragments.get(position);
    }


}
