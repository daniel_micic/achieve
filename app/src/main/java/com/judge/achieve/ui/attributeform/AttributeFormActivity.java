package com.judge.achieve.ui.attributeform;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.databinding.ActivityAttributeFormBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.Logcat;

public class AttributeFormActivity extends BaseActivity {

    ActivityAttributeFormBinding binding;

    /*@BindView(R.id.create_category_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.create_category_title_et)
    EditText mTitleEt;

    @BindView(R.id.create_category_easy_checkbox)
    CheckBox mEasyCheckbox;

    @BindView(R.id.create_category_normal_checkbox)
    CheckBox mNormalCheckbox;

    @BindView(R.id.create_category_hard_checkbox)
    CheckBox mHardCheckbox;

    @BindView(R.id.create_category_next_btn)
    FloatingActionButton mNextBtn;

    @BindView(R.id.create_category_done_btn)
    FloatingActionButton mDoneBtn;*/

    AttributeFormPresenter mPresenter;

    int mMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_attribute_form);

        if (savedInstanceState == null) {
            Logcat.d("save instance was null");
            mMode = getIntent().getIntExtra(C.EXTRA_MODE, C.MODE_CREATE);
            int id = getIntent().getIntExtra(C.EXTRA_ATTRIBUTE_ID, -1);
            mPresenter = new AttributeFormPresenter(this, mMode, id);
        } else {
            Logcat.d("save instance was not null");
            mMode =  savedInstanceState.getInt(C.SAVED_INSTANCE_MODE, C.MODE_CREATE);
            mPresenter = new AttributeFormPresenter(this, mMode, savedInstanceState);
        }

        initViews();

        if (mMode == C.MODE_EDIT)
            setViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logcat.d("on save instance");
        outState.putInt(C.SAVED_INSTANCE_MODE, mMode);
        mPresenter.onSaveInstace(outState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public AttributeFormPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        if (mMode == C.MODE_CREATE) {
            setTitle(getString(R.string.create_attribute));
            binding.createCategoryDoneBtn.setVisibility(View.GONE);
            binding.createCategoryNextBtn.setVisibility(View.VISIBLE);
        } else if (mMode == C.MODE_EDIT) {
            setTitle(getString(R.string.edit_attribute));
            binding.createCategoryDoneBtn.setVisibility(View.VISIBLE);
            binding.createCategoryNextBtn.setVisibility(View.GONE);
        }

        setSupportActionBar(binding.createCategoryToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        binding.createCategoryToolbar.setNavigationOnClickListener(view -> AttributeFormActivity.this.onBackPressed());
        
        binding.createCategoryEasyCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (binding.createCategoryNormalCheckbox.isChecked()) {
                    binding.createCategoryNormalCheckbox.performClick();
                }
                if (binding.createCategoryHardCheckbox.isChecked()) {
                    binding.createCategoryHardCheckbox.performClick();
                }
            }
        });

        binding.createCategoryNormalCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (binding.createCategoryEasyCheckbox.isChecked()) {
                    binding.createCategoryEasyCheckbox.performClick();
                }
                if (binding.createCategoryHardCheckbox.isChecked()) {
                    binding.createCategoryHardCheckbox.performClick();
                }
            }
        });

        binding.createCategoryHardCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (binding.createCategoryEasyCheckbox.isChecked()) {
                    binding.createCategoryEasyCheckbox.performClick();
                }
                if (binding.createCategoryNormalCheckbox.isChecked()) {
                    binding.createCategoryNormalCheckbox.performClick();
                }
            }
        });

        binding.createCategoryDoneBtn.setOnClickListener(view -> mPresenter.onNextClicked());
        binding.createCategoryNextBtn.setOnClickListener(view -> mPresenter.onNextClicked());

    }

    public void setViews() {
       setCategoryTitle(getPresenter().getAttributeTitle());
       setCategoryDifficulty(getPresenter().getAttributeDifficulty());
    }

    public String getCategoryTitle() {
        return binding.createCategoryTitleEt.getText().toString();
    }

    public void setCategoryTitle(String title) {
        binding.createCategoryTitleEt.setText(title);
    }

    public void setCategoryTitleError() {
        Snackbar.make(binding.getRoot(), "Please enter title", Snackbar.LENGTH_SHORT).show();
    };

    public Difficulty getCategoryDifficulty() {
        if (binding.createCategoryEasyCheckbox.isChecked()) {
            return Difficulty.EASY;
        }
        if (binding.createCategoryNormalCheckbox.isChecked()) {
            return Difficulty.NORMAL;
        }
        if (binding.createCategoryHardCheckbox.isChecked()) {
            return Difficulty.HARD;
        }
        return Difficulty.UNDEFINED;
    }

    public void setCategoryDifficulty(Difficulty difficulty) {
        switch (difficulty) {
            case EASY:
                binding.createCategoryEasyCheckbox.setChecked(true);
                binding.createCategoryNormalCheckbox.setChecked(false);
                binding.createCategoryHardCheckbox.setChecked(false);
                break;
            case NORMAL:
                binding.createCategoryEasyCheckbox.setChecked(false);
                binding.createCategoryNormalCheckbox.setChecked(true);
                binding.createCategoryHardCheckbox.setChecked(false);
                break;
            case HARD:
                binding.createCategoryEasyCheckbox.setChecked(false);
                binding.createCategoryNormalCheckbox.setChecked(false);
                binding.createCategoryHardCheckbox.setChecked(true);
                break;
            case UNDEFINED:
                binding.createCategoryEasyCheckbox.setChecked(false);
                binding.createCategoryNormalCheckbox.setChecked(false);
                binding.createCategoryHardCheckbox.setChecked(false);
                break;
        }
    }

    public void setCategoryDifficultyError() {
        Snackbar.make(binding.getRoot(), "Please select Difficulty", Snackbar.LENGTH_SHORT).show();
    };
}
