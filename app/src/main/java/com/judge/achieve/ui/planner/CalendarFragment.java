package com.judge.achieve.ui.planner;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.recruit_mp.android.lightcalendarview.LightCalendarView;
import jp.co.recruit_mp.android.lightcalendarview.WeekDay;

/**
 * Created by Daniel on 24.01.2017.
 */

public class CalendarFragment extends Fragment {

    com.judge.achieve.databinding.FragmentCalendarBinding binding;

    private LightCalendarView.OnStateUpdatedListener listener;
    private long date;

    public static CalendarFragment newInstance(long date) {
        CalendarFragment fragment = new CalendarFragment();

        Bundle args = new Bundle();
        args.putLong(C.EXTRA_CALENDAR_DATE, date);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.date = getArguments().getLong(C.EXTRA_CALENDAR_DATE);
        Logcat.d(new Date(date).toString());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calendar, container, false);

        binding.calendar.setWeekDayFilterColor(WeekDay.SUNDAY, ResourceUtil.getColor(R.color.white));
        binding.calendar.setWeekDayFilterColor(WeekDay.SATURDAY, ResourceUtil.getColor(R.color.white));
        binding.calendar.setFirstDayOfWeek(WeekDay.SUNDAY);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date from = new Date();
        Date to = new Date();
        try {
            DateTime dateTime = new DateTime(date);
            from = formatter.parse(String.format("01/%d/%d 00:00:00", dateTime.getMonthOfYear(), dateTime.getYear()));
            to = formatter.parse(String.format("01/%d/%d 00:00:00", dateTime.getMonthOfYear(), dateTime.getYear()));
        } catch (ParseException e) {
            Logcat.d(e.toString());
        }

        binding.calendar.setMonthFrom(from);
        binding.calendar.setMonthTo(to);
        binding.calendar.setMonthCurrent(new Date(date));
        binding.calendar.setDayFilterColor(WeekDay.SUNDAY, ResourceUtil.getColor(R.color.white));
        binding.calendar.setDayFilterColor(WeekDay.SATURDAY, ResourceUtil.getColor(R.color.white));

        if (listener != null) {
            binding.calendar.setOnStateUpdatedListener(listener);
        }

        return binding.getRoot();
    }

    public void setListener(LightCalendarView.OnStateUpdatedListener listener) {
        this.listener = listener;
        if (binding != null && binding.calendar != null)
            binding.calendar.setOnStateUpdatedListener(listener);

    }
}
