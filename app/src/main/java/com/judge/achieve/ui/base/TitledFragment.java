package com.judge.achieve.ui.base;

/**
 * Created by Bartman on 10/27/2016.
 */

public interface TitledFragment {

    String getTitle();

}
