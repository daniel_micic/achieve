package com.judge.achieve.ui.entryform;

import android.content.Context;
import android.os.Bundle;
import android.text.format.DateUtils;

import com.codetroopers.betterpickers.recurrencepicker.EventRecurrence;
import com.codetroopers.betterpickers.recurrencepicker.EventRecurrenceFormatter;
import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Recurrence;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.PlannerEntryQuery;
import com.judge.achieve.persistence.query.SkillQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.ui.entryform.viewpager.EntryFormProvider;
import com.judge.achieve.ui.entryform.viewpager.form.FormFragment;
import com.judge.achieve.ui.entryform.viewpager.picker.PickerAdapter;
import com.judge.achieve.ui.entryform.viewpager.picker.PickerItem;
import com.judge.achieve.utils.Logcat;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Daniel on 01.10.2016.
 */

public class EntryFormPresenter extends BasePresenter
        implements PickerAdapter.ExerPickerProvider,
        EntryFormProvider,
        FormFragment.OnReccurenceSetListener
        {

    private EntryFormActivity mView;
    private Context context;

    private PlannerEntry entry;
    private int mMode;

    private List<Attribute> attributes;
    private List<Skill> skills;
    private List<Exercise> exercises;

    private LinkedHashSet<Integer> attrIds = new LinkedHashSet<>();
    private LinkedHashSet<Integer> skillIds = new LinkedHashSet<>();
    private LinkedHashSet<Integer> exerIds = new LinkedHashSet<>();

    public EntryFormPresenter(EntryFormActivity view, long id) {
        super();

        mView = view;
        context = view;
        mMode = C.MODE_EDIT;
        entry = PlannerEntryQuery.getById(getRealm(), id);

        if (entry != null) {
            initHashMaps();
        } else {
            mMode = C.MODE_CREATE;
            Logcat.d("edit failed, creating new");
        }


    }

    public EntryFormPresenter(EntryFormActivity view, DateTime selectedDay) {
        super();

        mView = view;
        context = view;
        mMode = C.MODE_CREATE;
        entry = new PlannerEntry();
        entry.setStartDate(selectedDay);
    }

    public EntryFormPresenter(EntryFormActivity view, int mode, Bundle savedInstanceState) {
        super();
        Logcat.d("recreating from bundle");
        mView = view;
        context = view;
        mMode = mode;

        entry = savedInstanceState.getParcelable(C.SAVED_INSTANCE_EBTRY);

        initHashMaps();
    }

    public void onSaveInstace(Bundle outState) {
        Logcat.d("on save instance");

        //setAndCheckDataFromView(false);

        outState.putParcelable(C.SAVED_INSTANCE_EBTRY, entry);
    }

    public void initHashMaps() {
        for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attr : entry.getExercises().entrySet()) {
            attrIds.add(attr.getKey());
            for (Map.Entry<Integer, List<Integer>> skill : attr.getValue().entrySet()) {
                skillIds.add(skill.getKey());
                for (Integer id : skill.getValue()) {
                    exerIds.add(id);
                }
            }
        }
    }

    @Override
    public int getMode() {
        return mMode;
    }

    public PlannerEntry getEntry() {
    return entry;
    }

    public void setEntry(PlannerEntry entry) {
        this.entry = entry;
    }

    public String getFormattedStartDate() {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
        return dtf.print(entry.getStartDate());
    }

    @Override
    public String getEntryRecurrenceString() {
        switch (entry.getRecurrence()) {
            case HOURLY:
                return "Hourly";
            case DAILY:
                return "Daily";
            case WEEKLY:
                return "Weekly";
            case MONTHLY:
                return "Monthly";
            case YEARLY:
                return "Yearly";

        }
        return null;
    }


    @Override
    public List<PickerItem> getAttributes() {
        List<PickerItem> attributesForPicker = new ArrayList<>();

        if (attributes == null) {
            attributes = AttributeQuery.getAll(getRealm());
        }

        for (Attribute attribute : attributes) {
            attributesForPicker.add(new PickerItem(attribute, attrIds.contains(attribute.getId())));
        }

        return attributesForPicker;
    }



    @Override
    public List<PickerItem> getSkills() {
        ArrayList<PickerItem> skillsForPicker = new ArrayList<>();
        for (Integer attrId : attrIds) {
            boolean title = true;
            Attribute attribute = AttributeQuery.getById(getRealm(), attrId);
            for (Skill skill : attribute.getSkills()) {
                skillsForPicker.add(new PickerItem(skill, skillIds.contains(skill.getId()), title, attribute.getTitle()));
                title = false;
            }
        }

        return skillsForPicker;
    }

    @Override
    public List<PickerItem> getExercises() {
        ArrayList<PickerItem> exercisesForPicker = new ArrayList<>();
        for (Integer skillId : skillIds) {
            boolean title = true;
            Skill skill = SkillQuery.getById(getRealm(), skillId);
            for (Exercise exercise : skill.getExercises()) {
                exercisesForPicker.add(new PickerItem(exercise, exerIds.contains(exercise.getId()), title, skill.getTitle()));
                title = false;
            }
        }

        return exercisesForPicker;
    }



    public void onAttributeSelected(int id, boolean checked) {
        if (checked) {
            attrIds.add(id);
        } else {
            Logcat.d(attrIds.size());
            attrIds.remove(id);
            Logcat.d(attrIds.size());
            for (Skill s : AttributeQuery.getById(getRealm(), id).getSkills()) {
                for (Exercise e : s.getExercises()) {
                    exerIds.remove(e.getId());
                }
                skillIds.remove(s.getId());
            }
        }
    }

    public void onSkillSelected(int id, boolean checked) {
        if (checked) {
            skillIds.add(id);
        } else {
            Logcat.d(skillIds.size());
            skillIds.remove(id);
            Logcat.d(skillIds.size());

            for (Exercise e : SkillQuery.getById(getRealm(), id).getExercises()) {
                exerIds.remove(e.getId());
            }
        }
    }

    public void onExerciseSelected(int id, boolean checked) {
        if (checked) {
            exerIds.add(id);
        } else {
            Logcat.d(exerIds.size());
            exerIds.remove(id);
            Logcat.d(exerIds.size());

        }
    }

    public String setEntryReccurence(EventRecurrence recurrence) {
        Logcat.d(recurrence.freq);
        Logcat.d(recurrence.until);
        Logcat.d(recurrence.count);
        Logcat.d(recurrence.interval);
        Logcat.d(recurrence.byweeknoCount);

        String recurrenceString = EventRecurrenceFormatter.getRepeatString(context, context.getResources(), recurrence, false);

        switch(recurrence.freq) {
            default:
            case 3:
                entry.setRecurrence(Recurrence.HOURLY);
                break;
            case 4:
                entry.setRecurrence(Recurrence.DAILY);
                break;
            case 5:
                entry.setRecurrence(Recurrence.WEEKLY);

                for (int i : recurrence.byday) {
                   // Logcat.d(betterPickerDayToWeekdayNo(i));
                    entry.getWeekdays()[betterPickerDayToWeekdayNo(i)] = true;
                }
                break;
            case 6:
                entry.setRecurrence(Recurrence.MONTHLY);
                if (recurrence.byday != null) {
                    entry.setDayOfMonth(false);
                } else {
                    entry.setDayOfMonth(true);
                }
                Logcat.d(entry.isDayOfMonth());
                break;
            case 7:
                entry.setRecurrence(Recurrence.YEARLY);
                break;
        }

        if ((recurrence.until == null) && (recurrence.count == 0)) {
            Logcat.d("setting forever true");
            entry.setForever(true);
            entry.setLimit(-1);
            entry.setEndDate(new DateTime().withDate(1970,1,1));
        } else if (recurrence.count != 0) {
            entry.setLimit(recurrence.count);
            entry.setForever(false);
            entry.setEndDate(new DateTime().withDate(1970,1,1));
            recurrenceString += "; for " + entry.getLimit() + (entry.getLimit() == 1 ? " time" : " times");
        } else {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
            try {
                Logcat.d(format.parse(recurrence.until).toString());
                entry.setForever(false);
                entry.setLimit(-1);
                entry.setEndDate(new DateTime(format.parse(recurrence.until)).withTime(23,59,59,00));
                Logcat.d(entry.getEndDate().toString());
                SimpleDateFormat printFormat = new SimpleDateFormat("dd.MM.YYYY");
                recurrenceString += " until " + printFormat.format(entry.getEndDate().toDate());
            } catch (Exception e) {
                e.printStackTrace();
                entry.setForever(true);
                entry.setLimit(-1);
                entry.setEndDate(new DateTime().withDate(1970,1,1));
            }
        }

        if (recurrence.interval == 0) {
            entry.setEvery(1);
        } else {
            entry.setEvery(recurrence.interval);
        }

        return recurrenceString;
    }

    private static int betterPickerDayToWeekdayNo(int day) {
        switch (day) {
            case EventRecurrence.SU:
                return 6;
            case EventRecurrence.MO:
                return 0;
            case EventRecurrence.TU:
                return 1;
            case EventRecurrence.WE:
                return 2;
            case EventRecurrence.TH:
                return 3;
            case EventRecurrence.FR:
                return 4;
            case EventRecurrence.SA:
                return 5;
            default:
                throw new IllegalArgumentException("bad day argument: " + day);
        }
    }

    private static String dayToString(int day, int dayOfWeekLength) {
        return DateUtils.getDayOfWeekString(dayToUtilDay(day), dayOfWeekLength);
    }

    /**
     * Converts EventRecurrence's day of week to DateUtil's day of week.
     *
     * @param day of week as an EventRecurrence value
     * @return day of week as a DateUtil value.
     */
    private static int dayToUtilDay(int day) {
        switch (day) {
            case EventRecurrence.SU:
                return Calendar.SUNDAY;
            case EventRecurrence.MO:
                return Calendar.MONDAY;
            case EventRecurrence.TU:
                return Calendar.TUESDAY;
            case EventRecurrence.WE:
                return Calendar.WEDNESDAY;
            case EventRecurrence.TH:
                return Calendar.THURSDAY;
            case EventRecurrence.FR:
                return Calendar.FRIDAY;
            case EventRecurrence.SA:
                return Calendar.SATURDAY;
            default:
                throw new IllegalArgumentException("bad day argument: " + day);
        }
    }

    public void onDoneClicked() {

        if (exerIds.size() == 0) {
            mView.showNoExerciseError();
            return;
        }

        entry.setExercises(new LinkedHashMap<>());

        Logcat.d("for all attr " + AttributeQuery.getAll(getRealm()).size());
        for (Attribute attribute : AttributeQuery.getAll(getRealm())) {
            //Attribute is selected
            if (attrIds.contains(attribute.getId())) {
                Logcat.d("attrId " + attribute.getId());
                for (Skill skill : attribute.getSkills()) {
                    //Skill is selected
                    if (skillIds.contains(skill.getId())) {
                        Logcat.d("skillId " + skill.getId());
                        for (Exercise exercise : skill.getExercises()) {
                            //Exercise is selected
                            if (exerIds.contains(exercise.getId())) {
                                Logcat.d("exerId " + exercise.getId());
                                //Entry already has selected attribute
                                if (entry.getExercises().containsKey(attribute.getId())) {
                                    //Entry already has selected skill
                                    if (entry.getExercises().get(attribute.getId()).containsKey(skill.getId())) {
                                        Logcat.d("adding exercise to existing");
                                        List<Integer> exerciseIds = entry.getExercises().get(attribute.getId()).get(skill.getId());
                                        exerciseIds.add(exercise.getId());
                                        entry.getExercises().get(attribute.getId()).remove(skill.getId());
                                        entry.getExercises().get(attribute.getId()).put(skill.getId(), exerciseIds);
                                    } else {
                                        Logcat.d("new skill entry group");
                                        ArrayList<Integer> exerciseIds = new ArrayList<>();
                                        exerciseIds.add(exercise.getId());
                                        entry.getExercises().get(attribute.getId()).put(skill.getId(), exerciseIds);
                                    }
                                } else {
                                    Logcat.d("new attr skill entry group");
                                    LinkedHashMap<Integer, List<Integer>> map = new LinkedHashMap();
                                    ArrayList<Integer> exerciseIds = new ArrayList<>();
                                    exerciseIds.add(exercise.getId());
                                    map.put(skill.getId(), exerciseIds);
                                    entry.getExercises().put(attribute.getId(), map);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (mMode == C.MODE_CREATE) {
            PlannerEntryQuery.copyToRealm(getRealm(), entry);
        } else {
            PlannerEntryQuery.edit(getRealm(), entry);
        }

        //AlarmUtils.scheduleAlarm(mView, entry);

        mView.finish();
        mView.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    public void scheduleAlarm(PlannerEntry entry) {

    }







}
