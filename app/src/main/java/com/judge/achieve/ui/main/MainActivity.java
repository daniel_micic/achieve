package com.judge.achieve.ui.main;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.transition.Fade;
import android.support.transition.Transition;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Profile;
import com.judge.achieve.ui.attribute.AttributeActivity;
import com.judge.achieve.ui.attributeform.AttributeFormActivity;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.goal.GoalActivity;
import com.judge.achieve.ui.planner.PlannerActivity;
import com.judge.achieve.ui.shop.ShopActivity;
import com.judge.achieve.ui.statistics.StatisticsActivity;
import com.judge.achieve.ui.tutorial.TutorialActivity;
import com.judge.achieve.utils.DialogUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MyAnimationListener;
import com.judge.achieve.utils.PrefUtils;
import com.judge.achieve.utils.ResourceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends CircularOverlayActivity implements AttributeAdapter.AttributeListener {

    public static int AVATAR_REQUEST_CODE = 666;

    private Uri outputFileUri;

    AlertDialog dialog;

    DialogUtils.AvatarChangeListener avatarChangeListener;

    public DialogUtils.AvatarChangeListener getAvatarChangeListener() {
        return avatarChangeListener;
    }

    public void setAvatarChangeListener(DialogUtils.AvatarChangeListener avatarChangeListener) {
        this.avatarChangeListener = avatarChangeListener;
    }

    // VIews
    @BindView(R.id.main_root)
    CoordinatorLayout root;

    @BindView(R.id.main_app_bar_layout)
    AppBarLayout appBarLayout;

    @BindView(R.id.main_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.main_toolbar)
    Toolbar toolbar;

    @BindView(R.id.name)
    TextView profileName;
    @BindView(R.id.level)
    TextView level;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.avatar)
    SimpleDraweeView avatar;

    @BindView(R.id.goals)
    LinearLayout goals;

    @BindView(R.id.shop)
    LinearLayout shop;

    @BindView(R.id.statistics)
    LinearLayout statistics;

    @BindView(R.id.planner)
    LinearLayout planner;

    @BindView(R.id.attribute_list)
    RecyclerView recyclerView;

    @BindView(R.id.attribute_add)
    FloatingActionButton addFab;

    @BindView(R.id.overlay)
    LinearLayout mOverlay;


    private MenuItem profileItem;
    private ImageView profileActioNView;

    AttributeAdapter adapter;

    //Logic and presenters
    MainPresenter presenter;
    private State state;

    private Handler handler = new Handler();

    private enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private DialogUtils.DialogListener shopListener = new DialogUtils.DialogListener() {
        @Override
        public void onPositiveClicked() {
            Intent intent = new Intent(MainActivity.this, ShopActivity.class);
            startActivity(intent);
        }

        @Override
        public void onNegativeClicked() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (PrefUtils.isFirstRun()) {
            Intent intent = new Intent(MainActivity.this, TutorialActivity.class);
            startActivity(intent);
            finish();
        } else {
            Logcat.d("shit");
        }

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initViews();

        presenter = new MainPresenter(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (presenter.isNewLevel()) {
            Logcat.d("new levele");
            ObjectAnimator animation = ObjectAnimator.ofInt(progress, "progress", 100);
            animation.setDuration(500);
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
            handler.postDelayed(() -> {
                progress.setProgress(0);
                level.setText("Level " + String.valueOf(presenter.getProfile().getLevel()));
                setViews(presenter.getProfile(), presenter.getAttributes());
            }, 500);
        } else {
            setViews(presenter.getProfile(), presenter.getAttributes());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())  {
            case R.id.edit_profile:
                String profilePic = getPresenter().getProfile().getProfilePic();
                Uri uri = profilePic == null ? null : Uri.parse(profilePic);
                dialog = DialogUtils.getEditProfileDialog(this, uri, presenter.getProfile().getName());
                dialog.show();
                break;
            case R.id.about:
                presenter.updateExerciseOnNewDay();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Logcat.d("result ok");
            if (requestCode == AVATAR_REQUEST_CODE) {
                Logcat.d("avatar");
                final boolean isCamera;
                if (data == null || data.getData() == null) {
                    Logcat.d("is camera");
                    isCamera = true;
                } else {
                    isCamera = false;
                }

                if (!isCamera) {
                    outputFileUri = data == null ? null : data.getData();
                    Logcat.d(outputFileUri.toString());
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(outputFileUri, filePathColumn, null, null, null);
                    if(cursor.moveToFirst()){
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        outputFileUri = Uri.parse("file://" + cursor.getString(columnIndex));
                        Logcat.d(outputFileUri.toString());
                    } else {
                        Snackbar.make(root, "Picture selection failed, please try again", Snackbar.LENGTH_SHORT).show();
                    }
                    cursor.close();
                }

                avatarChangeListener.onAvatarChanged(outputFileUri);
            }
        }
    }

    public boolean checkPermissions() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Do something for lollipop and above versions
            int requestCode = 555;
            String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            int permissionCheck = checkSelfPermission(permission);
            Logcat.d("checked for permission");
            if (!(permissionCheck == PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{permission}, requestCode);
            } else {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 555) {
            for (int i : grantResults) {
                if (i == PackageManager.PERMISSION_GRANTED) {
                    if (dialog != null) {
                        dialog.findViewById(R.id.dialog_avatar).performClick();
                    }
                } else {
                    Snackbar.make(root, "This functionality is not available without permissions", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public MainPresenter getPresenter() {
        return presenter;
    }

    @Override
    public View getOverlay() {
        return mOverlay;
    }

    @Override
    public void setOverlayGone() {
        mOverlay.setVisibility(View.GONE);
    }

    public void initViews() {
        setSupportActionBar(toolbar);

        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.transparent));

        goals.setOnClickListener(view -> {
            startActivity(new Intent(this, GoalActivity.class));
            overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
        });
        shop.setOnClickListener(view -> {
            Logcat.d("click on shop");
            Intent intent = new Intent(this, ShopActivity.class);
            startActivity(intent);

/*            InitialGenerator.generateProfile(getPresenter().getRealm());
                Profile profile = ProfileQuery.get(getPresenter().getRealm());
                profile.setLevel(1);
                profile.setPoints(0f);
                ProfileQuery.edit(getPresenter().getRealm(), profile);
*/
            }
        );
        //shop.getBackground().setColorFilter(ResourceUtil.getColor(R.color.white), PorterDuff.Mode.MULTIPLY);

        statistics.setOnClickListener(view -> {
            if (getPresenter().hasStatisticsAccess())
                if (getPresenter().checkForExercises()) {
                    startActivity(new Intent(this, StatisticsActivity.class));
                } else {
                    Snackbar.make(root, "No Exercise statistic data to show", Snackbar.LENGTH_SHORT).show();
                }
            else
                DialogUtils.getLockedDialog(this, C.iap_statistics, shopListener).show();

        });

        planner.setOnClickListener(view -> {
            if (getPresenter().hasPlannerAccess())
                startActivity(new Intent(this, PlannerActivity.class));
            else
                DialogUtils.getLockedDialog(this, C.iap_planner, shopListener).show();
        });

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        addFab.setOnClickListener(view ->  {
                if (getPresenter().canCreateAttirbute()) {
                    MyAnimationListener listener = new MyAnimationListener() {
                        @Override
                        public void onMyAnimationFinished() {
                            Intent intent = new Intent(MainActivity.this, AttributeFormActivity.class);
                            intent.putExtra(C.EXTRA_MODE, C.MODE_CREATE);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
                        }
                    };

                    final int x = (int) (addFab.getX() + (addFab.getWidth() / 2));
                    final int y = (int) (addFab.getY() + (addFab.getHeight() / 2));

                    addFab.animate()
                            .setInterpolator(new AccelerateDecelerateInterpolator())
                            .rotationBy(90)
                            .setDuration(150)
                            .withEndAction(() -> circularRevealActivity(x, y, listener));
                } else {
                    DialogUtils.getLockedDialog(this, C.iap_attributes, shopListener).show();
                }
            }
        );
    }

    public void setViews(Profile profile, ArrayList<Attribute> attributes) {
        if (profile == null || attributes == null) {
            //TODO shit happened
            return;
        }
        profileName.setText(profile.getName());
        level.setText("Level " + String.valueOf(profile.getLevel()));

        //TODO complete this beautiful animation
        ObjectAnimator animation = ObjectAnimator.ofInt(progress, "progress", profile.getProgressInPercentage());
        animation.setDuration(500); // 0.5 second
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        progress.setProgress(profile.getProgressInPercentage());

        if (profile.getProfilePic() != null) {
            Logcat.d(profile.getProfilePic());
            avatar.setImageURI(Uri.parse(profile.getProfilePic()));
        } else {
            /*Uri uri = new Uri.Builder()
                    .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                    .path(String.valueOf(R.drawable.avatar_placeholder))
                    .build();*/
            /*Uri uri = Uri.parse("android.resource://com.judge.achieve/drawable/avatar_placeholder.png");
            Logcat.d(uri.toString());
            avatar.setImageURI(uri);*/
        }

        adapter = new AttributeAdapter(this, attributes, this);
        recyclerView.setAdapter(adapter);

    }

    public void onEditProfile(String name) {
        getPresenter().onEditProfile(name, outputFileUri);
        profileName.setText(getPresenter().getProfile().getName());
        String profilePic = getPresenter().getProfile().getProfilePic();
        if (profilePic != null)
            avatar.setImageURI(Uri.parse(profilePic));
    }

    @Override
    public void onAttributeClicked(View view, final int id) {
        /*final int x = position[0] + (int)actualTouchPosition[0] - 2;
        final int y = position[1] + (int)actualTouchPosition[1] - actionBarHeight/2;*/
        //TODO complete this beautiful transition
        /*handler.postDelayed(() -> {
                    Intent intent = new Intent(MainActivity.this, AttributeActivity.class);
                    intent.putExtra(C.EXTRA_ATTRIBUTE_ID, id);
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(MainActivity.this, view, "attribute");
                    startActivity(intent, options.toBundle());//transitionOptions(MainActivity.this, view, "attribute"));
                //}, 0);*/
        final  MyAnimationListener listener = new MyAnimationListener() {
            @Override
            public void onMyAnimationFinished() {
                Intent intent = new Intent(MainActivity.this, AttributeActivity.class);
                intent.putExtra(C.EXTRA_ATTRIBUTE_ID, id);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
            }
        };

        handler.postDelayed(() -> circularRevealActivity(listener), 200);
    }

    @Override
    public void onAttributeDelete(int position, Attribute attribute) {

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(R.string.msg_delete_attr)
                .setPositiveButton(getString(R.string.yes),
                        (dialogInterface, i) -> {
                            deleteAttribute(position, attribute);
                        })
                .setNegativeButton(getString(R.string.no),
                        (dialogInterface, i) -> dialogInterface.cancel())
                .create();

        dialog.setOnShowListener(dialogInterface -> {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ResourceUtil.getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ResourceUtil.getColor(R.color.colorPrimary));
        });

        dialog.show();


    }

    public void deleteAttribute(int position, Attribute attribute) {
        presenter.onCategoryDelete(attribute);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, adapter.getItemCount());
    }

    @Override
    public void onAttributeEdit(Attribute attribute) {
        final int id = attribute.getId();

        final  MyAnimationListener listener = new MyAnimationListener() {
            @Override
            public void onMyAnimationFinished() {
                Intent intent = new Intent(MainActivity.this, AttributeFormActivity.class);
                intent.putExtra(C.EXTRA_MODE, C.MODE_EDIT);
                intent.putExtra(C.EXTRA_ATTRIBUTE_ID, id);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
            }
        };

        handler.postDelayed(() -> circularRevealActivity(listener), 0);
    }

    public Bundle transitionOptions(Activity activity, View transitionView, String transitionNameResId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return null;
        }

        Transition fade = new Fade();

        View decorView = activity.getWindow().getDecorView();
        View statusBar = decorView.findViewById(android.R.id.statusBarBackground);
        View navigationBar = decorView.findViewById(android.R.id.navigationBarBackground);
        //View appBarLayout = decorView.findViewById(**R.id.appbarlayout**);
        String transitionName =transitionNameResId;

        android.support.v4.util.Pair<View, String> p1 = android.support.v4.util.Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME);
        android.support.v4.util.Pair<View, String> p2 = android.support.v4.util.Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME);
        android.support.v4.util.Pair<View, String> p3 = android.support.v4.util.Pair.create(transitionView, transitionName);
        //noinspection unchecked - we're not worried about the "unchecked" conversion of List<Pair> to Pair[] here
        return ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, p1, p2, p3)
                .toBundle();
    }

    public void setOutputFileUri(Uri outputFileUri) {
        this.outputFileUri = outputFileUri;
    }


}
