package com.judge.achieve.ui.shop;

import android.content.Context;
import android.os.Handler;

import com.judge.achieve.common.C;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.IABUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.iabUtils.IabHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Daniel on 01.10.2016.
 */

public class ShopPresenter extends BasePresenter {

    String part1 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAphdGL3Qt9TDft5BWtK9ZGrl90p4R6ccjVf8NDY8IRiPn20go1p+jb/KH9VwxP+UxJ9LV8t09MM1xz/ic2mX9QitHwRNDgcfV9pFvBK7yyfkjv/5IUkaW368+T";
    String part2 = "EPmHspvWM2PXm/u4vFxerUTkG66S47G9cVCIV3wgxqv5ZyUqjiJ0PS52etR+B9CX200wcObX7wL9sdtECS0KRC8esWJU/2TUFfLCO8D4MIe5LdB5j";
    String part3 = "A/EU95bfAolCrRBRMs3VWiqDU9Ubs4lyYUN3Akm+9ZM+pL5afUXfTiRPST8pH8RibIApwJToXWUhoPRE71m70FVsqVOMOC4OmR/kOuZoGqdQIDAQAB";

    private ShopActivity mView;
    private Context mContext;

    private Attribute mAttribute;

    private IabHelper iabHelper;

    Handler handler = new Handler();

    public interface IABListener {
        void onSucess();
        void onFailure();
    }
    public ShopPresenter(ShopActivity view) {
        super();

        mView = view;
        mContext = view;

        iabHelper = new IabHelper(view, part1 + part2 + part3);
        iabHelper.startSetup(result -> {
            try {
                checkForIAP();
            } catch (IabHelper.IabAsyncInProgressException e) {
                e.printStackTrace();
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void setViews() {
    }

    public HashMap<String, Boolean> checkForIAP() throws IabHelper.IabAsyncInProgressException{
        if (iabHelper.isSetupDone() && !iabHelper.isAsyncInProgress()) {
            HashMap map = new HashMap();
            List<String> skus = new ArrayList<>();
            skus.add(C.iap_full_access);
            skus.add(C.iap_attributes);
            skus.add(C.iap_exercises);
            skus.add(C.iap_planner);
            skus.add(C.iap_statistics);
            iabHelper.queryInventoryAsync(true, skus, null, (result, inv) -> {

                if (inv == null) {
                    mView.showError2();
                    return;
                }

                if (inv.hasPurchase(C.iap_full_access)) {
                    Logcat.d("has full access");
                    IABUtils.setFulLAccess(true);
                }
                if (inv.hasPurchase(C.iap_attributes)) {
                    IABUtils.setAttributes(true);
                }
                if (inv.hasPurchase(C.iap_exercises)) {
                    IABUtils.setExercises(true);
                }
                if (inv.hasPurchase(C.iap_planner)) {
                    IABUtils.setPlanner(true);
                }
                if (inv.hasPurchase(C.iap_statistics)) {
                    IABUtils.setStatistics(true);
                }
                mView.setViews();
            });
            return map;
        }
        return null;
    }

    public void purchaseFullAccess(IABListener listener) {
        IabHelper.OnIabPurchaseFinishedListener listener1 = (result, info) -> {
            if (result.isSuccess()) {
                IABUtils.setFulLAccess(true);
                listener.onSucess();
            } else {
                mView.showError();
            }
        };

        try {
            iabHelper.launchPurchaseFlow(mView, C.iap_full_access, 100, listener1, null);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
            mView.showError();
        }
    }

    public void purchaseAttr(IABListener listener) {
        IabHelper.OnIabPurchaseFinishedListener listener1 = (result, info) -> {
            if (result.isSuccess()) {
                IABUtils.setAttributes(true);
                listener.onSucess();
            } else {
                mView.showError();
            }
        };

        try {
            iabHelper.launchPurchaseFlow(mView, C.iap_attributes, 101, listener1, null);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
            mView.showError();
        }

    }

    public void purchaseExer(IABListener listener) {
        IabHelper.OnIabPurchaseFinishedListener listener1 = (result, info) -> {
            if (result.isSuccess()) {
                IABUtils.setExercises(true);
                listener.onSucess();
            } else {
                mView.showError();
            }
        };

        try {
            iabHelper.launchPurchaseFlow(mView, C.iap_exercises, 101, listener1, null);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
            mView.showError();
        }
    }

    public void purchaseStat(IABListener listener) {
        IabHelper.OnIabPurchaseFinishedListener listener1 = (result, info) -> {
            if (result.isSuccess()) {
                IABUtils.setStatistics(true);
                listener.onSucess();
            } else {
                mView.showError();
            }
        };

        try {
            iabHelper.launchPurchaseFlow(mView, C.iap_statistics, 101, listener1, null);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
            mView.showError();
        }
    }

    public void purchasePlan(IABListener listener) {
        IabHelper.OnIabPurchaseFinishedListener listener1 = (result, info) -> {
            if (result.isSuccess()) {
                IABUtils.setPlanner(true);
                listener.onSucess();
            } else {
                mView.showError();
            }
        };

        try {
            iabHelper.launchPurchaseFlow(mView, C.iap_planner, 101, listener1, null);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
            mView.showError();
        }

    }




}
