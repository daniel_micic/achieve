package com.judge.achieve.ui.planner;

import android.util.Pair;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.ExerciseQuery;
import com.judge.achieve.persistence.query.PlannerEntryQuery;
import com.judge.achieve.persistence.query.SkillQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Daniel on 01.10.2016.
 */

public class PlannerPresenter extends BasePresenter implements EntryAdapter.PlannerEntryProvider{

    private PlannerActivity view;
    private List<PlannerEntry> entries;

    private DateTime selectedDay = DateTime.now();
    private DateTime currentMonth = DateTime.now();

    public PlannerPresenter(PlannerActivity view) {
        super();
        this.view = view;
        selectedDay = DateTime.now();
        currentMonth = DateTime.now();
    }

    @Override
    public void onResume() {
        super.onResume();
        Logcat.d(selectedDay.toString());
        entries = setEntriesForDay(selectedDay);
        Logcat.d(entries.size());
    }

    public List<PlannerEntry> setEntriesForDay(DateTime selectedDay) {
        Logcat.d("start");
        entries = new ArrayList<>();
        this.selectedDay = selectedDay;

        for (PlannerEntry entry : PlannerEntryQuery.getAll(getRealm())) {

            //entry = checkForDeletedObjects(entry);

            if (entry.getStartDate().isAfter(selectedDay.withTime(23,59,59,59))) {
                continue;
            }
            switch (entry.getRecurrence()) {
                case HOURLY:
                    generateHourlyEntry(entry, selectedDay);
                    break;
                case DAILY:
                    generateDailyEntry(entry, selectedDay);
                    break;
                case WEEKLY:
                    generateWeeklyEntry(entry, selectedDay);
                    break;
                case MONTHLY:
                    generateMonthlyEntry(entry, selectedDay);
                    break;
                case YEARLY:
                    generateYearlyEntry(entry, selectedDay);
                    break;
                case NEVER:
                    generateNeverOcurrence(entry, selectedDay);
                    break;

            }
        }

        Collections.sort(entries);
        Logcat.d("end");
        return entries;
    }

    private void generateNeverOcurrence(PlannerEntry e, DateTime selectedDay) {
        if (e.getStartDate().withTimeAtStartOfDay().isEqual(selectedDay.withTimeAtStartOfDay())) {
            entries.add(e);
        }
    }

    private void generateHourlyEntry(PlannerEntry e, DateTime selectedDay) {
        // either midnight if entry startdate is before selected day or entry start date if its on selected day
        DateTime newStartTime = selectedDay.withTimeAtStartOfDay();
        //e.setStartDate(DateTime.now().minusDays(1).withTime(20,28,0,0));
        //how many times the entry was repeated before selected day
        long beforeTheDay = 0;
        //reccurence in minutes
        long every = e.getEvery() * 60;
        //how many minutes from midnight should be first occurence of entry (calculated and used only if entry startdate was before selected day)
        long minutesFromMidnight = 0;

        if (e.getStartDate().isBefore(selectedDay.withTimeAtStartOfDay())) {
            Logcat.d("starts before selected day");
            newStartTime = selectedDay.withTimeAtStartOfDay();

            long minutesFromStart = new Duration(e.getStartDate(), newStartTime).getStandardMinutes();
            // correction for entry that is exactly on midnight
            if (minutesFromStart % every == 0) {
                beforeTheDay = minutesFromStart / every;
                minutesFromMidnight = 0;
            } else {
                beforeTheDay = (minutesFromStart / every) + 1;
                minutesFromMidnight = every - (minutesFromStart % every);
            }
        } else if ((e.getStartDate().isAfter(selectedDay.withTimeAtStartOfDay()) || e.getStartDate().isEqual(selectedDay.withTimeAtStartOfDay())) &&
                e.getStartDate().isBefore(selectedDay.withTime(23,59,59,59))) {
            Logcat.d("starts on selected day");
            newStartTime = e.getStartDate();
        } else {
            Logcat.d("starts after selected day");
            return;
        }

        long minutesFromStart = new Duration(e.getStartDate(), selectedDay).getStandardMinutes();

        Logcat.d(minutesFromStart);
        Logcat.d(beforeTheDay);
        Logcat.d(e.getLimit());
        Logcat.d(minutesFromMidnight);

        if (e.isForever())  {
            Logcat.d("is Forever");
            boolean first = true;
            do {
                if (first) {
                    first = false;
                    newStartTime = newStartTime.plusMinutes((int)minutesFromMidnight);
                    Logcat.d(newStartTime.toString());
                } else {
                    newStartTime = newStartTime.plusMinutes((int)every);
                    Logcat.d(newStartTime.toString());
                }

                if (newStartTime.isBefore(selectedDay.withTime(23,59,59,59))) {
                    entries.add(new PlannerEntry(e, newStartTime));
                } else {
                    break;
                }
            } while (newStartTime.isBefore(selectedDay.withTime(23,59,59,59)));
        } else if ((e.getLimit() != -1) && (beforeTheDay < e.getLimit())) {
            Logcat.d("is in limit");
            for (int i = 0; i < (e.getLimit() - beforeTheDay); i++) {
                if (i == 0) {
                    newStartTime = newStartTime.plusMinutes((int)minutesFromMidnight);
                    Logcat.d(newStartTime.toString());
                } else {
                    newStartTime = newStartTime.plusMinutes((int)every);
                    Logcat.d(newStartTime.toString());
                }

                if (newStartTime.isBefore(selectedDay.withTime(23,59,59,59))) {
                    entries.add(new PlannerEntry(e, newStartTime));
                } else {
                    break;
                }
            }
        } else if (selectedDay.isBefore(e.getEndDate())) {
            Logcat.d("is until");
            boolean first = true;
            do {
                if (first) {
                    first = false;
                    newStartTime = newStartTime.plusMinutes((int)minutesFromMidnight);
                    Logcat.d(newStartTime.toString());
                } else {
                    newStartTime = newStartTime.plusMinutes((int)every);
                    Logcat.d(newStartTime.toString());
                }

                if (newStartTime.isBefore(e.getEndDate()) && newStartTime.isBefore(selectedDay.withTime(23,59,59,59))) {
                    entries.add(new PlannerEntry(e, newStartTime));
                } else {
                    break;
                }
            } while (newStartTime.isBefore(e.getEndDate()) && newStartTime.isBefore(selectedDay.withTime(23,59,59,59)));
        } else {
            Logcat.d("no condition");
        }
    }

    private void generateDailyEntry(PlannerEntry e, DateTime selectedDay) {
        selectedDay = selectedDay.withTime(e.getStartDate().getHourOfDay(), e.getStartDate().getMinuteOfHour(),0,0);

        int daysDifference = Days.daysBetween(e.getStartDate(), selectedDay).getDays();
        //new Duration(e.getStartDate(), selectedDay).toStandardDays().getDays();

        if (e.isForever() ||
           ((e.getLimit() != -1) && (daysDifference / e.getEvery() < e.getLimit())) ||
           (selectedDay.isBefore(e.getEndDate())))
        {
            if (daysDifference % e.getEvery() == 0) {
                e.setStartDate(e.getStartDate().withDate(selectedDay.getYear(), selectedDay.getMonthOfYear(), selectedDay.getDayOfMonth()));
                entries.add(e);
            }
        }
    }

    private void  generateWeeklyEntry(PlannerEntry e, DateTime selectedDay) {
        selectedDay = selectedDay.withTime(e.getStartDate().getHourOfDay(), e.getStartDate().getMinuteOfHour(),0,0);

        int weeksDifference = Weeks.weeksBetween(e.getStartDate().minusDays(e.getStartDate().getDayOfWeek()), selectedDay).getWeeks();
        Logcat.d(weeksDifference);

        if (e.isForever() ||
           ((e.getLimit() != -1) && (weeksDifference / e.getEvery() < e.getLimit())) ||
           (selectedDay.isBefore(e.getEndDate())))
        {
            Logcat.d("until condition passed");
            if (weeksDifference % e.getEvery() == 0) {
                Logcat.d("recurrence condition passed");
                if (e.getWeekdays()[selectedDay.getDayOfWeek() - 1]) {
                    Logcat.d("dayOfWeek condition passed " + selectedDay.getDayOfWeek());
                    e.setStartDate(e.getStartDate().withDate(selectedDay.getYear(), selectedDay.getMonthOfYear(), selectedDay.getDayOfMonth()));
                    entries.add(e);
                }
            }
        }
    }

    private void generateMonthlyEntry(PlannerEntry e, DateTime selectedDay) {
        selectedDay = selectedDay.withTime(e.getStartDate().getHourOfDay(), e.getStartDate().getMinuteOfHour(),0,0);
        int monthsDifference = Months.monthsBetween(e.getStartDate(), selectedDay).getMonths();
        Logcat.d(monthsDifference);

        if (e.isForever() ||
           ((e.getLimit() != -1) && (monthsDifference / e.getEvery() < e.getLimit())) ||
           (selectedDay.isBefore(e.getEndDate())))
        {
            Logcat.d("until condition passed");
            if (monthsDifference % e.getEvery() == 0) {
                if (e.isDayOfMonth()) {
                    Logcat.d("selected " + selectedDay.getDayOfMonth());
                    Logcat.d("selected " + e.getStartDate().getDayOfMonth());
                    if (selectedDay.getDayOfMonth() == e.getStartDate().getDayOfMonth()) {
                        Logcat.d("recurrence condition passed and day of month");
                        e.setStartDate(e.getStartDate().withDate(selectedDay.getYear(), selectedDay.getMonthOfYear(), selectedDay.getDayOfMonth()));
                        entries.add(e);
                    }
                } else if (
                        // same week and same day of the week
                        (e.getStartDate().getDayOfMonth() / 7 == selectedDay.getDayOfMonth() / 7) &&
                        (e.getStartDate().getDayOfWeek() == selectedDay.getDayOfWeek())
                        )
                {
                    Logcat.d("recurrence condition passed and specific day");
                    e.setStartDate(e.getStartDate().withDate(selectedDay.getYear(), selectedDay.getMonthOfYear(), selectedDay.getDayOfMonth()));
                    entries.add(e);
                }
            }
        }
    }

    private void generateYearlyEntry(PlannerEntry e, DateTime selectedDay) {
        selectedDay = selectedDay.withTime(e.getStartDate().getHourOfDay(), e.getStartDate().getMinuteOfHour(),0,0);
        int yearsDifference = Years.yearsBetween(e.getStartDate(), selectedDay).getYears();
        Logcat.d(yearsDifference);

        Logcat.d(selectedDay.getDayOfMonth());
        Logcat.d(e.getStartDate().getDayOfMonth());

        Logcat.d(selectedDay.getMonthOfYear());
        Logcat.d(e.getStartDate().getMonthOfYear());

        if (e.isForever() ||
           ((e.getLimit() != -1) && (yearsDifference / e.getEvery() < e.getLimit())) ||
           (selectedDay.isBefore(e.getEndDate())))
        {
            Logcat.d("until condition passed");
            if ((yearsDifference % e.getEvery() == 0) &&
                (selectedDay.getDayOfMonth() == e.getStartDate().getDayOfMonth()) &&
                (selectedDay.getMonthOfYear() == e.getStartDate().getMonthOfYear()))
            {
                Logcat.d("recurrence condition passed");
                e.setStartDate(e.getStartDate().withDate(selectedDay.getYear(), selectedDay.getMonthOfYear(), selectedDay.getDayOfMonth()));
                entries.add(e);
            }
        }
    }

    public PlannerEntry checkForDeletedObjects(PlannerEntry e) {

        Logcat.d("start");
        List<Integer> attrsToDelete = new ArrayList<>();
        List<Integer> skillsToDelele = new ArrayList<>();
        List<Integer> exersToDelete = new ArrayList<>();

        for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attrs : e.getExercises().entrySet()) {
            if (AttributeQuery.getById(getRealm(), attrs.getKey()) == null) {
                attrsToDelete.add(attrs.getKey());
            } else {
                for (Map.Entry<Integer, List<Integer>> skills : attrs.getValue().entrySet()) {
                    if (SkillQuery.getById(getRealm(), skills.getKey()) == null) {
                        skillsToDelele.add(skills.getKey());
                    } else {
                        for (Integer exerId : skills.getValue()) {
                            if (ExerciseQuery.getById(getRealm(), exerId) == null) {
                                exersToDelete.add(exerId);
                            }
                        }
                    }

                }
            }
        }

        for (Integer id : attrsToDelete) {
            e.getExercises().remove(id);
        }

        for (Integer id : skillsToDelele) {
            for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attrs : e.getExercises().entrySet()) {
                attrs.getValue().remove(id);
            }
        }

        for (Integer id : exersToDelete) {
            for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attrs : e.getExercises().entrySet()) {
                for (Map.Entry<Integer, List<Integer>> skills : attrs.getValue().entrySet()) {
                    Logcat.d(skills.getValue().remove(id));
                }
            }
        }

        long newId = PlannerEntryQuery.edit(getRealm(), e);

        Logcat.d("end");
        return PlannerEntryQuery.getById(getRealm(), newId);
    }

    @Override
    public PlannerEntry getEntryForPosition(int position) {
        return entries.get(position);
    }

    @Override
    public HashMap<Integer, Boolean> getAttributeColorMap(int position) {
        PlannerEntry entry = entries.get(position);
        HashMap<Integer, Boolean> colors = new HashMap<>();
        for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> mapEntry : entry.getExercises().entrySet()) {
            colors.put(AttributeQuery.getById(getRealm(), mapEntry.getKey()).getPosition(), true);
        }
        return colors;
    }

    @Override
    public Pair<Integer, Pair<Integer, List<Integer>>> getEntryGroupForList(int entryPosition, int groupPosition) {
        return entries.get(entryPosition).getEntryForList().get(groupPosition);
    }

    @Override
    public String getExerciseName(int id, int skillId) {
        if (id == C.ALL) {
            String exercises = "";
            boolean first = true;

            for (Exercise e : SkillQuery.getById(getRealm(), skillId).getExercises()) {
                if (!first) {
                    exercises += System.getProperty("line.separator");
                }
                exercises += e.getTitle();
                first = false;
            }
            return exercises;
        } else {
            try {
                return ExerciseQuery.getById(getRealm(), id).getTitle();
            } catch (Exception e ){
                e.printStackTrace();
                return "Unavailable";
            }
        }
    }

    @Override
    public String getSkillName(int id) {
        if (id == C.ALL) {
            return "All";
        } else {
            try {
                return SkillQuery.getById(getRealm(), id).getTitle();
            } catch (Exception e ){
                e.printStackTrace();
                return "Unavailable";
            }
        }
    }

    @Override
    public String getAttributeName(int id) {
        if (id == C.ALL) {
            return "All";
        } else {
            try {
                return AttributeQuery.getById(getRealm(), id).getTitle();
            } catch (Exception e ){
                e.printStackTrace();
                return "Unavailable";
            }
        }
    }

    @Override
    public int getAttributeColor(int id) {
        try {
            switch (AttributeQuery.getById(getRealm(), id).getPosition()) {
                default:
                case 0:
                    return ResourceUtil.getColor(R.color.attribute1);
                case 1:
                    return ResourceUtil.getColor(R.color.attribute2);
                case 2:
                    return ResourceUtil.getColor(R.color.attribute3);
                case 3:
                    return ResourceUtil.getColor(R.color.attribute4);
            }
        } catch (Exception e ){
            e.printStackTrace();
            return ResourceUtil.getColor(R.color.attribute1);
        }
    }

    @Override
    public String getEntryTime(int position) {
        DateTime time = entries.get(position).getStartDate();
        return (time.getHourOfDay() < 10 ? "0" + time.getHourOfDay() : time.getHourOfDay())
                + ":" +
                (time.getMinuteOfHour() < 10 ? "0" + time.getMinuteOfHour() : time.getMinuteOfHour());
    }

    @Override
    public String getSelectedDateFormat() {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("d MMMM yyyy");
        return dtf.print(getSelectedDay());
    }

    @Override
    public int getEntryCount() {
        return entries.size();
    }

    @Override
    public int getGroupForEntryCount(int position) {
        if (entries != null && !entries.isEmpty()) {
            return entries.get(position).getEntryForList().size();
        } else {
            return 0;
        }
    }

    @Override
    public void deleteEntry(long id) {
        PlannerEntryQuery.delete(getRealm(), id);
        setEntriesForDay(selectedDay);
    }

    @Override
    public void editEntry(long id) {
        Logcat.d("editEntry");
        view.onEntryEdit(id);
    }

    public DateTime getSelectedDay() {
        if (selectedDay == null) {
            return DateTime.now();
        } else {
            return selectedDay;
        }
    }

    public void setSelectedDay(DateTime selectedDay) {
        this.selectedDay = selectedDay;
    }

    public DateTime getCurrentMonth() {
        if (currentMonth == null) {
            return DateTime.now();
        } else {
            return currentMonth;
        }
    }

    public void setCurrentMonth(DateTime currentMonth) {
        this.currentMonth = currentMonth;
    }
}
