package com.judge.achieve.ui.entryform.viewpager.picker;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ItemExerPickerBinding;

import java.util.List;

/**
 * Created by Daniel on 27.02.2017.
 */

public class PickerAdapter extends RecyclerView.Adapter<PickerAdapter.ExerPickerViewHolder> {

    ExerPickerAdapterType type;

    ExerPickerProvider provider;

    ExerPickerListener listener;

    int attrId;
    int skillId;

    public interface ExerPickerProvider {
        List<PickerItem> getAttributes();
        List<PickerItem> getSkills();
        List<PickerItem> getExercises();
    }

    public interface ExerPickerListener {
        void onSelected(boolean checked, int exId);
    }


    public enum ExerPickerAdapterType {
        ATTR,
        SKILL,
        EXER;
    }

    PickerAdapter(ExerPickerAdapterType type,
                  ExerPickerProvider provider,
                  ExerPickerListener listener) {
        this.type = type;
        this.provider = provider;
        this.listener = listener;
    }


    @Override
    public PickerAdapter.ExerPickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemExerPickerBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_exer_picker, parent, false);
        ExerPickerViewHolder vh = new ExerPickerViewHolder(binding);
        return vh;
    }

    @Override
    public void onBindViewHolder(PickerAdapter.ExerPickerViewHolder vh, int position) {

        switch (type) {
            case ATTR:
                vh.binding.parentTitle.setVisibility(View.GONE);
                vh.binding.exerPickerTitle.setText(provider.getAttributes().get(position).getTitle());
                vh.binding.exerPickerCheckbox.setChecked(provider.getAttributes().get(position).isChecked());
                vh.binding.exerPickerCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
                    listener.onSelected(b, provider.getAttributes().get(position).getId());
                });
                break;
            case SKILL:
                vh.binding.parentTitle.setVisibility(provider.getSkills().get(position).isHasParentTitle() ? View.VISIBLE : View.GONE);
                vh.binding.parentTitle.setText(provider.getSkills().get(position).getParentTitle());
                vh.binding.exerPickerTitle.setText(provider.getSkills().get(position).getTitle());
                vh.binding.exerPickerCheckbox.setChecked(provider.getSkills().get(position).isChecked());
                vh.binding.exerPickerCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
                    listener.onSelected(b, provider.getSkills().get(position).getId());
                });
                break;
            case EXER:
                vh.binding.parentTitle.setVisibility(provider.getExercises().get(position).isHasParentTitle() ? View.VISIBLE : View.GONE);
                vh.binding.parentTitle.setText(provider.getExercises().get(position).getParentTitle());
                vh.binding.exerPickerTitle.setText(provider.getExercises().get(position).getTitle());
                vh.binding.exerPickerCheckbox.setChecked(provider.getExercises().get(position).isChecked());
                vh.binding.exerPickerCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
                    listener.onSelected(b, provider.getExercises().get(position).getId());
                });
                break;
        }


    }

    @Override
    public int getItemCount() {
        switch (type) {
            case ATTR:
                return provider.getAttributes().size();
            case SKILL:
                return provider.getSkills().size();
            case EXER:
                return provider.getExercises().size();
            default:
                return 0;
        }
    }

    public class ExerPickerViewHolder extends RecyclerView.ViewHolder{

        ItemExerPickerBinding binding;

        public ExerPickerViewHolder(ItemExerPickerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}