package com.judge.achieve.ui.attribute;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.ActivityAttributeBinding;
import com.judge.achieve.databinding.DialogInputBinding;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.exerciseform.ExerciseFormActivity;
import com.judge.achieve.ui.planner.PlannerActivity;
import com.judge.achieve.ui.shop.ShopActivity;
import com.judge.achieve.ui.statistics.StatisticsActivity;
import com.judge.achieve.utils.DialogUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MyAnimationListener;
import com.judge.achieve.utils.ResourceUtil;

public class AttributeActivity extends CircularOverlayActivity implements ExerciseAdapter.ExerciseListener,
        SkillAdapter.SkillListener {

    private static String TAG = AttributeActivity.class.getName();

    ActivityAttributeBinding binding;

    private ActionMode actionMode;

    private AttributePresenter presenter;

    private SkillAdapter adapter;

    private Handler handler = new Handler();

    private DialogUtils.DialogListener shopListener = new DialogUtils.DialogListener() {
        @Override
        public void onPositiveClicked() {
            Intent intent = new Intent(AttributeActivity.this, ShopActivity.class);
            startActivity(intent);
        }

        @Override
        public void onNegativeClicked() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        int id;
        if (savedInstanceState == null) {
            id = getIntent().getExtras().getInt(C.EXTRA_ATTRIBUTE_ID);
        } else {
            id = savedInstanceState.getInt(C.SAVED_INSTANCE_CATEGORY_ID);
        }

        presenter = new AttributePresenter(this, id);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_attribute);

        initViews();
        setViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //DialogUtils.getLevelUpDialog(this, 5).show();

        /*if (adapter == null) {
            adapter = new SkillAdapter(this, presenter.getAttributeSkills(), getPresenter(), getPresenter());
            binding.skillList.setAdapter(adapter);
        } else {
            binding.skillList.removeAllViews();
            adapter.notifyDataSetChanged();
        }*/

        Logcat.d("data set");
        if (actionMode != null) {
            adapter.setActionMode(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        adapter.cancelAnimations();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == C.RESULT_OK) {
            presenter.update();

            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstance(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.category_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (adapter.isAnimating())
            return super.onOptionsItemSelected(item);

        actionMode = startSupportActionMode(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.category_action_mode_menu, menu);
                mode.setTitle(getString(R.string.edit_or_delete));
                adapter.setActionMode(true);
                adapter.notifyItemRangeChanged(0, adapter.getItemCount());
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                actionMode.finish();
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                adapter.setActionMode(false);
                adapter.notifyItemRangeChanged(0, adapter.getItemCount());
                actionMode = null;
            }
        });
        return super.onOptionsItemSelected(item);
    }

    @Override
    public AttributePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setOverlayGone() {
        if (binding != null) {
            binding.overlay.setVisibility(View.GONE);
        }
    }

    @Override
    public View getOverlay() {
        if (binding != null) {
            return binding.overlay;
        }
        else return null;
    }

    private void initViews() {
        setSupportActionBar(binding.attributeToolbar);
        setTitle(presenter.getAttributeTitle());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        binding.attributeToolbar.setNavigationOnClickListener(view -> AttributeActivity.this.onBackPressed());

        binding.skillList.setLayoutManager(new LinearLayoutManager(this));
        binding.skillList.setNestedScrollingEnabled(false);
        //binding.skillList.setItemAnimator(new MyItemAnimator());

        RecyclerView.ItemAnimator animator = binding.skillList.getItemAnimator();

        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        binding.exerciseAdd.setOnClickListener(view -> {
            if (getPresenter().canCreateExercise()) {
                MyAnimationListener listener = new MyAnimationListener() {
                    @Override
                    public void onMyAnimationFinished() {
                        Intent intent = new Intent(AttributeActivity.this, ExerciseFormActivity.class);
                        intent.putExtra(C.EXTRA_MODE, C.MODE_CREATE);
                        intent.putExtra(C.EXTRA_ATTRIBUTE_ID, presenter.getAttributeId());
                        startActivityForResult(intent, C.MODE_CREATE);
                        overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
                        finishActionMode();
                    }
                };

                final int x = (int) (binding.exerciseAdd.getX() + (binding.exerciseAdd.getWidth() / 2));
                final int y = (int) (binding.exerciseAdd.getY() + (binding.exerciseAdd.getHeight() / 2));

                circularRevealActivity(x, y, listener);
            } else {
                DialogUtils.getLockedDialog(AttributeActivity.this, C.iap_exercises, shopListener).show();
            }
        });
    }

    private void setViews() {
        adapter = new SkillAdapter(this, presenter);
        binding.skillList.setAdapter(adapter);
    }

    @Override
    public void onSkillEditClick(int skillPosition) {

        DialogInputBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_input, null, false);
        binding.input.setText(presenter.getSkillForPosition(skillPosition).getTitle());
        binding.input.setHint(ResourceUtil.getString(R.string.enter_skill_title));

        new AlertDialog.Builder(this)
                .setTitle(ResourceUtil.getString(R.string.edit_skill))
                .setView(binding.getRoot())
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    String title = binding.input.getText().toString();
                    presenter.editSkill(title, skillPosition);
                    adapter.notifyItemChanged(skillPosition, true);
                    finishActionMode();
                }).setNegativeButton("Cancel", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                }).show();
    }

    @Override
    public void onSkillDeleteClick(int skillPosition) {

        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_delete_skill)
                .setPositiveButton(getString(R.string.yes),
                        (dialogInterface, i) -> {
                            presenter.deleteSkill(skillPosition);
                            adapter.notifyItemRemoved(skillPosition);
                            adapter.notifyDataSetChanged();
                        })
                .setNegativeButton(getString(R.string.no),
                        (dialogInterface, i) -> dialogInterface.cancel())
                .show();
    }

    @Override
    public void onSkillPlannerClick(int skillPosition) {
        if (getPresenter().hasPlannerAccess()) {
            Intent intent = new Intent(AttributeActivity.this, PlannerActivity.class);
            intent.putExtra(C.EXTRA_ATTRIBUTE_ID, presenter.getAttributeId());
            intent.putExtra(C.EXTRA_SKILL_ID, presenter.getSkillForPosition(skillPosition).getId());
            startActivity(intent);
        } else {
            DialogUtils.getLockedDialog(this, C.iap_planner, shopListener).show();
        }
    }

    @Override
    public void onSkillStatisticsClick(int skillPosition) {
        if (getPresenter().hasStatisticsAccess()) {
            Intent intent = new Intent(AttributeActivity.this, StatisticsActivity.class);
            intent.putExtra(C.EXTRA_ATTRIBUTE_ID, presenter.getAttributeId());
            intent.putExtra(C.EXTRA_SKILL_ID, presenter.getSkillForPosition(skillPosition).getId());
            Logcat.d(presenter.getAttributeId());
            Logcat.d(presenter.getSkillForPosition(skillPosition).getId());
            startActivity(intent);
        } else {
            DialogUtils.getLockedDialog(this, C.iap_statistics, shopListener).show();
        }
    }

    @Override
    public void onExerciseDoneClick(final Exercise e,
                                    int skillPosition) {
        presenter.doExercise(e, skillPosition);
        adapter.notifyItemChanged(skillPosition, true);
    }

    @Override
    public void onExerciseEditClick(Exercise exercise, Integer skillId ) {
        MyAnimationListener listener = new MyAnimationListener() {
            @Override
            public void onMyAnimationFinished() {
                Intent intent = new Intent(AttributeActivity.this, ExerciseFormActivity.class);
                intent.putExtra(C.EXTRA_ATTRIBUTE_ID, presenter.getAttributeId());
                intent.putExtra(C.EXTRA_SKILL_ID, skillId);
                intent.putExtra(C.EXTRA_EXERCISE_ID, exercise.getId());
                intent.putExtra(C.EXTRA_MODE, C.MODE_EDIT);
                startActivityForResult(intent, C.MODE_EDIT);
                overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
                finishActionMode();
            }
        };

        handler.postDelayed(() -> circularRevealActivity(listener), 200);
    }

    @Override
    public void onExerciseDeleteClick(Exercise exercise, int exercisePosition, int skillPosition) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_delete_exercise)
                .setPositiveButton(getString(R.string.yes),
                        (dialogInterface, i) -> {
                            presenter.deleteExercise(exercise, exercisePosition, skillPosition);
                            adapter.notifyItemChanged(skillPosition);
                        })
                .setNegativeButton(getString(R.string.no),
                        (dialogInterface, i) -> dialogInterface.cancel())
                .show();
    }

    public void finishActionMode() {
        if (actionMode != null) {
            actionMode.finish();
        }
    }



    /*private class MyItemAnimator extends RecyclerView.ItemAnimator {

    }*/
}
