package com.judge.achieve.ui.intro;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ActivityIntroFormBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.ui.main.MainActivity;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;

import static com.judge.achieve.ui.main.MainActivity.AVATAR_REQUEST_CODE;

public class IntroFormActivity extends BaseActivity {


    ActivityIntroFormBinding binding;
    IntroFormPresenter mPresenter;

    int mMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_intro_form);

        mPresenter = new IntroFormPresenter(this);

        initViews();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPresenter.onSaveInstance(outState);
    }

    @Override
    public IntroFormPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onBackPressed() {
        Logcat.d("back pressed");
        if (binding.viewPager.getCurrentItem() == 1) {
            binding.viewPager.setCurrentItem(0, true);
            handleFabs();
        }
    }

    public boolean checkPermissions() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // Do something for lollipop and above versions
            int requestCode = 555;
            String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            int permissionCheck = checkSelfPermission(permission);
            Logcat.d("checked for permission");
            if (!(permissionCheck == PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{permission}, requestCode);
            } else {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 555) {
            for (int i : grantResults) {
                if (i == PackageManager.PERMISSION_GRANTED) {
                    getPresenter().pickPhoto();
                } else {
                    Snackbar.make(binding.getRoot(), "This functionality is not available without permissions", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Logcat.d("result ok");
            if (requestCode == AVATAR_REQUEST_CODE) {
                Logcat.d("avatar");
                final boolean isCamera;
                if (data == null || data.getData() == null) {
                    Logcat.d("is camera");
                    isCamera = true;
                } else {
                    isCamera = false;
                }

                if (!isCamera) {
                    getPresenter().setPhotoUri(data == null ? null : data.getData());
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(getPresenter().getPhotoUri(), filePathColumn, null, null, null);
                    if(cursor.moveToFirst()){
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        getPresenter().setPhotoUri(Uri.parse("file://" + cursor.getString(columnIndex)));
                    } else {
                        Snackbar.make(binding.getRoot(), "Picture selection failed, please try again", Snackbar.LENGTH_SHORT).show();
                    }
                    cursor.close();
                }

                binding.avatar.setImageURI(getPresenter().getPhotoUri());
            }
        }
    }

/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
*/
    private void initViews() {

        binding.profileNextBtn.show();

        binding.viewPager.setAdapter(new IntroFormPagerAdapter(this));
        binding.viewPager.setCurrentItem(0);
        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                handleFabs();
            }

            @Override
            public void onPageSelected(int position) {
                handleFabs();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.profileNextBtn.setOnClickListener(view -> {
            if (binding.viewPager.getCurrentItem() == 0) {
                getPresenter().saveProfile(binding.profileNameEt.getText().toString());
                Snackbar.make(binding.getRoot(), "You are now known as " + getPresenter().getProfile().getName(), BaseTransientBottomBar.LENGTH_SHORT).show();
                binding.viewPager.setCurrentItem(1, true);
                handleFabs();
            }
        });

        binding.exampleBtn.getBackground().setColorFilter(ResourceUtil.getColor(R.color.attribute1), PorterDuff.Mode.MULTIPLY);
        binding.exampleBtn.setOnClickListener(view -> {
            getPresenter().onExamplePicked();
        });

        binding.templateBtn.getBackground().setColorFilter(ResourceUtil.getColor(R.color.attribute2), PorterDuff.Mode.MULTIPLY);
        binding.templateBtn.setOnClickListener(view -> {
            getPresenter().onTemplatePicked();
        });

        binding.emptyBtn.getBackground().setColorFilter(ResourceUtil.getColor(R.color.attribute3), PorterDuff.Mode.MULTIPLY);
        binding.emptyBtn.setOnClickListener(view -> {
            getPresenter().onEmptyPicked();
        });

        binding.avatar.setOnClickListener(view -> {
            if (checkPermissions()) {
                getPresenter().pickPhoto();
            }
        });


    }

    private void handleFabs() {
        if (binding.viewPager.getCurrentItem() == 0) {
            binding.profileNextBtn.show();
        } else {
            binding.profileNextBtn.hide();
        }
    }

    public void goToMain() {
        Intent intent = new Intent(IntroFormActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

}
