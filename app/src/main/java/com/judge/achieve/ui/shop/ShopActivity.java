package com.judge.achieve.ui.shop;

import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.judge.achieve.R;
import com.judge.achieve.databinding.ActivityShopBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.IABUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;
import com.judge.achieve.utils.iabUtils.IabHelper;

public class ShopActivity extends BaseActivity {


    private static final String itemListId = "ITEM_ID_LIST";
    ActivityShopBinding binding;

    ShopPresenter mPresenter;

    ServiceConnection connection;
    IInAppBillingService inAppBillingService;

    Handler handler = new Handler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);

        mPresenter = new ShopPresenter(this);

        initViews();

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mPresenter.checkForIAP();
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (inAppBillingService != null) {
            unbindService(connection);
        }

    }

    @Override
    public ShopPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        binding.fullBuy.setOnClickListener(view -> getPresenter().purchaseFullAccess(new ShopPresenter.IABListener() {
            @Override
            public void onSucess() {
                binding.fullBuy.setVisibility(View.GONE);
                binding.fullCheck.setVisibility(View.VISIBLE);
                binding.attrBuy.setVisibility(View.GONE);
                binding.attrCheck.setVisibility(View.VISIBLE);
                binding.exerBuy.setVisibility(View.GONE);
                binding.exerCheck.setVisibility(View.VISIBLE);
                binding.statBuy.setVisibility(View.GONE);
                binding.statCheck.setVisibility(View.VISIBLE);
                binding.planBuy.setVisibility(View.GONE);
                binding.planCheck.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        }));
        binding.shopIconBgFull.getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        binding.attrBuy.setOnClickListener(view -> getPresenter().purchaseAttr(new ShopPresenter.IABListener() {
            @Override
            public void onSucess() {
                binding.attrBuy.setVisibility(View.GONE);
                binding.attrCheck.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        }));
        binding.shopIconBgAttr.getBackground().setColorFilter(ResourceUtil.getCategoryColor(1), PorterDuff.Mode.MULTIPLY);


        binding.exerBuy.setOnClickListener(view -> getPresenter().purchaseExer(new ShopPresenter.IABListener() {
            @Override
            public void onSucess() {
                binding.exerBuy.setVisibility(View.GONE);
                binding.exerCheck.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        }));
        binding.shopIconBgExer.getBackground().setColorFilter(ResourceUtil.getCategoryColor(2), PorterDuff.Mode.MULTIPLY);


        binding.statBuy.setOnClickListener(view -> getPresenter().purchaseStat(new ShopPresenter.IABListener() {
            @Override
            public void onSucess() {
                binding.statBuy.setVisibility(View.GONE);
                binding.statCheck.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        }));
        binding.shopIconBgStat.getBackground().setColorFilter(ResourceUtil.getCategoryColor(3), PorterDuff.Mode.MULTIPLY);


        binding.planBuy.setOnClickListener(view -> getPresenter().purchasePlan(new ShopPresenter.IABListener() {
            @Override
            public void onSucess() {
                binding.planBuy.setVisibility(View.GONE);
                binding.planCheck.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        }));
        binding.shopIconBgPlan.getBackground().setColorFilter(ResourceUtil.getCategoryColor(4), PorterDuff.Mode.MULTIPLY);

    }

    public void setViews() {
        if (IABUtils.hasFullAccess()) {
            binding.fullBuy.setVisibility(View.GONE);
            binding.fullCheck.setVisibility(View.VISIBLE);
        }
        if (IABUtils.hasStatistics()) {
            binding.statBuy.setVisibility(View.GONE);
            binding.statCheck.setVisibility(View.VISIBLE);
        }
        if (IABUtils.hasExercises()) {
            binding.exerBuy.setVisibility(View.GONE);
            binding.exerCheck.setVisibility(View.VISIBLE);
        }
        if (IABUtils.hasPlanner()) {
            binding.planBuy.setVisibility(View.GONE);
            binding.planCheck.setVisibility(View.VISIBLE);
        }
        if (IABUtils.hasAttributtes()) {
            binding.attrBuy.setVisibility(View.GONE);
            binding.attrCheck.setVisibility(View.VISIBLE);
        }
    }

    public void showError() {
        Toast.makeText(this, "Purchase Failed", Toast.LENGTH_SHORT).show();
    }

    public void showError2() {
        Snackbar.make(binding.getRoot(), "Something has gone wrong. please check your connection and try again", Toast.LENGTH_SHORT).show();
    }


}
