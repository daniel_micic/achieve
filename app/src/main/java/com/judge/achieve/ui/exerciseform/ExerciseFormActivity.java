package com.judge.achieve.ui.exerciseform;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.common.enums.Scope;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExerciseFormActivity extends BaseActivity {

    @BindView(R.id.root)
    CoordinatorLayout root;

    @BindView(R.id.create_exercise_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.create_exercise_title_et)
    EditText mTitleEt;

    @BindView(R.id.create_exercise_subcategory_et)
    EditText mSubcategoryEt;

    @BindView(R.id.create_exercise_subcategory_spinner)
    Spinner mSubcategorySpinner;

    @BindView(R.id.create_exercise_easy_checkbox)
    CheckBox mEasyCheckbox;

    @BindView(R.id.create_exercise_normal_checkbox)
    CheckBox mNormalCheckbox;

    @BindView(R.id.create_exercise_hard_checkbox)
    CheckBox mHardCheckbox;

    @BindView(R.id.create_exercise_small_checkbox)
    CheckBox mSmallCheckbox;

    @BindView(R.id.create_exercise_medium_checkbox)
    CheckBox mMediumCheckbox;

    @BindView(R.id.create_exercise_large_checkbox)
    CheckBox mLargeCheckbox;

    @BindView(R.id.create_exercise_description_et)
    EditText mDescriptionEt;

    @BindView(R.id.create_exercise_add_another_btn)
    TextView mAnotherBtn;

    @BindView(R.id.create_exercise_done_btn)
    FloatingActionButton mDoneBtn;

    @BindView(R.id.create_exercise_next_btn)
    FloatingActionButton mNextBtn;

    ExerciseFormPresenter mPresenter;

    int mMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        setContentView(R.layout.activity_exercise_form);
        ButterKnife.bind(this);


        if (savedInstanceState == null) {
            Logcat.d("save instance was null");
            mMode = getIntent().getIntExtra(C.EXTRA_MODE, C.MODE_CREATE);
            mPresenter = new ExerciseFormPresenter(this, mMode, getIntent());
        } else {
            Logcat.d("save instance was not null");
            mMode =  savedInstanceState.getInt(C.SAVED_INSTANCE_MODE, C.MODE_CREATE);
            mPresenter = new ExerciseFormPresenter(this, mMode, savedInstanceState);
        }

        initViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logcat.d("on save instance");
        outState.putInt(C.SAVED_INSTANCE_MODE, mMode);
        mPresenter.onSaveInstace(outState);
    }

    @Override
    public ExerciseFormPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onBackPressed() {
        Logcat.d("back pressed");
        if (mViewPager.getCurrentItem() == 1) {
            mViewPager.setCurrentItem(0, true);
            handleFabs();
        } else {
            super.onBackPressed();
            setResult(C.RESULT_FAIL);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
*/
    private void initViews() {

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setNavigationOnClickListener(view -> ExerciseFormActivity.this.onBackPressed());

        mNextBtn.show();
        mDoneBtn.hide();

        mViewPager.setAdapter(new ExerciseFormPagerAdapter(this));
        mViewPager.setCurrentItem(0);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                handleFabs();
            }

            @Override
            public void onPageSelected(int position) {
                handleFabs();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mSubcategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == adapterView.getCount() - 1) {
                    Logcat.d("create new");

                    mSubcategorySpinner.animate().alpha(0f).setDuration(300).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mSubcategorySpinner.setVisibility(View.GONE);
                        }
                    });

                    mSubcategoryEt.setAlpha(0f);
                    mSubcategoryEt.setVisibility(View.VISIBLE);
                    mSubcategoryEt.animate().alpha(1f).setDuration(300).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mSubcategoryEt.requestFocus();
                            Utils.showSoftInput(mSubcategoryEt, ExerciseFormActivity.this);
                        }
                    });

                    mPresenter.onCreateNewSubcategory();
                } else {
                    mPresenter.onSubcategorySelected(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSubcategorySpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        mEasyCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (mNormalCheckbox.isChecked()) {
                    mNormalCheckbox.performClick();
                }
                if (mHardCheckbox.isChecked()) {
                    mHardCheckbox.performClick();
                }
            }
        });


        mNormalCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (mEasyCheckbox.isChecked()) {
                    mEasyCheckbox.performClick();
                }
                if (mHardCheckbox.isChecked()) {
                    mHardCheckbox.performClick();
                }
            }
        });

        mHardCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (mEasyCheckbox.isChecked()) {
                    mEasyCheckbox.performClick();
                }
                if (mNormalCheckbox.isChecked()) {
                    mNormalCheckbox.performClick();
                }
            }
        });

        mSmallCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (mMediumCheckbox.isChecked()) {
                    mMediumCheckbox.performClick();
                }
                if (mLargeCheckbox.isChecked()) {
                    mLargeCheckbox.performClick();
                }
            }
        });

        mMediumCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (mSmallCheckbox.isChecked()) {
                    mSmallCheckbox.performClick();
                }
                if (mLargeCheckbox.isChecked()) {
                    mLargeCheckbox.performClick();
                }
            }
        });

        mLargeCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (mSmallCheckbox.isChecked()) {
                    mSmallCheckbox.performClick();
                }
                if (mMediumCheckbox.isChecked()) {
                    mMediumCheckbox.performClick();
                }
            }
        });

        if (mMode == C.MODE_CREATE)
            mMediumCheckbox.setChecked(true);

        mNextBtn.setOnClickListener(view -> {
            if (mViewPager.getCurrentItem() == 0) {
                mViewPager.setCurrentItem(1, true);
                handleFabs();
            }
        });
        mDoneBtn.setOnClickListener(view -> {
            getPresenter().onDoneClicked();
        });
    }

    public void resetViews() {
        mTitleEt.setText("");
        mSubcategorySpinner.setVisibility(View.VISIBLE);
        mSubcategorySpinner.setSelection(0);
        mSubcategoryEt.setVisibility(View.GONE);
        Logcat.d("reseting views");
        mMediumCheckbox.setChecked(true);
        mNormalCheckbox.setChecked(true);
        mDescriptionEt.setText("");

        mViewPager.setCurrentItem(0, true);
    }

    private void handleFabs() {
        if (mViewPager.getCurrentItem() == 0) {
            mNextBtn.show();
            mDoneBtn.hide();
        } else {
            mNextBtn.hide();
            mDoneBtn.show();
        }
    }

    public String getExerciseTitle() {
        return mTitleEt.getText().toString();
    }

    public void setExerciseTitle(String title) {
        mTitleEt.setText(title);
    }

    public void setExerciseTitleError() {
        Snackbar.make(root, "Please enter title", Snackbar.LENGTH_SHORT).show();
    };

    public void setSubcategorySpinnerData(ArrayList<String> data) {//, String subcategoryTitle) {
        if (data.size() > 1) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSubcategorySpinner.setAdapter(adapter);

        } else {
            Logcat.d("no subcategory, Et is visible");
            mSubcategorySpinner.setVisibility(View.GONE);
            mSubcategoryEt.setVisibility(View.VISIBLE);
        }

        /*if (!TextUtils.isEmpty(subcategoryTitle)) {
            mSubcategoryEt.setText(subcategoryTitle);
        }*/
    }

    public void setSubcategory(int position) {
        Logcat.d(position);
        mSubcategorySpinner.setSelection(position);
    }

    public String getSkillTitle() {
        return mSubcategoryEt.getText().toString();
    }

    public void setSubcategoryTitle(String subcategoryTitle) {
        mSubcategoryEt.setText(subcategoryTitle);
        mSubcategoryEt.setVisibility(View.VISIBLE);
        mSubcategorySpinner.setVisibility(View.GONE);
    }

    public void setSubcategoryTitleError() {
        Snackbar.make(root, "Please select Skill", Snackbar.LENGTH_SHORT).show();
    };

    public Difficulty getExerciseDifficulty() {
        if (mEasyCheckbox.isChecked()) {
            return Difficulty.EASY;
        }
        if (mNormalCheckbox.isChecked()) {
            return Difficulty.NORMAL;
        }
        if (mHardCheckbox.isChecked()) {
            return Difficulty.HARD;
        }
        return Difficulty.UNDEFINED;
    }

    public void setExerciseDifficulty(Difficulty difficulty) {
        switch (difficulty) {
            case EASY:
                mEasyCheckbox.setChecked(true);
                mNormalCheckbox.setChecked(false);
                mHardCheckbox.setChecked(false);
                break;
            case NORMAL:
                mEasyCheckbox.setChecked(false);
                mNormalCheckbox.setChecked(true);
                mHardCheckbox.setChecked(false);
                break;
            case HARD:
                mEasyCheckbox.setChecked(false);
                mNormalCheckbox.setChecked(false);
                mHardCheckbox.setChecked(true);
                break;
            case UNDEFINED:
                mEasyCheckbox.setChecked(false);
                mNormalCheckbox.setChecked(false);
                mHardCheckbox.setChecked(false);
        }
    }

    public void setExerciseDifficultyError() {
        Snackbar.make(root, "Please select Difficulty", Snackbar.LENGTH_SHORT).show();
    }

    public Scope getExerciseScope() {
        if (mSmallCheckbox.isChecked()) {
            return Scope.SMALL;
        }
        if (mMediumCheckbox.isChecked()) {
            return Scope.MEDIUM;
        }
        if (mLargeCheckbox.isChecked()) {
            return Scope.LARGE;
        }
        return Scope.UNDEFINED;
    }

    public void setExerciseScope(Scope scope) {
        Logcat.d(scope.getValue());
        switch (scope) {
            case SMALL:
                Logcat.d("setting small");
                mSmallCheckbox.setChecked(true);
                mMediumCheckbox.setChecked(false);
                mLargeCheckbox.setChecked(false);
                break;
            case MEDIUM:
                Logcat.d("setting medium");
                mSmallCheckbox.setChecked(false);
                mMediumCheckbox.setChecked(true);
                mLargeCheckbox.setChecked(false);
                break;
            case LARGE:
                mSmallCheckbox.setChecked(false);
                mMediumCheckbox.setChecked(false);
                mLargeCheckbox.setChecked(true);
                break;
            case UNDEFINED:
                mSmallCheckbox.setChecked(false);
                mMediumCheckbox.setChecked(false);
                mLargeCheckbox.setChecked(false);
        }
    }

    public String getExerciseDescription() {
        return mDescriptionEt.getText().toString();
    }

    public void setExerciseDescription (String description) {
        mDescriptionEt.setText(description);
    }

    public void setExerciseScopeError() {
        Snackbar.make(root, "Please select Scope", Snackbar.LENGTH_SHORT).show();
    }


}
