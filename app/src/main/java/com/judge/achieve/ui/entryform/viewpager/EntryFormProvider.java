package com.judge.achieve.ui.entryform.viewpager;

import com.judge.achieve.model.PlannerEntry;

/**
 * Created by Daniel on 28.02.2017.
 */

public interface EntryFormProvider {

    int getMode();

    PlannerEntry getEntry();
    String getEntryRecurrenceString();

    String getFormattedStartDate();

}
