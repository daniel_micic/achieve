package com.judge.achieve.ui.about;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ActivityShopBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.Logcat;

public class AboutActivity extends BaseActivity {

    ActivityShopBinding binding;

    AboutPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);

        mPresenter = new AboutPresenter(this);

        initViews();

        setViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public AboutPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {

    }

    public void setViews() {

    }
}
