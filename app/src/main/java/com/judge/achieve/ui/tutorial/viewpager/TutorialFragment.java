package com.judge.achieve.ui.tutorial.viewpager;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.FragmentTutorialBinding;
import com.judge.achieve.utils.ResourceUtil;

/**
 * Created by Daniel on 24.01.2017.
 */

public class TutorialFragment extends Fragment {

    FragmentTutorialBinding binding;

    int position;
    public static TutorialFragment newInstance(int position) {
        TutorialFragment fragment = new TutorialFragment();

        Bundle args = new Bundle();
        args.putInt(C.EXTRA_POSITION, position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getArguments().getInt(C.EXTRA_POSITION);
      }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tutorial, container, false);
        ImageRequest imageRequest;

        switch (position) {
            default:
            case 0:
                binding.tutorialTitle.setText(ResourceUtil.getString(R.string.tutorial_welcome));
                binding.tutorialText.setText(ResourceUtil.getString(R.string.tutorial_welcome_msg));
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.icon).build();
                binding.tutorialImage.setImageURI(imageRequest.getSourceUri());

                break;
            case 1:
                binding.tutorialTitle.setText(ResourceUtil.getString(R.string.tutorial_attributes));
                binding.tutorialText.setText(ResourceUtil.getString(R.string.tutorial_attributes_msg));
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.tutorial_main).build();
                binding.tutorialImage.setImageURI(imageRequest.getSourceUri());
                break;
            case 2:
                binding.tutorialTitle.setText(ResourceUtil.getString(R.string.tutorial_skill_ex));
                binding.tutorialText.setText(ResourceUtil.getString(R.string.tutorial_skill_ex_msg));
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.tutorial_detail).build();
                binding.tutorialImage.setImageURI(imageRequest.getSourceUri());
                break;
            case 3:
                binding.tutorialImage.setVisibility(View.GONE);
                binding.tripleLayout.setVisibility(View.VISIBLE);

                binding.tutorialTitle.setText(ResourceUtil.getString(R.string.tutorial_goals));
                binding.tutorialText.setText(ResourceUtil.getString(R.string.tutorial_goals_msg));

                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.tutorial_goals).build();
                binding.tutorialImage1.setImageURI(imageRequest.getSourceUri());
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.tutorial_stats).build();
                binding.tutorialImage2.setImageURI(imageRequest.getSourceUri());
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.tutorial_planner).build();
                binding.tutorialImage3.setImageURI(imageRequest.getSourceUri());
                break;
        }

        return binding.getRoot();
    }

}
