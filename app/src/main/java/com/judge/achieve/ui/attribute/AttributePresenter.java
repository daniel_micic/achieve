package com.judge.achieve.ui.attribute;

import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;

import com.judge.achieve.common.C;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.ExerciseHistory;
import com.judge.achieve.model.Profile;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.ExerciseHistoryQuery;
import com.judge.achieve.persistence.query.ExerciseQuery;
import com.judge.achieve.persistence.query.ProfileQuery;
import com.judge.achieve.persistence.query.SkillQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.DialogUtils;
import com.judge.achieve.utils.IABUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.Utils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Daniel on 24.07.2016.
 */
public class AttributePresenter
        extends BasePresenter
        implements SkillAdapter.SkillProvider,
        ExerciseAdapter.ExerciseDescriptionHandler,
        ExerciseAdapter.HistoryStringUpdater{

    private AttributeActivity view;

    private Handler handler = new Handler();
    private int attributeId;
    private Attribute attribute;
    private int historyStringCycleNo = 1;

    private HashMap<Integer, ExerciseItem> exerciseItems = new HashMap<>();
    private List<Pair<Integer, ExerciseAdapter.HistoryStringListener>> historyStringListeners = new ArrayList<>();

    class ExerciseItem {
        boolean expaned;
        boolean animating;

        public ExerciseItem(boolean expaned, boolean animating) {
            this.expaned = expaned;
            this.animating = animating;
        }


        public boolean isExpaned() {
            return expaned;
        }

        public void setExpaned(boolean expaned) {
            this.expaned = expaned;
        }

        public boolean isAnimating() {
            return animating;
        }

        public void setAnimating(boolean animating) {
            this.animating = animating;
        }


    }

    public AttributePresenter(AttributeActivity view, int id) {
        super();

        this.view = view;
        attributeId = id;
        attribute = AttributeQuery.getById(getRealm(), attributeId);
        if (exerciseItems == null) {
            exerciseItems = new HashMap<>();
        }
        for (Skill skill : attribute.getSkills()) {
            for (Exercise exercise : skill.getExercises()) {
                exerciseItems.put(exercise.getId(), new ExerciseItem(false, false));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Logcat.d("onResume");
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (Pair<Integer, ExerciseAdapter.HistoryStringListener> listener : historyStringListeners) {
                    Pair<Boolean, String> result = Utils.getExerciseHistoryString(ExerciseQuery.getById(getRealm(), listener.first), historyStringCycleNo);
                    if (result == null) {
                        return;
                    }
                    if (result.first) {
                        listener.second.onChange(result.second);
                    }
                }

                historyStringCycleNo++;

                handler.postDelayed(this, 6000);
            }
        }, 7000);

        if (attribute == null) {
            attribute = AttributeQuery.getById(getRealm(), attributeId);
            if (exerciseItems == null) {
                exerciseItems = new HashMap<>();
            }
            for (Skill skill : attribute.getSkills()) {
                for (Exercise exercise : skill.getExercises()) {
                    exerciseItems.put(exercise.getId(), new ExerciseItem(false, false));
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacksAndMessages(null);
    }

    public void onSaveInstance(Bundle outState) {
        outState.putInt(C.SAVED_INSTANCE_CATEGORY_ID, attributeId);
    }

    public void update() {
        attribute = AttributeQuery.getById(getRealm(), attributeId);
    }

    public Attribute getAttribute() {
        if (attribute == null) {
            attribute = AttributeQuery.getById(getRealm(), attributeId);
        }
        return attribute;
    }

    public int getAttributeId() {
        return getAttribute().getId();
    }

    public ArrayList<Skill> getAttributeSkills() {
        return getAttribute().getSkills();
    }

    public String getAttributeTitle() {
        return getAttribute().getTitle();
    }

    @Override
    public Skill getSkillForPosition(int position) {
        return getAttributeSkills().get(position);
    }

    @Override
    public int getCount() {
        return getAttributeSkills().size();
    }

    public void editSkill(String skillTitle, int skillPosition) {
        Skill s = getSkillForPosition(skillPosition);
        s.setTitle(skillTitle);

        SkillQuery.edit(getRealm(), s);
    }

    public void deleteSkill(int skillPosition) {
        Skill s = getSkillForPosition(skillPosition);

        /*for (Exercise exercise : s.getExercises()) {
            ExerciseQuery.delete(getRealm(), exercise.getId());
        }*/

        SkillQuery.delete(getRealm(), s.getId());
        getAttributeSkills().remove(s);
    }

    public void doExercise(final Exercise e,
                                    int skillPosition) {
        float points = e.getPointsForExercise();
        boolean newLevel = false;

        Profile profile = ProfileQuery.get(getRealm());
        if (profile.addPoints(points)) {
            newLevel = true;
            DialogUtils.getLevelUpDialog(view, profile.getLevel()).show();
        }
        ProfileQuery.edit(getRealm(), profile);

        attribute.addPoints(points);
        AttributeQuery.edit(getRealm(), attribute);

        Skill s = getSkillForPosition(skillPosition);
        if (s.addPoints(points)) {
            if (!newLevel) {
                DialogUtils.getNewSkillPointDialog(view, s.getTitle()).show();
            }
        }
        SkillQuery.edit(getRealm(), s);

        e.setCountForDay(e.getCountForDay() + 1);

        boolean newEntry = false;
        ExerciseHistory eh = e.getStatistics().isEmpty() ? null : e.getStatistics().get(e.getStatistics().size() - 1);

        if (eh == null || !eh.getDate().isEqual(DateTime.now().withTimeAtStartOfDay())) {
            eh = new ExerciseHistory();
            eh.setId(ExerciseHistoryQuery.getNewPrimaryKey(getRealm()));
            eh.setDate(DateTime.now().withTimeAtStartOfDay());
            newEntry = true;
        }
        eh.setCountForDay(e.getCountForDay());
        eh.setPoints(e.getPointsForExercise() * e.getCountForDay());

        if (newEntry)
            ExerciseHistoryQuery.copyToRealm(getRealm(), eh, e.getId());
        else
            ExerciseHistoryQuery.edit(getRealm(), eh);

        e.getStatistics().add(eh);

        ExerciseQuery.edit(getRealm(), e);
    }

    public void deleteExercise(Exercise e, int exercisePosition, int skillPosition) {
        Logcat.d(e.getId());

        /*for (ExerciseHistory eh : e.getStatistics() ) {
            //ExerciseHistoryQuery.d
            ExerciseHistoryQuery.delete(getRealm(), eh.getId());
        }*/

        ExerciseQuery.delete(getRealm(), e.getId());
        Logcat.d(getAttributeSkills().get(skillPosition).getExercises().size());

        getSkillForPosition(skillPosition).getExercises().remove(exercisePosition);
        Logcat.d(getAttributeSkills().get(skillPosition).getExercises().size());

    }

    @Override
    public void addListener(ExerciseAdapter.HistoryStringListener listener, int exerciseId) {
        historyStringListeners.add(new Pair<>(exerciseId, listener));
    }

    @Override
    public boolean isExerciseAnimating(int id) {
        return exerciseItems.get(id).isAnimating();
    }

    @Override
    public boolean isExerciseExpanded(int id) {
        return exerciseItems.get(id).isExpaned();
    }

    @Override
    public void setExerciseExpanded(int id, boolean expanded) {
        ExerciseItem item = exerciseItems.get(id);
        item.setExpaned(expanded);
        exerciseItems.put(id, item);
    }

    @Override
    public void setExerciseAnimating(int id, boolean animating) {
        ExerciseItem item = exerciseItems.get(id);
        item.setAnimating(animating);
        exerciseItems.put(id, item);
    }

    public boolean canCreateExercise() {
        if (IABUtils.hasExercises()) {
            return true;
        } else {
            int count = 0;
            for (Skill skill : attribute.getSkills()) {
                count += skill.getExercises().size();
            }
            if (count < 5) {
                return true;
            }
        }
        return false;
    }

    public boolean hasStatisticsAccess() {
        if (IABUtils.hasStatistics()) {
            return true;
        }
        return false;
    }

    public boolean hasPlannerAccess() {
        if (IABUtils.hasPlanner()) {
            return true;
        }
        return false;
    }
}
