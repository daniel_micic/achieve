package com.judge.achieve.ui.goal.viewpager;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.judge.achieve.BR;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.FragmentGoalBinding;
import com.judge.achieve.ui.base.BaseFragment;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.goal.GoalActivity;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.Utils;

/**
 * Created by Daniel on 24.01.2017.
 */

public class GoalFragment extends BaseFragment {

    FragmentGoalBinding binding;

    GoalFragmentPresenter presenter;

    private boolean isEntryAnimation;

    public static GoalFragment newInstance(int goalId, boolean entryAnimation) {
        GoalFragment fragment = new GoalFragment();

        Bundle args = new Bundle();
        args.putInt(C.EXTRA_GOAL_ID, goalId);
        args.putBoolean(C.EXTRA_GOAL_ENTRY_ANIM, entryAnimation);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int id;
        if (savedInstanceState == null) {
            id = getArguments().getInt(C.EXTRA_GOAL_ID);
            isEntryAnimation = getArguments().getBoolean(C.EXTRA_GOAL_ENTRY_ANIM);
        } else {
            id = savedInstanceState.getInt(C.SAVED_INSTANCE_GOAL_ID);
        }

        presenter = new GoalFragmentPresenter(getActivity(), id);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_goal, container, false);
        binding.setVariable(BR.presenter, presenter);

        binding.getRoot().setTag(getPresenter().getId());

        binding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            if (isEntryAnimation) {
                entryAnimation();
                isEntryAnimation = false;
            }
        });

        binding.getRoot().setScaleX(0.85f);
        binding.getRoot().setScaleY(0.85f);

        binding.goalMore.setOnClickListener(view -> {
            PopupMenu menu = new PopupMenu(getActivity(), binding.goalMore, Gravity.RIGHT);
            menu.inflate(R.menu.edit_delete_menu);
            menu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.delete:
                        ((GoalActivity)getActivity()).removeFragment(getPresenter().getId());
                        break;
                    case R.id.edit:
                        getPresenter().onGoalEdit((CircularOverlayActivity)getActivity());
                        break;
                }
                return false;
            });
            menu.show();
        });

        if (!TextUtils.isEmpty(getPresenter().getAttributeSkill())) {
            if (getPresenter().getGoalAttributePosition() != C.UNDEFINED)
                binding.goalAttrSkill.setBackgroundDrawable(Utils.getAttrBackgroundChips(getPresenter().getGoalAttributePosition()));
        } else {
            binding.goalAttrSkill.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(getPresenter().getGoalDifficulty())) {
            binding.goalDifficulty.setVisibility(View.GONE);
        }

        if (!getPresenter().getGoal().isFinished()) {
            binding.finishButton.setOnClickListener(view -> onGoalFinished());
            binding.finishCheck.setScaleX(0f);
            binding.finishCheck.setScaleY(0f);
            //binding.finishCheck.setAlpha(0f);
            binding.finishCheck.setRotation(-180);
        } else {
            binding.finishText.setVisibility(View.GONE);
            binding.finishCheck.setVisibility(View.VISIBLE);
        }

        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstance(outState);
    }

    @Override
    public GoalFragmentPresenter getPresenter() {
        return presenter;
    }

    public void entryAnimation() {
        float topLayoutPos = binding.topLayout.getY();
        float bottomLayoutPos = binding.bottomLayout.getY();
        Logcat.d(topLayoutPos +" "+ bottomLayoutPos);

        binding.topLayout.setY(topLayoutPos - 400);
        binding.topLayout.setAlpha(0f);
        binding.bottomLayout.setY(bottomLayoutPos - 400);
        binding.bottomLayout.setAlpha(0f);

        Logcat.d((topLayoutPos - 400) + " " + (bottomLayoutPos - 400));

        binding.bottomLayout.animate()
                .y(bottomLayoutPos)
                .alpha(1f)
                .setDuration(500)
                .setInterpolator(new DecelerateInterpolator());

        binding.topLayout.animate()
                .y(topLayoutPos)
                .alpha(1f)
                .setDuration(500)
                .setInterpolator(new DecelerateInterpolator())
                .setStartDelay(100);
    }

    public void exitAnimation() {
        if (binding != null) {
            float topLayoutPos = binding.topLayout.getY();
            float bottomLayoutPos = binding.bottomLayout.getY();
            Logcat.d(topLayoutPos + " " + bottomLayoutPos);

            Logcat.d((topLayoutPos - 400f) + " " + (bottomLayoutPos - 400f));

            binding.topLayout.animate()
                    .y(topLayoutPos - 400f)
                    .alpha(0.25f)
                    .setDuration(400)
                    .setInterpolator(new AccelerateInterpolator());

            binding.bottomLayout.animate()
                    .y(bottomLayoutPos - 400f)
                    .alpha(0.5f)
                    .setDuration(400)
                    .setInterpolator(new AccelerateInterpolator())
                    .setStartDelay(200);
        }
    }

    private void onGoalFinished() {
        if (!getPresenter().getGoal().isFinished()) {
            binding.finishText.animate()
                    .scaleX(0f)
                    .scaleY(0f)
                    .rotationBy(180)
                    .setDuration(300);


            new Handler().postDelayed(() ->
                            binding.finishCheck.animate()
                                    .scaleX(1)
                                    .scaleY(1)
                                    //.alpha(1)
                                    .rotationBy(180)
                                    .setDuration(300)
                    , 150);
        }

        getPresenter().onGoalFinished();
    }
}
