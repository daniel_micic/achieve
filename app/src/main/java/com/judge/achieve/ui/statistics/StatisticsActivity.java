package com.judge.achieve.ui.statistics;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.ActivityStatisticsBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MySpinnerListener;
import com.judge.achieve.utils.ResourceUtil;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;

public class StatisticsActivity extends BaseActivity {

    StatisticsPresenter presenter;

    ActivityStatisticsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_statistics);

        int attrId = -1;
        int skillId = -1;
        if (getIntent().getExtras() != null) {
            attrId = getIntent().getExtras().getInt(C.EXTRA_ATTRIBUTE_ID, -1);
            skillId = getIntent().getExtras().getInt(C.EXTRA_SKILL_ID, -1);
        }

        if (attrId == -1 || skillId == -1) {
            Logcat.d("something is -1");
            presenter = new StatisticsPresenter(this);
        } else {
            Logcat.d(attrId);
            Logcat.d(skillId);
            presenter = new StatisticsPresenter(this, attrId, skillId);
        }

        initViews();
        setViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstace(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.statisticsAttributeSpinner.setSelection(getPresenter().getAttributeSelectedPos());
        binding.statisticsSkillSpinner.setSelection(getPresenter().getSkillSelectedPos());
    }

    @Override
    public StatisticsPresenter getPresenter() {
        return presenter;
    }


    private void initViews() {

        setSupportActionBar(binding.statisticsToolbar);
        setTitle(R.string.statistics);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        binding.statisticsToolbar.setNavigationOnClickListener(view -> StatisticsActivity.this.onBackPressed());
        
        initLineChart();
        initBarChart();

        binding.statisticsAttributeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsAttributeSpinner.setOnItemSelectedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                Logcat.d("attr selected");
                presenter.onAttributeSelected(position);
                setSkillSpinnerData();
                setExerciseSpinnerData();
            }
        });

        binding.statisticsSkillSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsSkillSpinner.setOnItemSelectedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                Logcat.d("skill selected");
                presenter.onSkillSelected(position);
                setExerciseSpinnerData();
            }
        });

        binding.statisticsExerciseSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsExerciseSpinner.setOnItemSelectedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                Logcat.d("exercise selected");
                presenter.onExerciseSelected(position);
            }
        });

        binding.statisticsIntervalSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsIntervalSpinner.setOnItemSelectedEvenIfUnchangedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                Logcat.d(position);
                if (position != 3)
                    presenter.onIntervalSelected(position);
                else {
                    showDateRangePickerDialog();
                }
            }
        });
        binding.statisticsModeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsModeSpinner.setOnItemSelectedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                presenter.setMode(position == 1 ? StatisticsPresenter.Mode.AGGREGATE : StatisticsPresenter.Mode.DAILY);
            }
        });

        binding.statisticsChartSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsChartSpinner.setOnItemSelectedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                presenter.setChartType(position == 1 ? StatisticsPresenter.ChartType.LINE : StatisticsPresenter.ChartType.BAR);
            }
        });

        binding.statisticsValueSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        binding.statisticsValueSpinner.setOnItemSelectedListener(new MySpinnerListener() {
            @Override
            public void onItemSelected(int position) {
                presenter.setValueType(position == 1 ? StatisticsPresenter.ValueType.AMOUNT_XP: StatisticsPresenter.ValueType.EXER_COUNT);

                /* TODO finish when time
                switch (position) {
                    case 0:
                        presenter.setValueType(StatisticsPresenter.ValueType.EXER_COUNT);
                        break;
                    case 1:
                        presenter.setValueType(StatisticsPresenter.ValueType.SKILL_POINTS);
                        break;
                    case 2:
                        presenter.setValueType(StatisticsPresenter.ValueType.AMOUNT_XP);
                        break;
                }*/
            }
        });

        binding.statisticsApply.setOnClickListener(view -> setGraph());
    }

    private void initLineChart() {
        binding.lineChart.getLegend().setEnabled(false);
        binding.lineChart.setDescription(null);
        binding.lineChart.setNoDataText(ResourceUtil.getString(R.string.msg_no_data));
        binding.lineChart.setNoDataTextColor(ResourceUtil.getColor(R.color.white));
        binding.lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        YAxis left = binding.lineChart.getAxisLeft();
        YAxis right = binding.lineChart.getAxisRight();
        XAxis top = binding.lineChart.getXAxis();

        left.setAxisMinValue(0f);
        left.setDrawGridLines(false);
        left.setGranularity(1f);
        left.setSpaceTop(10);
        left.setXOffset(10);
        left.setDrawAxisLine(false);
        left.setDrawGridLines(false);
        left.setTextColor(ResourceUtil.getColor(R.color.white));
        left.setValueFormatter((value, axis) -> String.valueOf((int)value));

        top.setTextColor(ResourceUtil.getColor(R.color.white));
        top.setGranularity(1f);
        top.setPosition(XAxis.XAxisPosition.BOTTOM);

        top.setDrawAxisLine(false);
        top.setDrawGridLines(false);

        right.setDrawLabels(true);
        right.setDrawAxisLine(false);
        right.setDrawAxisLine(false);
        right.setDrawGridLines(false);

        binding.lineChart.invalidate();
    }

    private void initBarChart() {
        binding.barChart.getLegend().setEnabled(false);
        binding.barChart.setDescription(null);
        binding.barChart.setNoDataText(ResourceUtil.getString(R.string.msg_no_data));
        binding.barChart.setNoDataTextColor(ResourceUtil.getColor(R.color.white));
        binding.barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        YAxis left = binding.barChart.getAxisLeft();
        YAxis right = binding.barChart.getAxisRight();
        XAxis top = binding.barChart.getXAxis();

        left.setAxisMinValue(0f);
        left.setDrawGridLines(false);
        left.setGranularity(1f);
        left.setSpaceTop(10);
        left.setXOffset(10);
        left.setDrawAxisLine(false);
        left.setDrawGridLines(false);
        left.setTextColor(ResourceUtil.getColor(R.color.white));
        left.setValueFormatter((value, axis) -> String.valueOf((int)value));

        top.setTextColor(ResourceUtil.getColor(R.color.white));
        top.setGranularity(1f);
        top.setPosition(XAxis.XAxisPosition.BOTTOM);

        top.setDrawAxisLine(false);
        top.setDrawGridLines(false);

        //TODO remove after X Axis formating is done
        top.setDrawLabels(false);
        top.setDrawAxisLine(false);
        top.setDrawAxisLine(false);
        top.setDrawGridLines(false);

        right.setDrawLabels(false);
        right.setDrawAxisLine(false);
        right.setDrawAxisLine(false);
        right.setDrawGridLines(false);

        binding.barChart.invalidate();
    }

    public void setViews() {
        setAttributeSpinnerData();
        setSkillSpinnerData();
        setExerciseSpinnerData();
        setGraph();
        setIntervalSpinnerData();
        setModeSpinnerData();
        setChartSpinnerData();
        setValueSpinnerData();
    }

    public void setGraph() {
        if (presenter.getChartType() == StatisticsPresenter.ChartType.LINE) {
            binding.barChart.setVisibility(View.GONE);
            binding.lineChart.setVisibility(View.VISIBLE);
            binding.lineChart.setData(getPresenter().getBarData());
            binding.lineChart.getAxisLeft().setAxisMaxValue(presenter.getActualMax());
            binding.lineChart.fitScreen();
            binding.lineChart.invalidate();
        } else {
            binding.lineChart.setVisibility(View.GONE);
            binding.barChart.setVisibility(View.VISIBLE);
            binding.barChart.setData(getPresenter().getLineData());
            binding.barChart.getAxisLeft().setAxisMaxValue(presenter.getActualMax());
            binding.barChart.fitScreen();
            binding.barChart.invalidate();
        }
    }

    public void setAttributeSpinnerData() {
        ArrayAdapter<String> attributeAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, presenter.getAttributeTitles());
        attributeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsAttributeSpinner.setAdapter(attributeAdapter);
    }

    public void setSkillSpinnerData() {
        ArrayAdapter<String> SkillAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, presenter.getSkillTitles());
        SkillAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsSkillSpinner.setAdapter(SkillAdapter);
    }

    public void setExerciseSpinnerData() {
        ArrayAdapter<String> exerciseAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, presenter.getExerciseTitles());
        exerciseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsExerciseSpinner.setAdapter(exerciseAdapter);
    }

    public void setIntervalSpinnerData() {
        ArrayAdapter<String> intervalAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, presenter.getIntervals());
        intervalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsIntervalSpinner.setAdapter(intervalAdapter);
    }

    public void setModeSpinnerData() {
        List<String> values = Arrays.asList("Daily", "Aggregate");
        ArrayAdapter<String> attributeAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, values);
        attributeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsModeSpinner.setAdapter(attributeAdapter);
    }

    public void setChartSpinnerData() {
        List<String> values = Arrays.asList("Bar", "Line");
        ArrayAdapter<String> attributeAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, values);
        attributeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsChartSpinner.setAdapter(attributeAdapter);
    }

    public void setValueSpinnerData() {
        //TODO maybe add skill points but presneter is not finished for that "Skill points",

        List<String> values = Arrays.asList("Exercise count", "Experience gained");
        ArrayAdapter<String> attributeAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner_subcategory, values);
        attributeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.statisticsValueSpinner.setAdapter(attributeAdapter);
    }

    public void showDateRangePickerDialog() {

        Logcat.d("showing dialog");
        DateTime now = DateTime.now();
        DatePickerDialog dialog = DatePickerDialog.newInstance(
                (view, year, monthOfYear, dayOfMonth, yearEnd, monthOfYearEnd, dayOfMonthEnd) -> {
                    DateTime start = new DateTime().withDate(year, monthOfYear + 1, dayOfMonth).withTimeAtStartOfDay();
                    DateTime end = new DateTime().withDate(yearEnd, monthOfYearEnd + 1, dayOfMonthEnd).withTimeAtStartOfDay();
                    presenter.setStartDate(start);
                    presenter.setEndDate(end);
                },
                now.getYear(),
                now.getMonthOfYear() - 1,
                now.getDayOfMonth()
        );
        dialog.setAccentColor(ResourceUtil.getColor(R.color.colorPrimary));
        dialog.show(getFragmentManager(), "Date");
    }

    /*public void showStartDatePickerDialogs() {
        DatePickerDialog.OnDateSetListener fromListener = (DatePicker datePicker, int year, int month, int day) -> {
            DateTime startDate = new DateTime().withDate(year, month, day);
            presenter.setStartDate(startDate);
            showEndDatePickerDialogs();
        };

        DateTime to = DateTime.now();
        DateTime from = to.minusDays(30);

        new DatePickerDialog(
                this,
                fromListener,
                from.getYear(),
                from.getMonthOfYear(),
                from.getDayOfMonth()
        ).show();
    }

    public void showEndDatePickerDialogs() {
        DatePickerDialog.OnDateSetListener fromListener = (DatePicker datePicker, int i, int i1, int i2) -> {
            DateTime
        };

        DateTime to = DateTime.now();
        DateTime from = to.minusDays(30);

        new DatePickerDialog(
                this,
                fromListener,
                from.getYear(),
                from.getMonthOfYear(),
                from.getDayOfMonth()
        ).show();
    }*/

}
