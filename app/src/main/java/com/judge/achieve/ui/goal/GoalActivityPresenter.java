package com.judge.achieve.ui.goal;

import com.judge.achieve.model.Goal;
import com.judge.achieve.persistence.query.GoalQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.ui.goal.viewpager.GoalAdapter;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;

/**
 * Created by Daniel on 23.07.2016.
 */
public class GoalActivityPresenter extends BasePresenter implements GoalAdapter.GoalProvider {

    private GoalActivity view;

    private ArrayList<Goal> goals;

    public GoalActivityPresenter(GoalActivity view) {
        super();

        this.view = view;
    }

    @Override
    public void onResume() {
        super.onResume();

        goals = GoalQuery.getAll(getRealm());

    }

    public ArrayList<Goal> getGoals() {
        return goals;
    }

    public void setGoals(ArrayList<Goal> goals) {
        this.goals = goals;
    }

    public int getCount() {
        return goals.size();
    }

    public Goal getGoal(int position) {
        return goals.get(position);
    }

    public void removeFragment(int position, int id) {
        Logcat.d(position);
        GoalQuery.delete(getRealm(), id);
        goals.remove(position);
    }
}
