package com.judge.achieve.ui.attribute;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ItemSkillBinding;
import com.judge.achieve.model.Skill;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;
import com.judge.achieve.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 25.05.2016.
 */
public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.SkillViewHolder> {

    private Context contex;

    private SkillListener skillListener;
    private SkillProvider skillProvider;
    private ExerciseAdapter.ExerciseListener exerciseListener;
    private ExerciseAdapter.ExerciseDescriptionHandler handler;
    private ExerciseAdapter.HistoryStringUpdater updater;

    private boolean isActionMode = false;
    private boolean isFirstView = true;
    private int noOfFStarted = 0;
    private int noOfFinished = 0;
    private ArrayList<ViewPropertyAnimator> animators = new ArrayList<>();

    public interface SkillListener {
        void onSkillDeleteClick(int skillPosition);
        void onSkillEditClick(int skillPosition);
        void onSkillPlannerClick(int skillPosition);
        void onSkillStatisticsClick(int skillPosition);
    }

    public interface SkillExpandHandler {
        void onItemChanged(int position);
    }

    public interface SkillProvider {
        Skill getSkillForPosition(int position);
        int getCount();
    }

    public SkillAdapter(AttributeActivity activity, AttributePresenter presenter) {
        this.skillListener = activity;
        this.exerciseListener = activity;
        this.contex = activity;
        this.updater = presenter;
        this.handler = presenter;
        this.skillProvider = presenter;
    }

    @Override
    public SkillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSkillBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_skill,parent,false);
        SkillViewHolder vh = new SkillViewHolder(binding);
        return vh;
    }

    @Override
    public void onBindViewHolder(final SkillViewHolder holder, final int position) {
        onBindViewHolder(holder, position, new ArrayList<>());
    }

    @Override
    public void onBindViewHolder(final SkillViewHolder holder, final int position, List<Object> payload) {
        if (payload.isEmpty()) {
            Skill s = skillProvider.getSkillForPosition(position);

            holder.binding.skillTitle.setText(s.getTitle());
            holder.binding.skillPoints.setText(String.valueOf(s.getProgress()));

            holder.binding.skillPoints.getBackground().setColorFilter(Utils.getCategoryColor(position), PorterDuff.Mode.MULTIPLY);

            if (isActionMode) {
                holder.binding.skillFeatureMenu.setVisibility(View.GONE);
                holder.binding.skillEditMenu.setVisibility(View.VISIBLE);
            } else {
                holder.binding.skillFeatureMenu.setVisibility(View.VISIBLE);
                holder.binding.skillEditMenu.setVisibility(View.GONE);
            }

            holder.binding.skillEdit.setOnClickListener(view -> skillListener.onSkillEditClick(position));
            holder.binding.skillDelete.setOnClickListener(view -> skillListener.onSkillDeleteClick(position));

            holder.binding.skillPlanner.setOnClickListener(view -> skillListener.onSkillPlannerClick(position));
            holder.binding.skillStatistics.setOnClickListener(view -> skillListener.onSkillStatisticsClick(position));

            ExerciseAdapter adapter = new ExerciseAdapter(s.getId(), s.getExercises(), position, updater, handler, exerciseListener);
            adapter.setActionMode(isActionMode);
            holder.binding.exerciseList.setLayoutManager(new LinearLayoutManager(contex));
            holder.binding.exerciseList.setAdapter(adapter);

            if (isFirstView) {
                Logcat.d("animating");
                float y = holder.binding.getRoot().getY();
                Logcat.d(y);
                float height = ResourceUtil.getDisplayMetrics().heightPixels;

                holder.binding.getRoot().setY(y + 400);
                holder.binding.getRoot().setAlpha(0f);
                ViewPropertyAnimator animator = holder.binding.getRoot().animate()
                        .translationY(y)
                        .alpha(1f)
                        .setDuration(400)
                        .setInterpolator(new DecelerateInterpolator())
                        .setStartDelay(position * 50 + 200)
                        .withStartAction(() -> {
                            noOfFStarted++;
                        })
                        .withEndAction(() -> {
                            isFirstView = false;
                            noOfFinished++;
                        });

                animators.add(animator);
            }
        } else {
            Skill s = skillProvider.getSkillForPosition(position);

            holder.binding.skillTitle.setText(String.valueOf(s.getTitle()));
            holder.binding.skillPoints.setText(String.valueOf(s.getProgress()));
        }
    }

    @Override
    public int getItemCount() {
        return skillProvider.getCount();
    }

    public boolean isActionMode() {
        return isActionMode;
    }

    public void setActionMode(boolean actionMode) {
        isActionMode = actionMode;

    }

    public boolean isAnimating() {
        return noOfFinished < noOfFStarted;
    }

    public void cancelAnimations() {
        for (ViewPropertyAnimator animator : animators) {
            animator.cancel();
        }
        animators = new ArrayList<>();
    }

    public static class SkillViewHolder extends RecyclerView.ViewHolder {

        ItemSkillBinding binding;
        public SkillViewHolder(ItemSkillBinding binding){
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
