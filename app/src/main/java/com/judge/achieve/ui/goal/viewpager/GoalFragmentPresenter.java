package com.judge.achieve.ui.goal.viewpager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Goal;
import com.judge.achieve.model.Profile;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.GoalQuery;
import com.judge.achieve.persistence.query.ProfileQuery;
import com.judge.achieve.persistence.query.SkillQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.goalform.GoalFormActivity;
import com.judge.achieve.utils.DialogUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MyAnimationListener;
import com.judge.achieve.utils.ResourceUtil;

/**
 * Created by Daniel on 24.01.2017.
 */

public class GoalFragmentPresenter extends BasePresenter {

    private Activity context;
    private int id;
    private Goal goal;
    private String attrSkill;
    private String difficulty;
    Handler handler = new Handler();

    public GoalFragmentPresenter(Activity view, int id) {
        this.id = id;
        this.goal = GoalQuery.getById(getRealm(), id);
        this.context = view;
        Logcat.d(goal.getId());
        Logcat.d(goal.getTitle());
        Logcat.d(goal.getDescription());
    }

    public String getAttributeSkill() {
        if (goal.getAttributeId() != C.UNDEFINED) {
            Attribute attribute = AttributeQuery.getById(getRealm(), goal.getAttributeId());
            if (attribute != null) {
                String attrSkill = attribute.getTitle();
                if (goal.getSkillId() != C.UNDEFINED) {
                    Skill skill = SkillQuery.getById(getRealm(), goal.getSkillId());
                    if (skill != null) {
                        attrSkill += " - " + skill.getTitle();
                        return attrSkill;
                    } else {
                        goal.setSkillId(C.UNDEFINED);
                        GoalQuery.edit(getRealm(), goal);
                        return attrSkill;
                    }
                } else {
                    return attrSkill;
                }
            } else {
                goal.setAttributeId(C.UNDEFINED);
                goal.setSkillId(C.UNDEFINED);
                GoalQuery.edit(getRealm(), goal);
            }
        }
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
        attrSkill = getAttributeSkill();
        difficulty = getGoalDifficulty();
    }

    public void onSaveInstance(Bundle outState) {
        outState.putInt(C.SAVED_INSTANCE_GOAL_ID, id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Goal getGoal() {

        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public String getAttrSkill() {
        return attrSkill;
    }

    public void setAttrSkill(String attrSkill) {
        this.attrSkill = attrSkill;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public int getGoalAttributePosition() {
        if(goal.getAttributeId() == C.UNDEFINED) {
            return C.UNDEFINED;
        }

        return AttributeQuery.getById(getRealm(), goal.getAttributeId()).getPosition();
    }

    public String getGoalDifficulty() {
        switch (goal.getDifficulty()) {
            case EASY:
                return ResourceUtil.getString(R.string.easy);
            case NORMAL:
                return ResourceUtil.getString(R.string.normal);
            case HARD:
                return ResourceUtil.getString(R.string.hard);
            default:
            case UNDEFINED:
                return "";
        }
    }

    public void onGoalFinished() {
        if (goal.isFinished())
            return;

        goal.setFinished(true);
        GoalQuery.edit(getRealm(), goal);

        Profile profile = ProfileQuery.get(getRealm());

        float points;
        switch (goal.getDifficulty()) {
            default:
            case EASY:
                points = (profile.getNextLevelPoints() / 3);
                break;
            case NORMAL:
                points = (int)(profile.getNextLevelPoints() / 3 * 2);
                break;
            case HARD:
                points = (int)(profile.getNextLevelPoints());
                break;
        }

        if (profile.addPoints(points)) {
            handler.postDelayed(() -> DialogUtils.getLevelUpDialog(context, profile.getLevel()).show(), 450);
        }
        ProfileQuery.edit(getRealm(), profile);

        /*if (goal.getAttributeId() == C.UNDEFINED) {
            ArrayList<Attribute> attrs = AttributeQuery.getAll(getRealm());
            float attrpoints = (points / attrs.size());
            for (Attribute attribute : attrs) {
                attribute.addPoints(attrpoints);
                AttributeQuery.edit(getRealm(), attribute);
                float skillPoints = attrpoints / attribute.getSkills().size();
                for (Skill skill : attribute.getSkills()) {
                    skill.addPoints(skillPoints);
                    SkillQuery.edit(getRealm(), skill);
                }
            }
        } else {

        };*/
    }

    public void onGoalDelete() {
        GoalQuery.delete(getRealm(), goal.getId());
    }

    public void onGoalEdit(final CircularOverlayActivity activity) {
        final MyAnimationListener listener = new MyAnimationListener() {
            @Override
            public void onMyAnimationFinished() {
                Intent intent = new Intent(activity, GoalFormActivity.class);
                intent.putExtra(C.EXTRA_MODE, C.MODE_EDIT);
                intent.putExtra(C.EXTRA_GOAL_ID, goal.getId());
                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
            }
        };

        activity.circularRevealActivity(listener);
    }
}
