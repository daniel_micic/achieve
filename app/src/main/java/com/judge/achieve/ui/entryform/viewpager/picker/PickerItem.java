package com.judge.achieve.ui.entryform.viewpager.picker;

import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.Skill;

/**
 * Created by Daniel on 15.03.2017.
 */

public class PickerItem {

    int id;
    String title;
    boolean checked;

    boolean hasParentTitle;
    String parentTitle;

    public PickerItem(Attribute attribute, boolean checked) {
        this.id = attribute.getId();
        this.title = attribute.getTitle();
        this.checked = checked;
        this.hasParentTitle = false;
    }

    public PickerItem(Skill skill, boolean checked, boolean hasParentTitle, String parentTitle) {
        this.id = skill.getId();
        this.title = skill.getTitle();
        this.checked = checked;
        this.hasParentTitle = hasParentTitle;
        this.parentTitle = parentTitle;
    }

    public PickerItem(Exercise exercise, boolean checked, boolean hasParentTitle, String parentTitle) {
        this.id = exercise.getId();
        this.title = exercise.getTitle();
        this.checked = checked;
        this.hasParentTitle = hasParentTitle;
        this.parentTitle = parentTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isHasParentTitle() {
        return hasParentTitle;
    }

    public void setHasParentTitle(boolean hasParentTitle) {
        this.hasParentTitle = hasParentTitle;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }
}
