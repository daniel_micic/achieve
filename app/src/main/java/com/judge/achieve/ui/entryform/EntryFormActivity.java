package com.judge.achieve.ui.entryform;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.ActivityEntryFormBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.ui.entryform.viewpager.EntryFormAdapter;
import com.judge.achieve.ui.entryform.viewpager.picker.PickerAdapter;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;
import com.judge.achieve.utils.Utils;

import org.joda.time.DateTime;

public class EntryFormActivity extends BaseActivity {

    ActivityEntryFormBinding binding;

    EntryFormPresenter mPresenter;

    EntryFormAdapter adapter;

    PickerAdapter.ExerPickerListener attrListener = (checked, exId) -> {
        getPresenter().onAttributeSelected(exId, checked);
        if (adapter != null)
            adapter.onAttrSelected();
    };
    PickerAdapter.ExerPickerListener skillListener = (checked, exId) -> {
        getPresenter().onSkillSelected(exId, checked);
        if (adapter != null)
            adapter.onSkillSelected();
    };
    PickerAdapter.ExerPickerListener exerListener = (checked, exId) -> {
        getPresenter().onExerciseSelected(exId, checked);
    };

    int mMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");


        if (savedInstanceState == null) {
            Logcat.d("save instance was null");
            mMode = getIntent().getIntExtra(C.EXTRA_MODE, C.MODE_CREATE);
            if (mMode == C.MODE_CREATE) {
                DateTime selectedDay = new DateTime(getIntent().getLongExtra(C.EXTRA_SELECTED_DAY, DateTime.now().getMillis()));
                mPresenter = new EntryFormPresenter(this, selectedDay);
            } else {
                long id = getIntent().getLongExtra(C.EXTRA_ENTRY_ID, -1);
                Logcat.d(id);
                mPresenter = new EntryFormPresenter(this, id);
            }
        } else {
            Logcat.d("save instance was not null");
            mMode =  savedInstanceState.getInt(C.SAVED_INSTANCE_MODE, C.MODE_CREATE);
            mPresenter = new EntryFormPresenter(this, mMode, savedInstanceState);
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_entry_form);

        initViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logcat.d("on save instance");
        outState.putInt(C.SAVED_INSTANCE_MODE, mMode);
        mPresenter.onSaveInstace(outState);
    }

    @Override
    public void onBackPressed() {
        Logcat.d("back pressed");
        if (binding.viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
            setResult(C.RESULT_FAIL);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        } else {
            binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() - 1, true);
            handleFabs();
        }
    }

    @Override
    public EntryFormPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        if (mMode == C.MODE_CREATE) {
            setTitle(getString(R.string.create_entry));
        } else if (mMode == C.MODE_EDIT) {
            setTitle(getString(R.string.edit_entry));
        }

        setSupportActionBar(binding.createEntryToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        binding.createEntryToolbar.setNavigationOnClickListener(view -> EntryFormActivity.this.onBackPressed());

        adapter = new EntryFormAdapter(getSupportFragmentManager(),
                getPresenter(),
                getPresenter(),
                getPresenter(),
                attrListener,
                skillListener,
                exerListener);

        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setCurrentItem(0);
        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                handleFabs();
                updateTitles();
            }

            @Override
            public void onPageSelected(int position) {
                handleFabs();
                updateTitles();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.createEntryNextBtn.setOnClickListener(view -> {
            binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() + 1);
            Utils.hideInput(this);
        });
        binding.createEntryDoneBtn.setOnClickListener(view -> getPresenter().onDoneClicked());
    }

    private void handleFabs() {
        if (binding.viewPager.getCurrentItem() != 3) {
            if (!binding.createEntryNextBtn.isShown())
                binding.createEntryNextBtn.show();
            if (binding.createEntryDoneBtn.isShown())
                binding.createEntryDoneBtn.hide();
        } else {
            binding.createEntryNextBtn.hide();
            binding.createEntryDoneBtn.show();
        }
    }

    private void updateTitles() {
        switch(binding.viewPager.getCurrentItem()) {
            case 0:
                setTitle(ResourceUtil.getString(R.string.create_entry));
                break;
            case 1:
                setTitle(ResourceUtil.getString(R.string.pick_attr));
                break;
            case 2:
                setTitle(ResourceUtil.getString(R.string.pick_skill));
                break;
            case 3:
                setTitle(ResourceUtil.getString(R.string.pick_exer));
                break;
        }
    }

    public void showNoExerciseError() {
        Snackbar.make(binding.getRoot(), "Please select exercises", Snackbar.LENGTH_SHORT).show();
    }


}
