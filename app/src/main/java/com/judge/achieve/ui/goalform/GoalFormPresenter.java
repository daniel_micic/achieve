package com.judge.achieve.ui.goalform;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Goal;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.GoalQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 01.10.2016.
 */

public class GoalFormPresenter extends BasePresenter {

    private GoalFormActivity mView;
    private Context mContext;

    private Goal goal;
    private ArrayList<SpinnerItem> items = new ArrayList<>();

    private int mMode;


    public GoalFormPresenter(GoalFormActivity view, int mode, int id) {
        super();

        mView = view;
        mContext = view;
        mMode = mode;

        switch (mMode) {
            default:
            case C.MODE_CREATE:
                goal = new Goal();
                break;
            case C.MODE_EDIT:
                goal = GoalQuery.getById(getRealm(), id);
                break;
        }
    }

    public GoalFormPresenter(GoalFormActivity view, int mode, Bundle savedInstanceState) {
        super();
        Logcat.d("recreating from bundle");
        mView = view;
        mContext = view;
        mMode = mode;

        goal = savedInstanceState.getParcelable(C.SAVED_INSTANCE_GOAL);
    }

    public void onSaveInstace(Bundle outState) {
        Logcat.d("on save instance");

        setAndCheckDataFromView(false);

        outState.putParcelable(C.SAVED_INSTANCE_GOAL, goal);
    }

    public void setViews() {
        if (goal == null) {
            return;
        }

        mView.setGoalTitle(goal.getTitle());
        mView.setGoalDifficulty(goal.getDifficulty());
    }

    public String getGoalTitle() {
        return goal.getTitle();
    }

    public String getGoalDescription() {
        return goal.getDescription();
    }


    public Difficulty getGoalDifficulty() {
        return goal.getDifficulty();
    }

    public List<String> getSpinerData() {
        ArrayList<String> data= new ArrayList<>();
        data.add("None");
        for (Attribute attribute : AttributeQuery.getAll(getRealm())) {
            data.add(attribute.getTitle());
            items.add(new SpinnerItem(attribute.getId(), C.UNDEFINED));
            for (Skill skill : attribute.getSkills()) {
                data.add(attribute.getTitle() + " - " + skill.getTitle());
                items.add(new SpinnerItem(attribute.getId(), skill.getId()));
            }
        }
        return data;
    }

    public void onSubcategorySelected(int position) {
        if (position == 0) {
            goal.setAttributeId(C.UNDEFINED);
            goal.setSkillId(C.UNDEFINED);
        } else {
            goal.setAttributeId(items.get(position - 1).getAttrId());
            goal.setSkillId(items.get(position - 1).getSkillId());
        }
    }

    //returns wehter the view had all neccesary data
    public boolean setAndCheckDataFromView(boolean showError) {
        String title = mView.getGoalTitle();

        if (!TextUtils.isEmpty(title)) {
            goal.setTitle(title);
        } else if (showError){
            mView.setGoalTitleError();
            return false;
        }

        String description = mView.getGoalDescription();
        goal.setDescription(description);

        Difficulty difficulty = mView.getGoalDifficulty();

        if (difficulty != Difficulty.UNDEFINED) {
            goal.setDifficulty(difficulty);
        } else if (showError){
            mView.setGoalDifficultyError();
            return false;
        }

        return true;
    }

    public void onDoneClicked() {

        if (!setAndCheckDataFromView(true)) {
            return;
        }

        if (mMode == C.MODE_CREATE) {
            createGoal();
        } else {
            editGoal();
        }

        mView.setResult(C.RESULT_OK);
        mView.finish();
        mView.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void createGoal() {
        GoalQuery.copyToRealm(getRealm(), goal);
    }

    public void editGoal() {
        GoalQuery.edit(getRealm(), goal);
    }

    static class SpinnerItem {

        int attrId;
        int skillId;

        public SpinnerItem(int attrId, int skillId) {
            this.attrId = attrId;
            this.skillId = skillId;
        }

        public int getAttrId() {
            return attrId;
        }

        public void setAttrId(int attrId) {
            this.attrId = attrId;
        }

        public int getSkillId() {
            return skillId;
        }

        public void setSkillId(int skillId) {
            this.skillId = skillId;
        }
    }
}
