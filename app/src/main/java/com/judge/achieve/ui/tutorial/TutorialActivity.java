package com.judge.achieve.ui.tutorial;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.ActivityTutorialBinding;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.intro.IntroFormActivity;
import com.judge.achieve.ui.tutorial.viewpager.TutorialAdapter;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MyAnimationListener;
import com.judge.achieve.utils.PrefUtils;
import com.judge.achieve.utils.ResourceUtil;

public class TutorialActivity extends CircularOverlayActivity {


    TutorialAdapter adapter;
    ActivityTutorialBinding binding;

    TutorialPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        PrefUtils.saveBool(C.PREF_FIRST_RUN, false);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_tutorial);

        mPresenter = new TutorialPresenter(this);

        initViews();

        setViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {

        if (binding.viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() - 1);
        }
    }

    @Override
    public TutorialPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        adapter = new TutorialAdapter(getSupportFragmentManager());
        binding.viewPager.setAdapter(adapter);

        binding.next.setOnClickListener(view -> {
            if (binding.viewPager.getCurrentItem() == 3) {
                int mWidth= this.getResources().getDisplayMetrics().widthPixels;
                int mHeight= this.getResources().getDisplayMetrics().heightPixels;
                circularRevealActivity(mWidth / 2, mHeight / 2, new MyAnimationListener() {
                    @Override
                    public void onMyAnimationFinished() {
                        Intent intent = new Intent(TutorialActivity.this, IntroFormActivity.class);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
                        finish();
                    }
                });
            } else {
                binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() + 1);
            }
        });

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    binding.next.setText(ResourceUtil.getString(R.string.begin));
                } else {
                    binding.next.setText(ResourceUtil.getString(R.string.next));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setViews() {

    }

    @Override
    public void setOverlayGone() {
        binding.overlay.setVisibility(View.GONE);
    }

    @Override
    public View getOverlay() {
        return binding.overlay;
    }
}
