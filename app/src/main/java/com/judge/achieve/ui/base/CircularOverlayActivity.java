package com.judge.achieve.ui.base;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;

import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MyAnimationListener;

/**
 * Created by Daniel on 17.10.2016.
 */

public abstract class CircularOverlayActivity extends BaseActivity {

    //variables
    public float[] actualTouchPosition;

    @Override
    protected void onResume() {
        super.onResume();
        setOverlayGone();
    }

    public abstract void setOverlayGone();

    public abstract View getOverlay();

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Logcat.d("touch");
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            //Logcat.d("action down");
            actualTouchPosition = new float[]{ev.getX(), ev.getY()};
        }
        return super.dispatchTouchEvent(ev);
    }

    @TargetApi(21)
    public void circularRevealActivity(int x, int y, MyAnimationListener listener) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            Logcat.d("circular reveal");
            View overlay = getOverlay();

            if (overlay != null) {
        /*float xRadius = x >= (mRoot.getWidth() / 2) ? x : (mRoot.getWidth() - x);
        float yRadius = y >= (mRoot.getHeight() / 2) ? y : (mRoot.getHeight() - y);
        float finalRadius = Math.max(xRadius, yRadius);*/

                // create the animator for this view (the start radius is zero)
                Animator circularReveal = ViewAnimationUtils.createCircularReveal(overlay, x, y, 0, getRadius(x, y));
                circularReveal.setDuration(400);
                circularReveal.addListener(listener);

                // make the view visible and start the animation
                overlay.setVisibility(View.VISIBLE);
                Logcat.d("start");
                circularReveal.start();
            }
        } else {
            listener.onMyAnimationFinished();
        }
    }

    @TargetApi(21)
    public void circularRevealActivity(MyAnimationListener listener) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Logcat.d("circular reveal");
        /*float xRadius = x >= (mRoot.getWidth() / 2) ? x : (mRoot.getWidth() - x);
        float yRadius = y >= (mRoot.getHeight() / 2) ? y : (mRoot.getHeight() - y);
        float finalRadius = Math.max(xRadius, yRadius);*/

            View overlay = getOverlay();

            if (overlay != null) {

                int x = (int) actualTouchPosition[0];
                int y = (int) actualTouchPosition[1] - getStatusBarHeight();

                // create the animator for this view (the start radius is zero)
                Animator circularReveal = ViewAnimationUtils.createCircularReveal(overlay, x, y, 0, getRadius(x, y));
                circularReveal.setInterpolator(new AccelerateInterpolator());
                circularReveal.setDuration(400);
                circularReveal.addListener(listener);

                // make the view visible and start the animation
                overlay.setVisibility(View.VISIBLE);
                Logcat.d("start");
                circularReveal.start();
            }
        } else {
            listener.onMyAnimationFinished();
        }
    }

    public float getRadius(int x, int y) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = size.y;
        float halfWidth = width / 2;
        float halfHeight = height / 2;

        float a;
        float b;

        if (x <= halfWidth ) {
            a = width - x;
        } else {
            a = x;
        }

        if (y <= halfHeight) {
            b = height - y;
        } else {
            b = y;
        }

        return (float)Math.sqrt(((a*a) + (b*b)));
    }



    /*@Override
    public boolean onTouchEvent(MotionEvent event) {
        Logcat.d("on touch");
        actualTouchPosition = new float[] {event.getX(), event.getY()};
        return super.onTouchEvent(event);
    }*/

    private int getStatusBarHeight() {
        Rect rectgle= new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
        return rectgle.top;
    }

}
