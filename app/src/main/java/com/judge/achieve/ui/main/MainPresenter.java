package com.judge.achieve.ui.main;

import android.net.Uri;
import android.text.TextUtils;

import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.Profile;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.ExerciseQuery;
import com.judge.achieve.persistence.query.ProfileQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.DateUtils;
import com.judge.achieve.utils.IABUtils;
import com.judge.achieve.utils.InitialGenerator;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 23.07.2016.
 */
public class MainPresenter extends BasePresenter {

    private MainActivity view;

    private Profile profile;

    private ArrayList<Attribute> attributes;

    private int oldLevel;

    public MainPresenter(MainActivity view) {
        super();

        this.view = view;

        if (DateUtils.isNewDay()) {
            updateExerciseOnNewDay();
        }

        /*if (!PrefUtils.getBool(C.PREF_FIRST_RUN)) {
            PrefUtils.saveBool(C.PREF_FIRST_RUN, true);
            Logcat.d("This fucking error showed up again - might have been in First Run check");
            InitialGenerator.generateProfile(getRealm());
        }*/

        profile = ProfileQuery.get(getRealm());
        if (profile != null) {
            oldLevel = profile.getLevel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getRealm() != null) {
            // TODO check why it was null ...
            profile = ProfileQuery.get(getRealm());

            if (profile == null) {
                Logcat.d("This fucking error showed up again");
                profile = InitialGenerator.generateProfile(getRealm());
            }

            ProfileQuery.edit(getRealm(), profile);

            attributes = AttributeQuery.getAll(getRealm());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        oldLevel = profile.getLevel();
    }

    public void updateExerciseOnNewDay() {
        List<Exercise> list = ExerciseQuery.getAll(getRealm());
        for (Exercise ex : list) {
            ex.setCountForDay(0);
            ExerciseQuery.edit(getRealm(), ex);
        }
    }

    public void onCategoryDelete(Attribute attribute) {
        /*Iterator<Attribute> iterator = profile.getCategories().iterator();
        while (iterator.hasNext()) {
            Attribute cat = iterator.next();
            if (cat.getAttributeId() == attribute.getAttributeId()) {
                iterator.remove();
                Logcat.d("removing from list " + cat.getTitle());
                break;
            }
        }*/

        attributes.remove(attribute);
        AttributeQuery.delete(getRealm(), attribute.getId());
    }

    public void onEditProfile(String name, Uri outputFileUri) {
        if (profile == null) {
            profile = ProfileQuery.get(getRealm());
        }

        profile.setName(TextUtils.isEmpty(name) ? "Unnamed" : name);
        if (outputFileUri != null)
            profile.setProfilePic(outputFileUri.toString());

        ProfileQuery.edit(getRealm(), profile);
    }


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    public boolean isNewLevel() {
        boolean is = profile.getLevel() > oldLevel;
        oldLevel = profile.getLevel();
        return is;
    }

    public boolean canCreateAttirbute() {
        if (IABUtils.hasAttributtes()) {
            return true;
        } else {
            if (attributes.size() < 3) {
                return true;
            }
        }
        return false;
    }

    public boolean hasStatisticsAccess() {
        if (IABUtils.hasStatistics()) {
            return true;
        }
        return false;
    }

    public boolean hasPlannerAccess() {
        if (IABUtils.hasPlanner()) {
            return true;
        }
        return false;
    }

    public boolean checkForExercises() {
        return ExerciseQuery.getAll(getRealm()).size() > 0;
    }

}
