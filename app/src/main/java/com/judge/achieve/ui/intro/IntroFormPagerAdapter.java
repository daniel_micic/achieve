package com.judge.achieve.ui.intro;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.R;

/**
 * Created by aa on 25. 12. 2016.
 */

public class IntroFormPagerAdapter extends PagerAdapter {

    IntroFormActivity activity;

    public IntroFormPagerAdapter(IntroFormActivity activity) {
        this.activity = activity;
    }

    public Object instantiateItem(ViewGroup collection, int position) {

        int resId = 0;
        switch (position) {
            case 0:
                resId = R.id.page_one;
                break;
            case 1:
                resId = R.id.page_two;
                break;
        }
        return activity.findViewById(resId);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }
}
