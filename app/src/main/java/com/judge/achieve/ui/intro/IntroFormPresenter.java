package com.judge.achieve.ui.intro;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.judge.achieve.common.C;
import com.judge.achieve.model.Profile;
import com.judge.achieve.persistence.query.ProfileQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.ui.main.MainActivity;
import com.judge.achieve.utils.InitialGenerator;
import com.judge.achieve.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 01.10.2016.
 */

public class IntroFormPresenter extends BasePresenter {

    private IntroFormActivity mView;

    Profile profile;
    Uri photoUri;

    public IntroFormPresenter(IntroFormActivity view) {
        super();
        mView = view;

        profile = InitialGenerator.generateProfile(getRealm());

    }

    public void onSaveInstance(Bundle out) {
        out.putParcelable(C.SAVED_INSTANCE_PROFILE, profile);
    }


    public void editProfile(String name, String photoUrl) {

    }

    public void onExamplePicked() {
        InitialGenerator.generateExample(getRealm());
        mView.goToMain();
    }

    public void onTemplatePicked() {
        InitialGenerator.generateTemplate(getRealm());
        mView.goToMain();
    }

    public void onEmptyPicked() {
        InitialGenerator.generateEmpty(getRealm());
        mView.goToMain();
    }

    public void pickPhoto() {
        final File imageFile = Utils.getOutputMediaFile();
        photoUri = Uri.fromFile(imageFile);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = mView.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        mView.startActivityForResult(chooserIntent, MainActivity.AVATAR_REQUEST_CODE);
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public void saveProfile(String name) {
        if (TextUtils.isEmpty(name)) {
            profile.setName("Unsung Hero");
        } else {
            profile.setName(name);
        }
        if (photoUri != null)
            profile.setProfilePic(photoUri.toString());
        ProfileQuery.edit(getRealm(), profile);
    }


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
