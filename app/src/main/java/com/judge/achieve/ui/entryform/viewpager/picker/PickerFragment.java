package com.judge.achieve.ui.entryform.viewpager.picker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.BR;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.FragmentCreateEntryPickerBinding;
import com.judge.achieve.utils.ResourceUtil;

/**
 * Created by Daniel on 28.02.2017.
 */

public class PickerFragment extends Fragment {

    FragmentCreateEntryPickerBinding binding;

    PickerAdapter.ExerPickerAdapterType type;

    PickerAdapter adapter;

    PickerAdapter.ExerPickerProvider provider;

    PickerAdapter.ExerPickerListener listener;




    public static PickerFragment newInstance(PickerAdapter.ExerPickerAdapterType type) {

        Bundle args = new Bundle();
        args.putSerializable(C.EXTRA_EXER_PICKER_TYPE, type);

        PickerFragment fragment = new PickerFragment();
        fragment.setArguments(args);

        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        type = (PickerAdapter.ExerPickerAdapterType)getArguments().getSerializable(C.EXTRA_EXER_PICKER_TYPE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_entry_picker, container, false);

        binding.setVariable(BR.provider, provider);

        adapter = new PickerAdapter(type, provider, listener);
        binding.exerPickerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.exerPickerList.setAdapter(adapter);
        
        switch (type) {
            case SKILL:
                binding.noSelected.setText(ResourceUtil.getString(R.string.no_skill_available));
                break;
            case EXER:
                binding.noSelected.setText(ResourceUtil.getString(R.string.no_exer_available));
                break;

        }

        update();

        return binding.getRoot();
    }

    public FragmentCreateEntryPickerBinding getBinding() {
        return binding;
    }

    public void setBinding(FragmentCreateEntryPickerBinding binding) {
        this.binding = binding;
    }


    public void setProvider(PickerAdapter.ExerPickerProvider provider) {
        this.provider = provider;
    }

    public void setListener(PickerAdapter.ExerPickerListener listener) {
        this.listener = listener;
    }

    public PickerAdapter.ExerPickerAdapterType getType() {
        return type;
    }

    public void setType(PickerAdapter.ExerPickerAdapterType type) {
        this.type = type;
    }

    public void update() {
        switch (type) {
            case SKILL:
                if (provider.getSkills() == null || provider.getSkills().size() == 0) {
                    binding.noSelected.setVisibility(View.VISIBLE);
                    binding.exerPickerList.setVisibility(View.GONE);
                } else {
                    binding.noSelected.setVisibility(View.GONE);
                    binding.exerPickerList.setVisibility(View.VISIBLE);
                }
                break;
            case EXER:
                if (provider.getExercises() == null || provider.getExercises().size() == 0) {
                    binding.noSelected.setVisibility(View.VISIBLE);
                    binding.exerPickerList.setVisibility(View.GONE);
                } else {
                    binding.noSelected.setVisibility(View.GONE);
                    binding.exerPickerList.setVisibility(View.VISIBLE);
                }
                break;

        }
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

}
