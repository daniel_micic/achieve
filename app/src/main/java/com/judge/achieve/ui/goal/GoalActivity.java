package com.judge.achieve.ui.goal;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ActivityGoalBinding;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.goal.viewpager.AddGoalFragment;
import com.judge.achieve.ui.goal.viewpager.GoalAdapter;
import com.judge.achieve.ui.goal.viewpager.GoalFragment;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;
import com.judge.achieve.utils.ZoomOutPageTransformer;

public class GoalActivity extends CircularOverlayActivity {

    GoalActivityPresenter presenter;

    ActivityGoalBinding binding;
    GoalAdapter adapter;

    private int currentPosition = 0;

    boolean shouldAnimate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_goal);

        initViews();

        presenter = new GoalActivityPresenter(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        setViews();
    }

    @Override
    public void onBackPressed() {
        Logcat.d("exit animation");
        if (adapter.getItem(binding.goalsPager.getCurrentItem()) instanceof GoalFragment)
            ((GoalFragment)adapter.getItem(binding.goalsPager.getCurrentItem())).exitAnimation();
        else
            ((AddGoalFragment)adapter.getItem(binding.goalsPager.getCurrentItem())).exitAnimation();

        new Handler().postDelayed(
                () -> binding.getRoot().animate()
                    .alpha(0f)
                    .setDuration(500)
                    .setInterpolator(new AccelerateInterpolator())
                    .withEndAction(() ->{
                        if (!super.isFinishing()) {
                            super.onBackPressed();
                            overridePendingTransition(R.anim.do_not_move, android.R.anim.fade_out);
                        }
                    })
                , 100);

    }

    @Override
    public void setOverlayGone() {
        binding.overlay.setVisibility(View.GONE);
    }

    @Override
    public View getOverlay() {
        return binding.overlay;
    }

    @Override
    protected void onPause() {
        super.onPause();

        currentPosition = binding.goalsPager.getCurrentItem();
        Logcat.d(currentPosition);
    }

    @Override
    public GoalActivityPresenter getPresenter() {
        return presenter;
    }

    public void initViews() {
        binding.goalsPager.setPageMargin((int) ResourceUtil.getDimension(R.dimen.default_padding));
        binding.goalsPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }

    public void setViews() {
        adapter = new GoalAdapter(getSupportFragmentManager(), getPresenter(), shouldAnimate);
        shouldAnimate = false;
        binding.goalsPager.setAdapter(adapter);
        Logcat.d(currentPosition);
        binding.goalsPager.setCurrentItem(currentPosition);

    }

    public void removeFragment(int id) {

        currentPosition = binding.goalsPager.getCurrentItem();
        getPresenter().removeFragment(currentPosition, id);
        setViews();
    }
}
