package com.judge.achieve.ui.main;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.judge.achieve.R;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel on 23.07.2016.
 */
public class AttributeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int CATEGORY_TYPE = 0;

    private Context mContext;
    private ArrayList<Attribute> mData;
    private AttributeListener mListener;

    public interface AttributeListener {
        void onAttributeClicked(View view, int id);
        void onAttributeEdit(Attribute category);
        void onAttributeDelete(int position, Attribute category);
    }

    public AttributeAdapter(Context context, ArrayList data, AttributeListener listener ) {
        this.mContext = context;
        this.mData = data;
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
            return CATEGORY_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final AttributeViewHolder categoryViewHolder = (AttributeViewHolder) holder;
        final Attribute c = mData.get(position);

        //categoryViewHolder.mBackground.getBackground().setColorFilter(Utils.getCategoryColor(position), PorterDuff.Mode.SRC_ATOP);
        categoryViewHolder.mBackground.setBackgroundColor(Utils.getCategoryColor(position));
        categoryViewHolder.mTitle.setText(c.getTitle());
        categoryViewHolder.mPoints.setText(String.valueOf(c.getProgress()));
        categoryViewHolder.mMore.setOnClickListener(view -> {
            PopupMenu menu = new PopupMenu(mContext, categoryViewHolder.mMore, Gravity.RIGHT);
            menu.inflate(R.menu.edit_delete_menu);
            menu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.delete:
                        mListener.onAttributeDelete(position, c);
                        break;
                    case R.id.edit:
                        mListener.onAttributeEdit(c);
                        break;
                }
                return false;
            });
            menu.show();
        });
        categoryViewHolder.click.setOnClickListener(categoryViewHolder);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_attribute, parent, false);
        return new AttributeViewHolder(view);
    }

    public class AttributeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.attribute_background) public RelativeLayout mBackground;
        @BindView(R.id.clickable_area) public RelativeLayout click;
        @BindView(R.id.attribute_title) public TextView mTitle;
        @BindView(R.id.attribute_points) public TextView mPoints;
        @BindView(R.id.attribute_more) public ImageView mMore;

        public AttributeViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View view) {
            mListener.onAttributeClicked(view, mData.get(getPosition()).getId());
        }
    }

    public List<Attribute> getData() {
        return mData;
    }
}
