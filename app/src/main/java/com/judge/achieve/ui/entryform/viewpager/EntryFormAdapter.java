package com.judge.achieve.ui.entryform.viewpager;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.judge.achieve.ui.entryform.viewpager.picker.PickerAdapter;
import com.judge.achieve.ui.entryform.viewpager.form.FormFragment;
import com.judge.achieve.ui.entryform.viewpager.picker.PickerFragment;

/**
 * Created by Daniel on 24.01.2017.
 */

public class EntryFormAdapter extends FragmentStatePagerAdapter {

    private boolean entryAnimation = true;

    private FormFragment formFragment;
    
    private PickerFragment attrFragment;
    private PickerFragment skillFragment;
    private PickerFragment exerFragment;

    EntryFormProvider entryFormProvider;
    PickerAdapter.ExerPickerProvider exerPickerProvider;
    FormFragment.OnReccurenceSetListener reccurenceSetListener;

    PickerAdapter.ExerPickerListener skillListener;
    PickerAdapter.ExerPickerListener attrListener;
    PickerAdapter.ExerPickerListener exerListener;

    public EntryFormAdapter(FragmentManager fragmentManager,
                            EntryFormProvider entryFormProvider,
                            PickerAdapter.ExerPickerProvider exerPickerProvider,
                            FormFragment.OnReccurenceSetListener reccurenceSetListener,
                            PickerAdapter.ExerPickerListener attrListener,
                            PickerAdapter.ExerPickerListener skillListener,
                            PickerAdapter.ExerPickerListener exerListener) {
        super(fragmentManager);
        this.entryFormProvider = entryFormProvider;
        this.exerPickerProvider = exerPickerProvider;
        this.reccurenceSetListener = reccurenceSetListener;

        this.exerListener = exerListener;
        this.skillListener = skillListener;
        this.attrListener = attrListener;


    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            default:
            case 0:
                if (formFragment == null) {
                    formFragment = new FormFragment();
                    formFragment.setProvider(entryFormProvider);
                    formFragment.setRecurrenceSetListener(reccurenceSetListener);
                }
                return formFragment;
            case 1:
                if (attrFragment == null) {
                    attrFragment = PickerFragment.newInstance(PickerAdapter.ExerPickerAdapterType.ATTR);
                    attrFragment.setProvider(exerPickerProvider);
                    attrFragment.setListener(attrListener);
                }
                return attrFragment;
            case 2:
                if (skillFragment == null) {
                    skillFragment  = PickerFragment.newInstance(PickerAdapter.ExerPickerAdapterType.SKILL);
                    skillFragment.setProvider(exerPickerProvider);
                    skillFragment.setListener(skillListener);
                }
                return skillFragment;
            case 3:
                if (exerFragment == null) {
                    exerFragment  = PickerFragment.newInstance(PickerAdapter.ExerPickerAdapterType.EXER);
                    exerFragment.setProvider(exerPickerProvider);
                    exerFragment.setListener(exerListener);
                }
                return exerFragment;
        }

    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    public void onAttrSelected() {
        if (skillFragment != null)
            skillFragment.update();
        if (exerFragment != null)
            exerFragment.update();
    }

    public void onSkillSelected() {
        if (exerFragment != null)
            exerFragment.update();
    }

}
