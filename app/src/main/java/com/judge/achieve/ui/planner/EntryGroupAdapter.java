package com.judge.achieve.ui.planner;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ItemEntryGroupBinding;
import com.judge.achieve.utils.ResourceUtil;

import java.util.List;

/**
 * Created by aa on 18. 2. 2017.
 */

public class EntryGroupAdapter extends RecyclerView.Adapter<EntryGroupAdapter.ExerciseGroupViewHolder> {

    private Context contex;
    private EntryAdapter.PlannerEntryProvider entryProvider;
    private int entryPosition;

    public EntryGroupAdapter(PlannerActivity activity,
                             EntryAdapter.PlannerEntryProvider provider,
                             int entryPostion) {
        this.contex = activity;
        this.entryProvider = provider;
        this.entryPosition = entryPostion;
    }

    @Override
    public ExerciseGroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemEntryGroupBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_entry_group,parent,false);
        ExerciseGroupViewHolder vh = new ExerciseGroupViewHolder(binding);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ExerciseGroupViewHolder holder, final int position) {
        Pair<Integer, Pair<Integer, List<Integer>>> entryGroupIds = entryProvider.getEntryGroupForList(entryPosition, position);

        if (position != getItemCount() - 1) {
            holder.binding.groupRoot.setPadding(0,0,0, ResourceUtil.getDimensionInPixel(R.dimen.default_padding));
        }

        String attribute = entryProvider.getAttributeName(entryGroupIds.first) + " - ";
        String skill = entryProvider.getSkillName(entryGroupIds.second.first);
        String exercises = "";

        boolean first = true;
        for (Integer exercise : entryGroupIds.second.second) {
            if (!first) {
                exercises += System.getProperty("line.separator");
            }
            exercises += entryProvider.getExerciseName(exercise, entryGroupIds.second.first);
            first = false;

        }


        //holder.binding.entryDot.setColorFilter(entryProvider.getAttributeColor(entryGroupIds.first), PorterDuff.Mode.MULTIPLY);
        holder.binding.attSkillTitle.setText(attribute + skill);
        //holder.binding.attSkillTitle.setBackground(entryProvider.getAttributeColor(entryGroupIds.first));
        holder.binding.exerciseTitles.setText(exercises);
    }

    @Override
    public int getItemCount() {
        return entryProvider.getGroupForEntryCount(entryPosition);
    }


    public static class ExerciseGroupViewHolder extends RecyclerView.ViewHolder {

        ItemEntryGroupBinding binding;

        public ExerciseGroupViewHolder(ItemEntryGroupBinding binding){
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
