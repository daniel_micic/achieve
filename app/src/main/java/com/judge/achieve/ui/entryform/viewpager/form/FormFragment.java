package com.judge.achieve.ui.entryform.viewpager.form;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codetroopers.betterpickers.recurrencepicker.EventRecurrence;
import com.codetroopers.betterpickers.recurrencepicker.LinearLayoutWithMaxWidth;
import com.codetroopers.betterpickers.recurrencepicker.RecurrencePickerDialogFragment;
import com.judge.achieve.BR;
import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.FragmentCreateEntryBinding;
import com.judge.achieve.ui.entryform.viewpager.EntryFormProvider;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;

import org.joda.time.DateTime;

/**
 * Created by Daniel on 28.02.2017.
 */

public class FormFragment extends Fragment {

    FragmentCreateEntryBinding binding;

    EntryFormProvider provider;

    OnReccurenceSetListener recurrenceSetListener;

    public interface OnReccurenceSetListener {
        String setEntryReccurence(EventRecurrence reccurence);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_entry, container, false);

        binding.setVariable(BR.provider, provider);

        //binding.createEntryTitleEt.setText(provider.getEntry().);

        binding.createEntryStartsPicker.setText(provider.getFormattedStartDate());
        binding.createEntryStartsPicker.setOnClickListener(view -> {

            TimePickerDialog.OnTimeSetListener timeSetListener = (timePicker, hour, minute) -> {
                provider.getEntry().setStartDate(provider.getEntry().getStartDate().withHourOfDay(hour).withMinuteOfHour(minute));
                binding.createEntryStartsPicker.setText(provider.getFormattedStartDate());
            };

            DatePickerDialog.OnDateSetListener dateSetListener = (datePicker, year, month, day) -> {
                provider.getEntry().setStartDate(new DateTime().withYear(year).withMonthOfYear(month + 1).withDayOfMonth(day).withTimeAtStartOfDay());

                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        R.style.DatePickerDialogTheme,
                        timeSetListener,
                        DateTime.now().getHourOfDay(),
                        DateTime.now().getMinuteOfHour(),
                        false
                );
                timePickerDialog.show();
            };

            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    getActivity(),
                    R.style.DatePickerDialogTheme,
                    dateSetListener,
                    DateTime.now().getYear(),
                    DateTime.now().getMonthOfYear() - 1,
                    DateTime.now().getDayOfMonth()
            );

            datePickerDialog.show();
        });

        if (provider.getMode() == C.MODE_EDIT)
            binding.createEntryRepeatPicker.setText(provider.getEntryRecurrenceString());

        binding.createEntryRepeatLayout.setOnClickListener(view -> {
            RecurrencePickerDialogFragment dialog = new RecurrencePickerDialogFragment();
            Bundle bundle = dialog.getArguments();
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putLong("bundle_event_start_time", provider.getEntry().getStartDate().getMillis());


            dialog.setArguments(bundle);

            dialog.setOnRecurrenceSetListener(rrule -> {
                if (rrule != null) {
                    Logcat.d("set");
                    EventRecurrence recurrence = new EventRecurrence();
                    recurrence.parse(rrule);
                    binding.createEntryRepeatPicker.setText(recurrenceSetListener.setEntryReccurence(recurrence));
                    dialog.dismiss();
                }
            });
            dialog.setOnDismissListener(dialoginterface -> {
                Logcat.d("dismissed");
                dialog.dismiss();
            });
            dialog.show(getFragmentManager(), "dialog");
            getActivity().getSupportFragmentManager().executePendingTransactions();

            LinearLayoutWithMaxWidth layout = (LinearLayoutWithMaxWidth) dialog.getDialog().getWindow().findViewById(R.id.weekGroup);
            for (int i = 0; i < layout.getChildCount(); i++) {
                View child = layout.getChildAt(i);
                child.getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimaryOrange), PorterDuff.Mode.SRC_ATOP);
            }
            layout = (LinearLayoutWithMaxWidth) dialog.getDialog().getWindow().findViewById(R.id.weekGroup2);
            for (int i = 0; i < layout.getChildCount(); i++) {
                View child = layout.getChildAt(i);
                child.getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimaryOrange), PorterDuff.Mode.SRC_ATOP);
            }

            dialog.getDialog().getWindow().findViewById(R.id.endCount).getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimaryOrange), PorterDuff.Mode.SRC_ATOP);
            dialog.getDialog().getWindow().findViewById(R.id.interval).getBackground().setColorFilter(ResourceUtil.getColor(R.color.colorPrimaryOrange), PorterDuff.Mode.SRC_ATOP);

        });

        return binding.getRoot();
    }

    public FragmentCreateEntryBinding getBinding() {
        return binding;
    }

    public void setBinding(FragmentCreateEntryBinding binding) {
        this.binding = binding;
    }

    public void setProvider(EntryFormProvider provider) {
        this.provider = provider;
    }

    public void setRecurrenceSetListener(OnReccurenceSetListener recurrenceSetListener) {
        this.recurrenceSetListener = recurrenceSetListener;
    }
}
