package com.judge.achieve.ui.statistics;

import android.os.Bundle;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.judge.achieve.R;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.ExerciseHistory;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.ResourceUtil;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Daniel on 01.10.2016.
 */

public class StatisticsPresenter extends BasePresenter {

    protected enum ValueType {
        SKILL_POINTS, EXER_COUNT, AMOUNT_XP
    }

    protected enum Mode {
        DAILY, AGGREGATE
    }

    protected enum ChartType {
        BAR, LINE
    }

    private StatisticsActivity view;

    private int attributeSelectedPos = 0;
    private int skillSelectedPos = 0;
    private int exerciseSelectedPos = 0;

    private int attrId = -1;
    private int skillId = -1;

    private boolean allAttributes = true;
    private boolean allSkills = true;
    private boolean allExercises = true;

    private DateTime startDate = DateTime.now().withTimeAtStartOfDay().minusDays(7);
    private DateTime endDate = DateTime.now().withTimeAtStartOfDay();

    private boolean lastWeek = true;
    private boolean lastMonth = false;
    private boolean lastYear = false;
    private boolean custom = false;

    private Mode mode = Mode.DAILY;
    private ChartType chartType = ChartType.BAR;
    private ValueType valueType = ValueType.EXER_COUNT;

    private Attribute attributeSelected;
    private int actualMax= 0;

    public StatisticsPresenter(StatisticsActivity view) {
        super();
        this.view = view;

        attributeSelected = AttributeQuery.getAll(getRealm()).get(attributeSelectedPos);
    }

    public StatisticsPresenter(StatisticsActivity view, int attrId, int skillId) {
        super();
        this.view = view;

        this.attrId = attrId;
        this.skillId = skillId;

        attributeSelected = AttributeQuery.getById(getRealm(), attrId);
    }

    public void onSaveInstace(Bundle outState) {

    }

    public int getActualMax() {
        return actualMax;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public ChartType getChartType() {
        return chartType;
    }

    public void setChartType(ChartType chartType) {
        this.chartType = chartType;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public BarData getLineData() {
        List<Integer> values = getGraphData();
        List<BarEntry> entries = new ArrayList<>();

        if (values == null || values.isEmpty()) {
            return null;
        }

        int xValue = 0;
        for (Integer value : values) {
            BarEntry entry = new BarEntry(xValue++, value);
            entries.add(entry);
        }

        BarDataSet set = new BarDataSet(entries, "Daily");
        set.setValueTextColor(ResourceUtil.getColor(R.color.white));
        set.setValueFormatter((value, entry, dataSetIndex, viewPortHandler) ->
                value == 0 ? "" : String.valueOf((int) value));
        return new BarData(set);
    }

    public LineData getBarData() {
        List<Integer> values = getGraphData();
        List<Entry> entries = new ArrayList<>();

        if (values == null || values.isEmpty()) {
            return null;
        }

        int xValue = 0;
        for (Integer value : values) {
            Entry entry = new Entry(xValue++, value);
            entries.add(entry);
        }

        LineDataSet set = new LineDataSet(entries, "Aggregate");
        set.setValueTextColor(ResourceUtil.getColor(R.color.white));

        if (values.size() < 20) {
            set.setValueFormatter((value, entry, dataSetIndex, viewPortHandler) ->
                    value == 0 ? "" : String.valueOf((int) value));
        } else if (values.size() < 100) {
            set.setValueFormatter((value, entry, dataSetIndex, viewPortHandler) -> {
                Logcat.d(dataSetIndex);
                if (dataSetIndex % 2 == 0) {
                    return value == 0 ? "" : String.valueOf((int) value);
                } else return "";
            });
            set.setDrawCircles(false);
        } else if (values.size() < 1000) {
            set.setValueFormatter((value, entry, dataSetIndex, viewPortHandler) -> {
                Logcat.d(dataSetIndex);
                if (dataSetIndex % 10 == 0) {
                    return value == 0 ? "" : String.valueOf((int) value);
                } else return "";
            });
            set.setDrawCircles(false);
        }

        return new LineData(set);
    }

    public List<Integer> getGraphData() {
        if (allAttributes) {

            List<Exercise> list = new ArrayList<>();
            for (Attribute attribute : AttributeQuery.getAll(getRealm())) {
                for (Skill skill : attribute.getSkills()) {
                    list.addAll(skill.getExercises());
                }
            }
            return generateGraphData(list);

        } else if (allSkills) {

            List<Exercise> list = new ArrayList<>();
            for (Skill skill : attributeSelected.getSkills()) {
                list.addAll(skill.getExercises());
            }
            return generateGraphData(list);

        } else if (allExercises) {

            return generateGraphData(attributeSelected.getSkills().get(skillSelectedPos).getExercises());

        } else {

            Exercise e = attributeSelected.getSkills().get(skillSelectedPos).getExercises().get(exerciseSelectedPos);
            return generateGraphData(Arrays.asList(e));

        }
    }

    private List<Integer> generateGraphData(final List<Exercise> exercises) {

        // cycle to check if there's any statistics available
        for (Exercise e : exercises) {
            if (e.getStatistics().isEmpty()) {
                //Logcat.d(exercises.indexOf(e) + " " + (exercises.size() - 1));
                if (exercises.indexOf(e) == exercises.size() - 1) {
                    return null;
                }
            } else {
                break;
            }
        }

        actualMax = 0;

        final List<Integer> yVals = new ArrayList<>();

        Thread thread = new Thread() {
            @Override
            public void run() {
                DateTime actualDate = startDate;
                int value = 0;
                if (mode == Mode.AGGREGATE) {
                    // TODO get total count from before start date
                }
                // iterate through selected interval
                while (actualDate.isBefore(endDate) || actualDate.isEqual(endDate)) {

                    if (mode == Mode.DAILY)
                        value = 0;

                    for (Exercise e : exercises) {
                        for (ExerciseHistory eh : e.getStatistics()) {
                            if (eh.getDate().isEqual(actualDate)) {

                                switch (valueType) {
                                    case EXER_COUNT:
                                        value += eh.getCountForDay();
                                        break;
                                    /* TODO finish when time, provide Skill points gain
                                    case SKILL_POINTS:
                                        value += (int)((eh.getCountForDay() * e.getPointsForExercise()) / C.BASE_LEVEL_XP);
                                        break;
                                    */
                                    case AMOUNT_XP:
                                        value += eh.getPoints();
                                        break;
                                }
                                //Logcat.d(valueType);
                                if (value > actualMax) {
                                    actualMax = value;
                                }
                                //Logcat.d(valueType);
                                break;
                            }
                        }
                    }

                    yVals.add(value);

                    actualDate = actualDate.plusDays(1);
                    //Logcat.d("");
                }
            }
        };


        thread.start();
        try {
            thread.join();
            return yVals;
        } catch (InterruptedException e) {
            return null;
        }
    }

    public ArrayList<String> getAttributeTitles() {
        ArrayList<String> list = new ArrayList<>();
        for (Attribute attribute : AttributeQuery.getAll(getRealm())) {
            list.add(attribute.getTitle());
            if (attribute.getId() == attrId) {
                attributeSelected = attribute;
                attributeSelectedPos = list.size() - 1;
            }
        }
        list.add(getString(R.string.all));

        return list;
    }

    public ArrayList<String> getSkillTitles() {
        ArrayList<String> list = new ArrayList<>();
        if (!allSkills) {
            for (Skill skill : attributeSelected.getSkills()) {
                list.add(skill.getTitle());
                if (skill.getId() == skillId) {
                    skillSelectedPos = list.size() - 1;
                }
            }
        }
        list.add(getString(R.string.all));

        return list;
    }

    public ArrayList<String> getExerciseTitles() {
        ArrayList<String> list = new ArrayList<>();
        if (!allExercises) {
            for (Exercise exercise : attributeSelected.getSkills().get(skillSelectedPos).getExercises()) {
                list.add(exercise.getTitle());
            }
        }
        list.add(getString(R.string.all));

        return list;
    }

    public ArrayList<String> getIntervals () {
        ArrayList<String> list = new ArrayList<>();

        list.add("Last week");
        list.add("Last month");
        list.add("Last year");
        list.add("Custom");

        return list;

    }

    public void onAttributeSelected(int position) {
        attributeSelectedPos = position;
        skillSelectedPos = 0;
        exerciseSelectedPos = 0;

        List<Attribute> attributes= AttributeQuery.getAll(getRealm());

        if (position == attributes.size()) {
            Logcat.d("All");
            allAttributes = true;
            allSkills = true;
            allExercises = true;
        } else {
            Logcat.d(position);
            attributeSelected = attributes.get(position);
            allAttributes = false;
            allSkills = false;
            allExercises = false;
        }
    }

    public void onSkillSelected(int position) {
        skillSelectedPos = position;
        exerciseSelectedPos = 0;

        if (allAttributes || position == attributeSelected.getSkills().size()) {
            Logcat.d("All");
            allSkills = true;
            allExercises = true;
        } else {
            allSkills = false;
            allExercises = false;
        }
    }

    public void onExerciseSelected(int position) {
        exerciseSelectedPos = position;

        if (allSkills || position == attributeSelected.getSkills().get(skillSelectedPos).getExercises().size()) {
            Logcat.d("All");
            allExercises = true;
        } else {
            allExercises = false;
        }
    }

    public void onIntervalSelected(int position) {
        switch (position) {
            case 0:
                startDate = DateTime.now().withTimeAtStartOfDay().minusDays(7);
                endDate = DateTime.now().withTimeAtStartOfDay();
                break;
            case 1:
                startDate = DateTime.now().withTimeAtStartOfDay().minusDays(30);
                endDate = DateTime.now().withTimeAtStartOfDay();
                break;
            case 2:
                startDate = DateTime.now().withTimeAtStartOfDay().minusDays(365);
                endDate = DateTime.now().withTimeAtStartOfDay();
                break;
        }
    }

    public int getAttributeSelectedPos() {
        return attributeSelectedPos;
    }

    public int getSkillSelectedPos() {
        return skillSelectedPos;
    }

    public int getExerciseSelectedPos() {
        return exerciseSelectedPos;
    }
}
