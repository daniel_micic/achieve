package com.judge.achieve.ui.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Daniel on 04.10.2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().onStop();
    }

    public abstract <T extends BasePresenter> T getPresenter();
}
