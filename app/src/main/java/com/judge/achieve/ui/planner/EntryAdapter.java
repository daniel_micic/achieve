package com.judge.achieve.ui.planner;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ItemEntryBinding;
import com.judge.achieve.databinding.ItemHeaderBinding;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.view.EntryDot;

import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Daniel on 25.05.2016.
 */
public class EntryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static int VIEW_HEADER = 0;
    private static int VIEW_ENTRY = 1;

    private PlannerActivity contex;
    private PlannerEntryProvider entryProvider;

    public interface PlannerEntryProvider {
        PlannerEntry getEntryForPosition(int position);
        HashMap<Integer, Boolean> getAttributeColorMap(int position);
        Pair<Integer, Pair<Integer, List<Integer>>> getEntryGroupForList(int entryPosition, int groupPosition);
        String getExerciseName(int id, int skillId);
        String getSkillName(int id);
        String getAttributeName(int id);
        int getAttributeColor(int id);
        String getEntryTime(int position);
        String getSelectedDateFormat();

        int getEntryCount();
        int getGroupForEntryCount(int position);

        void deleteEntry(long id);
        void editEntry(long id);
    }

    public EntryAdapter(PlannerActivity activity, PlannerEntryProvider provider) {
        this.contex = activity;
        this.entryProvider = provider;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_HEADER) {
            ItemHeaderBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_header, parent, false);
            HeaderViewHolder vh = new HeaderViewHolder(binding);
            return vh;
        } else {
            ItemEntryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_entry, parent, false);
            EntryViewHolder vh = new EntryViewHolder(binding);
            return vh;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        /*if (position == 0) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.binding.date.setText(entryProvider.getSelectedDateFormat());
        } else {*/
        EntryViewHolder entryViewHolder = (EntryViewHolder) holder;
        entryViewHolder.binding.dotLayout.addView(new EntryDot(contex, entryProvider.getAttributeColorMap(position)));
        entryViewHolder.binding.entryTitle.setVisibility(TextUtils.isEmpty(entryProvider.getEntryForPosition(position).getTitle()) ? GONE : View.VISIBLE);
        entryViewHolder.binding.entryTitle.setText(entryProvider.getEntryForPosition(position).getTitle());
        entryViewHolder.binding.time.setText(entryProvider.getEntryTime(position));
        EntryGroupAdapter adapter = new EntryGroupAdapter(contex, entryProvider, position);
        entryViewHolder.binding.entryGroupList.setLayoutManager(new LinearLayoutManager(contex));
        entryViewHolder.binding.entryGroupList.setAdapter(adapter);
        entryViewHolder.binding.entryGroupList.setNestedScrollingEnabled(false);

        entryViewHolder.binding.entryMore.setOnClickListener(view -> {
            PopupMenu menu = new PopupMenu(contex, entryViewHolder.binding.entryMore, Gravity.RIGHT);
            menu.inflate(R.menu.edit_delete_menu);
            menu.setOnMenuItemClickListener(item -> {
                Logcat.d("shit happend");
                switch (item.getItemId()) {
                    case R.id.delete:
                        entryProvider.deleteEntry(entryProvider.getEntryForPosition(position).getId());
                        notifyDataSetChanged();
                        break;
                    case R.id.edit:
                        entryProvider.editEntry(entryProvider.getEntryForPosition(position).getId());
                        break;
                }
                return false;
            });
            menu.show();
        });

    }

    @Override
    public int getItemCount() {
        return entryProvider.getEntryCount();
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_ENTRY;
        //return position == 0 ? VIEW_HEADER : VIEW_ENTRY;
    }

    public static class EntryViewHolder extends RecyclerView.ViewHolder {

        ItemEntryBinding binding;

        public EntryViewHolder(ItemEntryBinding binding){
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        ItemHeaderBinding binding;

        public HeaderViewHolder(ItemHeaderBinding binding){
            super(binding.getRoot());
            this.binding = binding;
        }
    }



}
