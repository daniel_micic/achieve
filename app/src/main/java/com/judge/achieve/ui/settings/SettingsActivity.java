package com.judge.achieve.ui.settings;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.IBinder;

import com.android.vending.billing.IInAppBillingService;
import com.judge.achieve.R;
import com.judge.achieve.databinding.ActivityShopBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.Logcat;

public class SettingsActivity extends BaseActivity {

    ActivityShopBinding binding;

    SettingsPresenter mPresenter;

    ServiceConnection connection;
    IInAppBillingService inAppBillingService;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);

        connection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                inAppBillingService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                inAppBillingService = IInAppBillingService.Stub.asInterface(service);
            }
        };

        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);

        mPresenter = new SettingsPresenter(this);

        initViews();

        setViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (inAppBillingService != null) {
            unbindService(connection);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public SettingsPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {

    }

    public void setViews() {

    }
}
