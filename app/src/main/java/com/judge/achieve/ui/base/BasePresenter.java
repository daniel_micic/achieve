package com.judge.achieve.ui.base;

import com.judge.achieve.AppController;

import io.realm.Realm;

/**
 * Created by Daniel on 04.10.2016.
 */

public class BasePresenter {

    private Realm mRealm;

    public BasePresenter() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onResume() {
        if (mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
    }

    public void onPause() {
    }

    public void onStop() {
        if (mRealm != null) {
            mRealm.close();
        }
    }

    public Realm getRealm() {
        return mRealm;
    }

    public String getString(int id) {
        return AppController.getContext().getResources().getString(id);
    }
}
