package com.judge.achieve.ui.planner;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.ActivityPlannerBinding;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.entryform.EntryFormActivity;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.utils.MyAnimationListener;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.recruit_mp.android.lightcalendarview.LightCalendarView;
import jp.co.recruit_mp.android.lightcalendarview.MonthView;

public class PlannerActivity extends CircularOverlayActivity {

    PlannerPresenter presenter;

    ActivityPlannerBinding binding;

    CalendarFragment calendarFragment;

    SimpleDateFormat titleFormat = new SimpleDateFormat("MMMM yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_planner);

        presenter = new PlannerPresenter(this);

        initViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public PlannerPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void onResume() {
        super.onResume();

        setViews();

        listener.onDateSelected(getPresenter().getSelectedDay().toDate());
    }

    private void initViews() {

        setSupportActionBar(binding.plannerToolbar);
        setTitle(R.string.planner);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        binding.entryAdd.setOnClickListener(view -> {
            MyAnimationListener listener = new MyAnimationListener() {
                @Override
                public void onMyAnimationFinished() {
                    Intent intent = new Intent(PlannerActivity.this, EntryFormActivity.class);
                    intent.putExtra(C.EXTRA_MODE, C.MODE_CREATE);
                    intent.putExtra(C.EXTRA_SELECTED_DAY, presenter.getSelectedDay().getMillis());
                    startActivityForResult(intent, C.MODE_CREATE);
                    overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
                }
            };

            final int x = (int) (binding.entryAdd.getX() + (binding.entryAdd.getWidth() / 2));
            final int y = (int) (binding.entryAdd.getY() + (binding.entryAdd.getHeight() / 2));

            circularRevealActivity(x, y, listener);
        });

        binding.plannerToolbar.setNavigationOnClickListener(view -> PlannerActivity.this.onBackPressed());

        binding.plannerCollapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.transparent));

        calendarFragment = CalendarFragment.newInstance(presenter.getSelectedDay().getMillis());

        calendarFragment.setListener(listener);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.calendar_container, calendarFragment);
        transaction.commit();

        binding.left.setOnClickListener(view -> setCalendarMonth(false));
        binding.right.setOnClickListener(view -> setCalendarMonth(true));

        binding.entryList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public View getOverlay() {
        return binding.overlay;
    }

    @Override
    public void setOverlayGone() {
        binding.overlay.setVisibility(View.GONE);
    }

    public void setCalendarMonth(boolean nextMonth) {
        /*DateTime month = new DateTime(binding.calendar.getMonthFrom());
        month = month.plusMonths(nextMonth ? 1 : -1);

        Logcat.d(month.toString());

        binding.calendar.setMonthFrom(new Date(month.getMillis()));
        binding.calendar.setMonthTo(new Date(month.getMillis()));
        binding.calendar.setMonthCurrent(new Date(month.getMillis()));*/
        presenter.setCurrentMonth(presenter.getCurrentMonth().plusMonths(nextMonth ? 1 : -1));

        calendarFragment = CalendarFragment.newInstance(presenter.getCurrentMonth().getMillis());
        calendarFragment.setListener(listener);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.calendar_container, calendarFragment);
        transaction.commit();

        /*if (presenter.getCurrentMonth().withTimeAtStartOfDay().getMillis() == DateTime.now().withTimeAtStartOfDay().getMillis()) {
            listener.onDateSelected(new Date());
        }*/
    }

    public void setViews() {
        EntryAdapter adapter = new EntryAdapter(this, getPresenter());
        binding.entryList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        binding.date.setText(getPresenter().getSelectedDateFormat());
    }


    private LightCalendarView.OnStateUpdatedListener listener = new LightCalendarView.OnStateUpdatedListener() {
        @Override
        public void onMonthSelected(@NotNull Date date, @NotNull MonthView monthView) {
            //onthView.setAccents();
            Logcat.d(date.toString());
            PlannerActivity.this.setTitle(titleFormat.format(date));
        }

        @Override
        public void onDateSelected(@NotNull Date date) {
            Logcat.d(date.toString());
            DateTime now = DateTime.now();
            getPresenter().setEntriesForDay(new DateTime(date).withTime(now.getHourOfDay(), now.getMinuteOfHour(), 0, 0));
            setViews();
        }
    };

    public void onEntryEdit(long id) {
        MyAnimationListener listener = new MyAnimationListener() {
            @Override
            public void onMyAnimationFinished() {
                Intent intent = new Intent(PlannerActivity.this, EntryFormActivity.class);
                intent.putExtra(C.EXTRA_MODE, C.MODE_EDIT);
                intent.putExtra(C.EXTRA_ENTRY_ID, id);
                startActivityForResult(intent, C.MODE_EDIT);
                overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
            }
        };

        circularRevealActivity(listener);
    }
}
