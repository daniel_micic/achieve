package com.judge.achieve.ui.attributeform;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.ExerciseQuery;
import com.judge.achieve.persistence.query.SkillQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.DialogUtils;
import com.judge.achieve.utils.Logcat;
import com.judge.achieve.ui.exerciseform.ExerciseFormActivity;

import java.util.ArrayList;

/**
 * Created by Daniel on 01.10.2016.
 */

public class AttributeFormPresenter extends BasePresenter {

    private AttributeFormActivity mView;
    private Context mContext;

    private Attribute mAttribute;
    private int mMode;

    private Difficulty mInitialDiffiulty;

    public AttributeFormPresenter(AttributeFormActivity view, int mode, int id) {
        super();

        mView = view;
        mContext = view;
        mMode = mode;

        switch (mMode) {
            default:
            case C.MODE_CREATE:
                mAttribute = new Attribute();
                break;
            case C.MODE_EDIT:
                mAttribute = AttributeQuery.getById(getRealm(), id);
                mInitialDiffiulty = mAttribute.getDifficulty();
                break;
        }
    }

    public AttributeFormPresenter(AttributeFormActivity view, int mode, Bundle savedInstanceState) {
        super();
        Logcat.d("recreating from bundle");
        mView = view;
        mContext = view;
        mMode = mode;

        mAttribute = savedInstanceState.getParcelable(C.SAVED_INSTANCE_CATEGORY);
        mInitialDiffiulty = Difficulty.fromValue(savedInstanceState.getInt(C.SAVED_INSTANCE_DIFFICULTY, Difficulty.NORMAL.getValue()));
    }

    public void onSaveInstace(Bundle outState) {
        Logcat.d("on save instance");

        setAndCheckDataFromView(false);

        outState.putParcelable(C.SAVED_INSTANCE_CATEGORY, mAttribute);
        if (mMode == C.MODE_EDIT)
            outState.putInt(C.SAVED_INSTANCE_DIFFICULTY, mInitialDiffiulty.getValue());
    }

    public void setViews() {
        if (mAttribute == null) {
            return;
        }

        mView.setCategoryTitle(mAttribute.getTitle());
        mView.setCategoryDifficulty(mAttribute.getDifficulty());
    }

    //returns wehter the view had all neccesary data
    public boolean setAndCheckDataFromView(boolean showError) {
        String title = mView.getCategoryTitle();

        if (!TextUtils.isEmpty(title)) {
            mAttribute.setTitle(title);
        } else if (showError){
            mView.setCategoryTitleError();
            return false;
        }

        Difficulty difficulty = mView.getCategoryDifficulty();

        if (difficulty != Difficulty.UNDEFINED) {
            mAttribute.setDifficulty(difficulty);
        } else if (showError){
            mView.setCategoryDifficultyError();
            return false;
        }
        return true;
    }

    public void onNextClicked() {

        if (!setAndCheckDataFromView(true)) {
            return;
        }

        if (mMode == C.MODE_CREATE) {
            createCategory();
        } else {
            editCategory();
        }
    }

    public void createCategory() {
        mAttribute.setSkills(new ArrayList<Skill>());

        Skill general = new Skill();
        general.setId(SkillQuery.getNewPrimaryKey(getRealm()));
        general.setTitle("General");
        general.setExercises(new ArrayList<Exercise>());
        mAttribute.getSkills().add(general);
        int categoryId = AttributeQuery.copyToRealm(getRealm(), mAttribute);

        Intent intent = new Intent(mView, ExerciseFormActivity.class);
        intent.putExtra(C.EXTRA_MODE, C.MODE_CREATE);
        intent.putExtra(C.EXTRA_ATTRIBUTE_ID, categoryId);
        mView.startActivity(intent);
        mView.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        mView.finish();
    }

    public void editCategory() {
        AttributeQuery.edit(getRealm(), mAttribute);
        if (mInitialDiffiulty != mAttribute.getDifficulty()) {
            final AlertDialog dialog = DialogUtils.getYesNoDialog(mContext,
                    mContext.getString(R.string.msg_all_exercise_update_diff),
                    mContext.getResources().getColor(R.color.colorPrimaryRed),
                    new DialogUtils.DialogListener() {
                        @Override
                        public void onPositiveClicked() {
                            for (Skill sc : mAttribute.getSkills()) {
                                for (Exercise e : sc.getExercises()) {
                                    e.setDifficulty(mAttribute.getDifficulty());
                                    ExerciseQuery.edit(getRealm(), e);
                                }
                            }
                            mView.finish();
                        }

                        @Override
                        public void onNegativeClicked() {
                            mView.finish();

                        }
                    });
            dialog.show();
        } else {
            mView.finish();
        }
    }


    public String getAttributeTitle() {
        if (mAttribute != null && mAttribute.getTitle() != null) {
            return mAttribute.getTitle();
        } else
            return "";

    }

    public Difficulty getAttributeDifficulty() {
        if (mAttribute != null && mAttribute.getDifficulty() != null) {
            return mAttribute.getDifficulty();
        } else
            return Difficulty.UNDEFINED;

    }



}
