package com.judge.achieve.ui.entryform;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.R;

/**
 * Created by Daniel on 27.02.2017.
 */

public class EntryFormPagerAdapter extends PagerAdapter {

    EntryFormActivity activity;

    public EntryFormPagerAdapter(EntryFormActivity activity) {
        this.activity = activity;
    }

    public Object instantiateItem(ViewGroup collection, int position) {

        int resId = 0;
        switch (position) {
            case 0:
                resId = R.id.page_one;
                break;
            case 1:
                resId = R.id.page_two;
                break;

        }
        return activity.findViewById(resId);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
    }
}