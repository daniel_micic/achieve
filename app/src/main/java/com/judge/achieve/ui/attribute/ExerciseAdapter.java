package com.judge.achieve.ui.attribute;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.judge.achieve.R;
import com.judge.achieve.databinding.ItemExerciseBinding;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.utils.AnimationUtils;
import com.judge.achieve.utils.Utils;

import java.util.ArrayList;

import static android.view.View.GONE;

/**
 * Created by Daniel on 25.05.2016.
 */
public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ExerciseViewHolder> {


    private Integer subcategoryId;
    private ArrayList<Exercise> data;
    private Integer subcategoryPosition;
    private HistoryStringUpdater updater;
    private ExerciseDescriptionHandler handler;
    private ExerciseListener listener;

    private boolean isActionMode = false;

    public interface ExerciseListener {
        void onExerciseDoneClick(Exercise e,
                                 int subcategoryPosition);
        void onExerciseEditClick(Exercise e,
                                 Integer subcategoryId);
        void onExerciseDeleteClick(Exercise e,
                                   int exercisePosition,
                                   int subcategoryPosition);
    }

    public interface ExerciseDescriptionHandler {
        boolean isExerciseExpanded(int id);
        boolean isExerciseAnimating(int id);
        void setExerciseExpanded(int id, boolean expanded);
        void setExerciseAnimating(int id, boolean animating);
    }

    public interface HistoryStringUpdater {
        void addListener(HistoryStringListener listener, int exerciseId);
    }

    public interface HistoryStringListener {
        void onChange(String text);
    }


    public ExerciseAdapter(Integer subcategoryId,
                           ArrayList<Exercise> data,
                           Integer subcategoryPosition,
                           HistoryStringUpdater updater,
                           ExerciseDescriptionHandler handler,
                           ExerciseListener listener) {
        this.subcategoryId = subcategoryId;
        this.data = data;
        this.subcategoryPosition = subcategoryPosition;
        this.updater = updater;
        this.handler = handler;
        this.listener = listener;
    }

    @Override
    public ExerciseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemExerciseBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_exercise,parent,false);
        ExerciseViewHolder vh = new ExerciseViewHolder(binding);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ExerciseViewHolder holder, final int position) {
        final Exercise e = data.get(position);

        // overlay is shown to indicate whether exercise was done that day
        boolean isOverlayVisible = e.getCountForDay() > 0;

        holder.binding.exerciseTitle.setText(e.getTitle());
        holder.binding.exerciseDescription.setVisibility(TextUtils.isEmpty(e.getDescription()) ? GONE : View.VISIBLE);
        holder.binding.exerciseDescription.setText(e.getDescription());
        holder.binding.exerciseCountForDay.setVisibility(View.VISIBLE);
        holder.binding.exerciseCountForDay.setText(String.valueOf(e.getCountForDay()) + " TODAY");

        HistoryStringListener historyStringListener = text -> {
            if (holder.binding != null) {
                holder.binding.exerciseCountForDay.animate().alpha(0f).setDuration(150).withEndAction(() -> {
                    holder.binding.exerciseCountForDay.setText(text);
                    holder.binding.exerciseCountForDay.animate().alpha(1f).setDuration(150);
                });
            }
        };

        updater.addListener(historyStringListener, e.getId());

        holder.binding.exerciseDescription.setVisibility(TextUtils.isEmpty(e.getDescription()) ? View.GONE : View.VISIBLE);

        /*holder.binding.exerciseExpand.setOnClickListener(view -> {
            if (handler.isExerciseAnimating(e.getId())) {
                return;
            } else {
                handler.setExerciseExpanded(e.getId(), !handler.isExerciseExpanded(e.getId()));
                notifyItemChanged(position);
            }

        });*/

        if (isOverlayVisible)
            holder.binding.exerciseOverlay.setVisibility(View.VISIBLE);

        if (isActionMode) {
            holder.binding.exerciseEditMenu.setVisibility(View.VISIBLE);
            holder.binding.exerciseCountForDay.setVisibility(View.GONE);
            //holder.binding.exerciseExpand.setVisibility(View.GONE);
        } else {
            holder.binding.exerciseEditMenu.setVisibility(View.GONE);
            holder.binding.exerciseCountForDay.setVisibility(View.VISIBLE);
            //holder.binding.exerciseExpand.setVisibility(View.VISIBLE);
        }

        holder.binding.exerciseOverlay.getBackground().setColorFilter(Utils.getCategoryColor(subcategoryPosition), PorterDuff.Mode.MULTIPLY);

        holder.binding.exerciseCheckboxBlack.setOnCheckedChangeListener((compoundButton, b) -> {
                    holder.binding.exerciseCountForDay.setVisibility(View.VISIBLE);

                    int x = holder.binding.exerciseCheckboxWhite.getLeft() + ((holder.binding.exerciseCheckboxWhite.getWidth()) / 2);
                    int y = holder.binding.exerciseCheckboxWhite.getTop() + (holder.binding.exerciseCheckboxWhite.getHeight() / 2);
                    int radius = x + 5; // I'm really not in mood to calculate distance from center of chcekbox to the corner of layout ...
                    holder.binding.exerciseOverlay.setVisibility(View.VISIBLE);
                    AnimationUtils.makeCircularReveal(holder.binding.exerciseOverlay, x, y, radius, null);

                    holder.binding.exerciseCheckboxWhite.performClick();
                }
        );
        holder.binding.exerciseCheckboxWhite.setOnCheckedChangeListener((compoundButton, b) -> {
                    if (b) {
                        listener.onExerciseDoneClick(e, subcategoryPosition);
                        // out of MVP concept but going to presenter only to set this back through two adapters seemed a bit fucking useless
                        // especialy when presenter sets e.countForDay nicely before this is called ...
                        //TODO don;t concatenate strings, use resource with placeholders
                        holder.binding.exerciseCountForDay.setText(String.valueOf(e.getCountForDay()) + " TODAY");

                        new Handler().postDelayed(() -> compoundButton.performClick(), 500);
                    }
                }
        );

        holder.binding.exerciseEdit.setOnClickListener(view -> {
            listener.onExerciseEditClick(e, subcategoryId);
        });

        holder.binding.exerciseDelete.setOnClickListener(view -> {
            listener.onExerciseDeleteClick(e, position, subcategoryPosition);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ExerciseViewHolder extends RecyclerView.ViewHolder {

        ItemExerciseBinding binding;
        public ExerciseViewHolder(ItemExerciseBinding binding){
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public boolean isActionMode() {
        return isActionMode;
    }

    public void setActionMode(boolean actionMode) {
        isActionMode = actionMode;
    }

    public void setData(ArrayList <Exercise> data) {
        this.data = data;
    }
}
