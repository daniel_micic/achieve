package com.judge.achieve.ui.tutorial.viewpager;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;

/**
 * Created by Daniel on 24.01.2017.
 */

public class TutorialAdapter extends FragmentStatePagerAdapter {


    private ArrayList<Fragment> fragments = new ArrayList<>();

    public TutorialAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return TutorialFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
