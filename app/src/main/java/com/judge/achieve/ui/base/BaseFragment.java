package com.judge.achieve.ui.base;

import android.support.v4.app.Fragment;

/**
 * Created by Daniel on 04.10.2016.
 */

public abstract class BaseFragment extends Fragment {

    public BasePresenter mPresenter;

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPresenter().onStop();
    }

    public abstract <T extends BasePresenter> T getPresenter();
}
