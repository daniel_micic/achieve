package com.judge.achieve.ui.exerciseform;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.common.enums.Scope;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.query.AttributeQuery;
import com.judge.achieve.persistence.query.ExerciseQuery;
import com.judge.achieve.persistence.query.SkillQuery;
import com.judge.achieve.ui.base.BasePresenter;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;

/**
 * Created by Daniel on 01.10.2016.
 */

public class ExerciseFormPresenter extends BasePresenter {

    private ExerciseFormActivity mView;

    private int mAttributeId;
    private int mSkillId;
    private int mExerciseId;

    private int mInitialSkillId;

    private Attribute mAttribute;
    private Exercise mExercise;
    private int mMode;

    private boolean isCreateNewSkill = false;

    public ExerciseFormPresenter(ExerciseFormActivity view,
                                 int mode,
                                 Intent intent) {
        super();

        mAttributeId = intent.getIntExtra(C.EXTRA_ATTRIBUTE_ID, -1);

        // TODO check if null ??
        mAttribute = AttributeQuery.getById(getRealm(), mAttributeId);

        mView = view;
        mMode = mode;

        switch (mMode) {
            default:
            case C.MODE_NEW_CATEGORY:
            case C.MODE_CREATE:
                if (mAttribute.getSkills().size() > 0) {
                    mSkillId = mAttribute.getSkills().get(0).getId();
                } else {
                    isCreateNewSkill = true;
                    mSkillId = -1;
                }
                mInitialSkillId = mSkillId;
                mExercise = new Exercise();
                break;
            case C.MODE_EDIT:
                mSkillId = intent.getIntExtra(C.EXTRA_SKILL_ID, -1);
                mInitialSkillId = mSkillId;
                mExerciseId = intent.getIntExtra(C.EXTRA_EXERCISE_ID, -1);
                mExercise = ExerciseQuery.getById(getRealm(), mExerciseId);
                break;
        }

        setViews(false, null);
    }

    public ExerciseFormPresenter(ExerciseFormActivity view, int mode, Bundle savedInstanceState) {
        super();
        Logcat.d("recreating from saved instance");
        mView = view;
        mMode = mode;

        isCreateNewSkill = savedInstanceState.getBoolean(C.SAVED_INSTANCE_IS_NEW_SUBCATEGORY);

        mAttributeId = savedInstanceState.getInt(C.SAVED_INSTANCE_CATEGORY_ID);
        mAttribute = AttributeQuery.getById(getRealm(), mAttributeId);

        mExercise = savedInstanceState.getParcelable(C.SAVED_INSTANCE_EXERCISE);
        mExerciseId = mExercise.getId();

        mInitialSkillId = savedInstanceState.getInt(C.SAVED_INSTANCE_SUBCATEGORY_INITIAL_ID);
        String mSkillTitle = "";
        if (isCreateNewSkill) {
            mSkillTitle = savedInstanceState.getString(C.SAVED_INSTANCE_SUBCATEGORY_TITLE);
        } else {
            mSkillId = savedInstanceState.getInt(C.SAVED_INSTANCE_SUBCATEGORY_ID);
        }

        setViews(true, mSkillTitle);
    }

    public void onSaveInstace(Bundle outState) {
        Logcat.d("on save instance");
        setAndCheckDataFromView(false);

        outState.putInt(C.SAVED_INSTANCE_MODE, mMode);
        outState.putInt(C.SAVED_INSTANCE_CATEGORY_ID, mAttributeId);
        outState.putInt(C.SAVED_INSTANCE_SUBCATEGORY_INITIAL_ID, mInitialSkillId);
        outState.putBoolean(C.SAVED_INSTANCE_IS_NEW_SUBCATEGORY, isCreateNewSkill);
        if (isCreateNewSkill) {
            outState.putString(C.SAVED_INSTANCE_SUBCATEGORY_TITLE, mView.getSkillTitle());
        } else {
            outState.putInt(C.SAVED_INSTANCE_SUBCATEGORY_ID, mSkillId);
        }
        outState.putParcelable(C.SAVED_INSTANCE_EXERCISE, mExercise);
    }

    public void setViews(boolean isRestore, String subcategoryTitle) {

        if (mMode == C.MODE_EDIT || isRestore) {
            mView.setExerciseTitle(mExercise.getTitle());
            mView.setExerciseDifficulty(mExercise.getDifficulty());
            mView.setExerciseScope(mExercise.getScope());
            mView.setExerciseDescription(mExercise.getDescription());
        } else {
            mView.setExerciseDifficulty(mAttribute.getDifficulty());
        }

        ArrayList<String> list = new ArrayList<>();
        for (Skill sc : mAttribute.getSkills()) {
            list.add(sc.getTitle());
        }
        list.add(getString(R.string.create_new));

        mView.setSubcategorySpinnerData(list);//, subcategoryTitle);

        if (isCreateNewSkill) {
            mView.setSubcategoryTitle(subcategoryTitle);
        } else {
            for (Skill sc : mAttribute.getSkills()) {
                if (sc.getId() == mSkillId) {
                    mView.setSubcategory(mAttribute.getSkills().indexOf(sc));
                }
            }
        }
    }

    public boolean setAndCheckDataFromView(boolean showError) {
        String title = mView.getExerciseTitle();

        if (!TextUtils.isEmpty(title)) {
            mExercise.setTitle(title);
        } else if (showError) {
            mView.setExerciseTitleError();
            return false;
        }

        if (isCreateNewSkill &&
            TextUtils.isEmpty(mView.getSkillTitle()) &&
            showError) {
                mView.setSubcategoryTitleError();
                return false;
        }

        Difficulty difficulty = mView.getExerciseDifficulty();

        if (difficulty != Difficulty.UNDEFINED) {
            mExercise.setDifficulty(difficulty);
        } else if (showError) {
            mView.setExerciseDifficultyError();
            return false;
        }

        Scope scope = mView.getExerciseScope();
        Logcat.d(scope.getValue());

        if (scope != Scope.UNDEFINED) {
            mExercise.setScope(scope);
        } else if (showError) {
            mView.setExerciseScopeError();
            return false;
        }

        String description = mView.getExerciseDescription();
        if (!TextUtils.isEmpty(description)) {
            mExercise.setDescription(description);
        }

        return true;
    }

    public void onSubcategorySelected(int position) {
        mSkillId = mAttribute.getSkills().get(position).getId();
    }

    public void onCreateNewSubcategory() {
        isCreateNewSkill = true;
    }

    public void onAnotherClicked() {
        if (!setAndCheckDataFromView(true)) {
            return;
        }

        createExercise();

        mView.resetViews();
        mView.setExerciseDifficulty(mAttribute.getDifficulty());
    }

    public void onDoneClicked() {
        if (!setAndCheckDataFromView(true)) {
            return;
        }

        Logcat.d("done clicked");
        if (mMode == C.MODE_CREATE) {
            createExercise();
            mView.setResult(C.RESULT_CREATED);
        } else {
            editExercise();
            mView.setResult(C.RESULT_EDITED);
        }

        mView.setResult(C.RESULT_OK);
        mView.finish();
        mView.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void createExercise() {
        if (!setAndCheckDataFromView(true)) {
            return;
        }

        if (isCreateNewSkill) {
            mSkillId = createSubcategory();
        }

        mExercise.setStatistics(new ArrayList<>());

        ExerciseQuery.copyToRealm(getRealm(), mExercise, mSkillId);
    }

    public void editExercise() {
        Logcat.d(mInitialSkillId);
        Logcat.d("edit desc" + mExercise.getDescription());

        if (isCreateNewSkill) {
            Logcat.d("is create new");
            mSkillId = createSubcategory();
            ExerciseQuery.edit(getRealm(), mExercise, mInitialSkillId, mSkillId);
        } else if (mInitialSkillId != mSkillId) {
            Logcat.d("moving to other sub");
            Logcat.d("initial " + mInitialSkillId);
            ExerciseQuery.edit(getRealm(), mExercise, mInitialSkillId, mSkillId);
        } else {
            Logcat.d("just editing values");
            ExerciseQuery.edit(getRealm(), mExercise);
        }
    }

    public int createSubcategory() {
        Skill skill = new Skill();

        skill.setTitle(mView.getSkillTitle());
        skill.setPoints(0);
        skill.setExercises(new ArrayList<Exercise>());

        return SkillQuery.copyToRealm(getRealm(), skill, mAttributeId);
    }


}
