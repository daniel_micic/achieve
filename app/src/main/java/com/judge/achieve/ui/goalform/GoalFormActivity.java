package com.judge.achieve.ui.goalform;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.databinding.ActivityGoalFormBinding;
import com.judge.achieve.ui.base.BaseActivity;
import com.judge.achieve.utils.Logcat;

public class GoalFormActivity extends BaseActivity {

    ActivityGoalFormBinding binding;

    GoalFormPresenter mPresenter;

    int mMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logcat.d("on create");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_goal_form);

        if (savedInstanceState == null) {
            Logcat.d("save instance was null");
            mMode = getIntent().getIntExtra(C.EXTRA_MODE, C.MODE_CREATE);
            int id = getIntent().getIntExtra(C.EXTRA_GOAL_ID, -1);
            mPresenter = new GoalFormPresenter(this, mMode, id);
        } else {
            Logcat.d("save instance was not null");
            mMode =  savedInstanceState.getInt(C.SAVED_INSTANCE_MODE, C.MODE_CREATE);
            mPresenter = new GoalFormPresenter(this, mMode, savedInstanceState);
        }

        initViews();
        setViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logcat.d("on save instance");
        outState.putInt(C.SAVED_INSTANCE_MODE, mMode);
        mPresenter.onSaveInstace(outState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public GoalFormPresenter getPresenter() {
        return mPresenter;
    }

    private void initViews() {
        if (mMode == C.MODE_CREATE) {
            setTitle(getString(R.string.create_goal));
        } else if (mMode == C.MODE_EDIT) {
            setTitle(getString(R.string.edit_goal));
        }

        setSupportActionBar(binding.createGoalToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        binding.createGoalToolbar.setNavigationOnClickListener(view -> GoalFormActivity.this.onBackPressed());
        
        binding.createGoalEasyCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (binding.createGoalNormalCheckbox.isChecked()) {
                    binding.createGoalNormalCheckbox.performClick();
                }
                if (binding.createGoalHardCheckbox.isChecked()) {
                    binding.createGoalHardCheckbox.performClick();
                }
            }
        });

        binding.createGoalNormalCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (binding.createGoalEasyCheckbox.isChecked()) {
                    binding.createGoalEasyCheckbox.performClick();
                }
                if (binding.createGoalHardCheckbox.isChecked()) {
                    binding.createGoalHardCheckbox.performClick();
                }
            }
        });

        binding.createGoalHardCheckbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (binding.createGoalEasyCheckbox.isChecked()) {
                    binding.createGoalEasyCheckbox.performClick();
                }
                if (binding.createGoalNormalCheckbox.isChecked()) {
                    binding.createGoalNormalCheckbox.performClick();
                }
            }
        });

        binding.createGoalAttrSkillSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getPresenter().onSubcategorySelected(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.createGoalAttrSkillSpinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);


        binding.createGoalDoneBtn.setOnClickListener(view -> mPresenter.onDoneClicked());
    }

    public void setViews() {
        setGoalTitle(getPresenter().getGoalTitle());
        setGoalDescription(getPresenter().getGoalDescription());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.layout_spinner_subcategory, getPresenter().getSpinerData());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.createGoalAttrSkillSpinner.setAdapter(adapter);
    }

    public String getGoalTitle() {
        return binding.createGoalTitleEt.getText().toString();
    }

    public void setGoalTitle(String title) {
        binding.createGoalTitleEt.setText(title);
    }

    public void setGoalTitleError() {
        Snackbar.make(binding.getRoot(), "Please enter Title", Snackbar.LENGTH_SHORT).show();
    };

    public String getGoalDescription() {
        return binding.createGoalDescriptionEt.getText().toString();
    }

    public void setGoalDescription(String description) {
        binding.createGoalDescriptionEt.setText(description);
    }

    public Difficulty getGoalDifficulty() {
        if (binding.createGoalEasyCheckbox.isChecked()) {
            return Difficulty.EASY;
        }
        if (binding.createGoalNormalCheckbox.isChecked()) {
            return Difficulty.NORMAL;
        }
        if (binding.createGoalHardCheckbox.isChecked()) {
            return Difficulty.HARD;
        }
        return Difficulty.UNDEFINED;
    }

    public void setGoalDifficulty(Difficulty difficulty) {
        switch (difficulty) {
            case EASY:
                binding.createGoalEasyCheckbox.setChecked(true);
                binding.createGoalNormalCheckbox.setChecked(false);
                binding.createGoalHardCheckbox.setChecked(false);
                break;
            case NORMAL:
                binding.createGoalEasyCheckbox.setChecked(false);
                binding.createGoalNormalCheckbox.setChecked(true);
                binding.createGoalHardCheckbox.setChecked(false);
                break;
            case HARD:
                binding.createGoalEasyCheckbox.setChecked(false);
                binding.createGoalNormalCheckbox.setChecked(false);
                binding.createGoalHardCheckbox.setChecked(true);
                break;
            case UNDEFINED:
                binding.createGoalEasyCheckbox.setChecked(false);
                binding.createGoalNormalCheckbox.setChecked(false);
                binding.createGoalHardCheckbox.setChecked(false);
                break;
        }
    }

    public void setGoalDifficultyError() {
        Snackbar.make(binding.getRoot(), "Please select Difficulty", Snackbar.LENGTH_SHORT).show();
    };
}
