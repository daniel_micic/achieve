package com.judge.achieve.ui.goal.viewpager;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.judge.achieve.R;
import com.judge.achieve.common.C;
import com.judge.achieve.databinding.FragmentAddGoalBinding;
import com.judge.achieve.ui.base.CircularOverlayActivity;
import com.judge.achieve.ui.goalform.GoalFormActivity;
import com.judge.achieve.utils.MyAnimationListener;
import com.judge.achieve.utils.ResourceUtil;

/**
 * Created by Daniel on 24.01.2017.
 */

public class AddGoalFragment extends Fragment {

    FragmentAddGoalBinding binding;
    private boolean isEntryAnimation;
    Handler handler = new Handler();

    public static AddGoalFragment newInstance(boolean entryAnimation) {
        AddGoalFragment fragment = new AddGoalFragment();

        Bundle args = new Bundle();
        args.putBoolean(C.EXTRA_GOAL_ENTRY_ANIM, entryAnimation);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int id;
        if (savedInstanceState == null) {
            id = getArguments().getInt(C.EXTRA_GOAL_ID);
            isEntryAnimation = getArguments().getBoolean(C.EXTRA_GOAL_ENTRY_ANIM);
        } else {
            id = savedInstanceState.getInt(C.SAVED_INSTANCE_GOAL_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_goal, container, false);

        binding.getRoot().setOnClickListener(view -> {
        });

        binding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            if (isEntryAnimation) {
                entryAnimation();
                isEntryAnimation = false;
            }
        });

        binding.addIc.setColorFilter(ResourceUtil.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        binding.button.setOnClickListener(view -> {
            MyAnimationListener listener = new MyAnimationListener() {
                @Override
                public void onMyAnimationFinished() {
                    //TODO remake into transition ?
                    Intent intent = new Intent(getActivity(), GoalFormActivity.class);
                    intent.putExtra(C.EXTRA_MODE, C.MODE_CREATE);
                    getActivity().startActivity(intent);
                    getActivity().overridePendingTransition(android.R.anim.fade_in, R.anim.do_not_move);
                }
            };
            handler.postDelayed(() -> ((CircularOverlayActivity)getActivity()).circularRevealActivity(listener), 200);

        });

        return binding.getRoot();
    }

    public void entryAnimation() {
        float rootLayoutPos = binding.getRoot().getY();

        binding.getRoot().setY(rootLayoutPos - 400);
        binding.getRoot().setAlpha(0f);

        binding.getRoot().animate()
                .y(rootLayoutPos)
                .alpha(1f)
                .setDuration(500)
                .setInterpolator(new DecelerateInterpolator())
                .setStartDelay(100);
    }

    public void exitAnimation() {
        if (binding != null) {
            float rootLayoutPos = binding.getRoot().getY();

            binding.getRoot().animate()
                    .y(rootLayoutPos - 400f)
                    .alpha(0f)
                    .setDuration(400)
                    .setInterpolator(new AccelerateInterpolator());
        }
    }
}
