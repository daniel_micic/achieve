package com.judge.achieve.persistence.query;

import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.persistence.model.AttributeDatabase;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Daniel on 02.08.2016.
 */
public class AttributeQuery {

    public static Attribute getById(Realm realm, int id) {
        AttributeDatabase cd = realm.where(AttributeDatabase.class).equalTo("id", id).findFirst();
        if (cd == null) {
            return null;
        } else {
            return cd.createAppObject();
        }
    }

    public static ArrayList<Attribute> getAll(Realm realm) {
        RealmResults<AttributeDatabase> results = realm.where(AttributeDatabase.class).findAll();

        ArrayList<Attribute> list = new ArrayList<>();
        for (AttributeDatabase cd : results) {
            list.add(cd.createAppObject());
        }
        return list;
    }

    public static int copyToRealm(Realm realm, Attribute attribute) {
        realm.beginTransaction();

        AttributeDatabase cd = AttributeDatabase.mapToDatabase(attribute);

        int primaryKey = getNewPrimaryKey(realm);

        cd.setId(primaryKey);

        cd = realm.copyToRealm(cd);

        /*ProfileDatabase pd = realm.where(ProfileDatabase.class).findFirst();
        pd.getCategories().add(cd);*/

        realm.commitTransaction();

        updateAttributeColors(realm);

        return primaryKey;
    }


    public static void edit(Realm realm, Attribute attribute) {
        Logcat.d("edit attribute");
        AttributeDatabase cd = realm.where(AttributeDatabase.class).equalTo("id", attribute.getId()).findFirst();

        realm.beginTransaction();
        cd.setTitle(attribute.getTitle());
        cd.setPoints(attribute.getPoints());
        cd.setDifficulty(attribute.getDifficulty().getValue());
        realm.commitTransaction();
    }

    public static void delete(Realm realm, int id) {
        Logcat.d("delete category");
        AttributeDatabase cd = realm.where(AttributeDatabase.class).equalTo("id", id).findFirst();

        SkillQuery.deleteAll(realm, cd.getSkills());

        if (cd != null) {
            realm.executeTransaction(realm1 -> cd.deleteFromRealm());
        }

        updateAttributeColors(realm);

        for (PlannerEntry e : PlannerEntryQuery.getAll(realm)) {

            Iterator<Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>>> iterator = e.getExercises().entrySet().iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().getKey() == id) {
                        Logcat.d("removing from entry " + e.getId() + " skill " + id);
                        iterator.remove();
                    }
                }

            if (e.getExercises().size() == 0) {
                PlannerEntryQuery.delete(realm, e.getId());
            } else {
                PlannerEntryQuery.edit(realm, e);
            }
        }
    }

    public static int getNewPrimaryKey(Realm realm) {
        Number maxId = realm.where(AttributeDatabase.class).max("id");
        int primaryKeyValue = maxId != null ? maxId.intValue() + 1 : 1;
        return primaryKeyValue;
    }

    public static void updateAttributeColors(Realm realm) {
        realm.beginTransaction();
        RealmResults<AttributeDatabase> results = realm.where(AttributeDatabase.class).findAll();

        for (AttributeDatabase cd : results) {
            cd.setPosition(results.indexOf(cd));
        }
        realm.commitTransaction();
    }
}
