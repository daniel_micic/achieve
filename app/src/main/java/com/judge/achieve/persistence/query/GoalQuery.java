package com.judge.achieve.persistence.query;

import com.judge.achieve.model.Goal;
import com.judge.achieve.persistence.model.GoalDatabase;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Daniel on 05.01.2017.
 */

public class GoalQuery {

    public static Goal getById(Realm realm, int id) {
        GoalDatabase gd = realm.where(GoalDatabase.class).equalTo("id", id).findFirst();
        return gd.createAppObject();
    }

    public static ArrayList<Goal> getAll(Realm realm) {
        RealmResults<GoalDatabase> results = realm.where(GoalDatabase.class).findAll();

        ArrayList<Goal> list = new ArrayList<>();
        for (GoalDatabase gd : results) {
            list.add(gd.createAppObject());
        }
        return list;
    }

    public static int copyToRealm(Realm realm, Goal goal) {
        realm.beginTransaction();

        GoalDatabase gd = GoalDatabase.mapToDatabase(goal);

        int primaryKey = getNewPrimaryKey(realm);

        gd.setId(primaryKey);

        realm.copyToRealm(gd);

        realm.commitTransaction();

        return primaryKey;
    }

    public static void copyToRealm(Realm realm, List<Goal> goals) {
        for (Goal goal : goals) {
            copyToRealm(realm, goal);
        }
    }

    public static void edit(Realm realm, Goal goal) {
        GoalDatabase gd = realm.where(GoalDatabase.class).equalTo("id", goal.getId()).findFirst();
        realm.executeTransaction(realm1 -> {
            gd.setTitle(goal.getTitle());
            gd.setDescription(goal.getDescription());
            gd.setDifficulty(goal.getDifficulty().getValue());
            gd.setFinished(goal.isFinished());
            gd.setAttributeId(goal.getAttributeId());
            gd.setSkillId(goal.getSkillId());
        });
    }

    public static int getNewPrimaryKey(Realm realm) {
        Number maxId = realm.where(GoalDatabase.class).max("id");
        int primaryKeyValue = maxId != null ? maxId.intValue() + 1 : 1;
        return primaryKeyValue;
    }

    public static void delete(Realm realm, long id) {
        Logcat.d("delete category");

        GoalDatabase ed = realm.where(GoalDatabase.class).equalTo("id", id).findFirst();

        if (ed != null) {


            realm.executeTransaction(realm1 -> ed.deleteFromRealm());
        }

    }

}
