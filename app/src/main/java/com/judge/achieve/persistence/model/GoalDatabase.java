package com.judge.achieve.persistence.model;

import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.model.Goal;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 05.01.2017.
 */

public class GoalDatabase extends RealmObject {

    @PrimaryKey
    int id;

    String title;
    String description;

    int difficulty;

    boolean finished;

    int attributeId;
    int skillId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(int attributeId) {
        this.attributeId = attributeId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public static GoalDatabase mapToDatabase(Goal g) {
        GoalDatabase gd = new GoalDatabase();
        gd.setId(g.getId());
        gd.setTitle(g.getTitle());
        gd.setDescription(g.getDescription());
        gd.setDifficulty(g.getDifficulty().getValue());
        gd.setFinished(g.isFinished());

        gd.setAttributeId(g.getAttributeId());
        gd.setSkillId(g.getSkillId());

        return gd;
    }

    public Goal createAppObject() {
        Goal g = new Goal();
        g.setId(getId());
        g.setTitle(getTitle());
        g.setDescription(getDescription());
        g.setDifficulty(Difficulty.fromValue(getDifficulty()));
        g.setFinished(isFinished());

        g.setAttributeId(getAttributeId());
        g.setSkillId(getSkillId());

        return g;
    }
}
