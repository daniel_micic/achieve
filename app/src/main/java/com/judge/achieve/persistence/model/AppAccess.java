package com.judge.achieve.persistence.model;

import io.realm.RealmObject;

/**
 * Created by aa on 13. 4. 2017.
 */

public class AppAccess extends RealmObject{

    boolean fullAccess;
    boolean attributes;
    boolean exercises;
    boolean statistics;
    boolean planner;

    public AppAccess() {
    }

    public boolean isFullAccess() {
        return fullAccess;
    }

    public void setFullAccess(boolean fullAccess) {
        this.fullAccess = fullAccess;
    }

    public boolean isAttributes() {
        return attributes;
    }

    public void setAttributes(boolean attributes) {
        this.attributes = attributes;
    }

    public boolean isExercises() {
        return exercises;
    }

    public void setExercises(boolean exercises) {
        this.exercises = exercises;
    }

    public boolean isStatistics() {
        return statistics;
    }

    public void setStatistics(boolean statistics) {
        this.statistics = statistics;
    }

    public boolean isPlanner() {
        return planner;
    }

    public void setPlanner(boolean planner) {
        this.planner = planner;
    }
}
