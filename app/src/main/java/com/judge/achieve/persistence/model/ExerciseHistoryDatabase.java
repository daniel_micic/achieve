package com.judge.achieve.persistence.model;

import com.judge.achieve.model.ExerciseHistory;

import org.joda.time.DateTime;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 05.02.2017.
 */

public class ExerciseHistoryDatabase extends RealmObject {

    @PrimaryKey
    long id;
    long date;
    int countForDay;
    float points;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getCountForDay() {
        return countForDay;
    }

    public void setCountForDay(int countForDay) {
        this.countForDay = countForDay;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public static ExerciseHistoryDatabase mapToDatabase(ExerciseHistory eh) {
        ExerciseHistoryDatabase ehd = new ExerciseHistoryDatabase();
        ehd.setId(eh.getId());
        ehd.setDate(eh.getDate().getMillis());
        ehd.setPoints(eh.getPoints());
        ehd.setCountForDay(eh.getCountForDay());
        return ehd;
    }

    public ExerciseHistory createAppObject() {
        ExerciseHistory eh = new ExerciseHistory();
        eh.setId(getId());
        eh.setDate(new DateTime(getDate()));
        eh.setPoints(getPoints());
        eh.setCountForDay(getCountForDay());
        return eh;
    }

}
