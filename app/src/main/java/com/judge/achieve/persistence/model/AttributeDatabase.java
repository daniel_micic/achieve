package com.judge.achieve.persistence.model;

import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Skill;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 23.07.2016.
 */
public class AttributeDatabase extends RealmObject{

    @PrimaryKey
    int id;

    String title;
    float points;
    int difficulty;

    // for color
    int position;

    RealmList<SkillDatabase> skills;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public RealmList<SkillDatabase> getSkills() {
        return skills;
    }

    public void setSkills(RealmList<SkillDatabase> skills) {
        this.skills = skills;
    }

    public static AttributeDatabase mapToDatabase(Attribute c) {
        AttributeDatabase cd = new AttributeDatabase();
        cd.setId(c.getId());
        cd.setTitle(c.getTitle());
        cd.setPoints(c.getPoints());
        cd.setDifficulty(c.getDifficulty().getValue());
        cd.setSkills(new RealmList<SkillDatabase>());
        cd.setPosition(c.getPosition());

        for (Skill sc : c.getSkills()) {
            SkillDatabase sd = SkillDatabase.mapToDatabase(sc);
            cd.getSkills().add(sd);
        }

        return cd;
    }

    public Attribute createAppObject() {
        Attribute c = new Attribute();
        c.setId(getId());
        c.setTitle(getTitle());
        c.setPoints(getPoints());
        c.setDifficulty(Difficulty.fromValue(getDifficulty()));
        c.setSkills(new ArrayList<Skill>());
        c.setPosition(getPosition());

        for (SkillDatabase scd : getSkills()) {
            c.getSkills().add(scd.createAppObject());
        }

        return c;
    }

    public Attribute createAppObjectWithoutChildren() {
        Attribute c = new Attribute();
        c.setId(getId());
        c.setTitle(getTitle());
        c.setPoints(getPoints());
        c.setDifficulty(Difficulty.fromValue(getDifficulty()));
        c.setSkills(new ArrayList<Skill>());

        return c;
    }
}
