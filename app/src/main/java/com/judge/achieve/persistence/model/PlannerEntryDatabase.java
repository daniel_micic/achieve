package com.judge.achieve.persistence.model;

import com.judge.achieve.common.enums.Recurrence;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.utils.Logcat;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 15.02.2017.
 */

public class PlannerEntryDatabase extends RealmObject{

    @PrimaryKey
    private long id;
    private String title;
    // Start & recurrence
    Long startDate;
    int recurrence;

    // Until
    boolean forever;
    int limit;
    Long endDate;

    // Every X hours. days etc
    int every;

    // Weekly
    boolean monday;
    boolean tuesday;
    boolean wednesday;
    boolean thursday;
    boolean friday;
    boolean saturday;
    boolean sunday;

    //Monthly - Day of month vs Specific day of month (3rd Wednesday)
    boolean dayOfMonth = true;

    //      AttributeId      SkillId       ExerciseId
    RealmList<PlannerEntryIdsDatabase> ids = new RealmList<PlannerEntryIdsDatabase>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int recurrence) {
        this.recurrence = recurrence;
    }

    public boolean isForever() {
        return forever;
    }

    public void setForever(boolean forever) {
        this.forever = forever;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public int getEvery() {
        return every;
    }

    public void setEvery(int every) {
        this.every = every;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    public RealmList<PlannerEntryIdsDatabase> getIds() {
        return ids;
    }

    public void setIds(RealmList<PlannerEntryIdsDatabase> ids) {
        this.ids = ids;
    }

    public boolean isDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(boolean dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public static PlannerEntryDatabase mapToDatabase(PlannerEntry pe) {
        PlannerEntryDatabase ped = new PlannerEntryDatabase();
        ped.setId(pe.getId());
        ped.setTitle(pe.getTitle());
        ped.setStartDate(pe.getStartDate().getMillis());
        ped.setRecurrence(pe.getRecurrence().getValue());
        ped.setForever(pe.isForever());
        ped.setLimit(pe.getLimit());
        ped.setEndDate(pe.getEndDate().getMillis());
        ped.setEvery(pe.getEvery());

        ped.setMonday(pe.getWeekdays()[0]);
        ped.setTuesday(pe.getWeekdays()[1]);
        ped.setWednesday(pe.getWeekdays()[2]);
        ped.setThursday(pe.getWeekdays()[3]);
        ped.setFriday(pe.getWeekdays()[4]);
        ped.setSaturday(pe.getWeekdays()[5]);
        ped.setSunday(pe.getWeekdays()[6]);

        ped.setDayOfMonth(pe.isDayOfMonth());
        
        ped.setIds(new RealmList<>());

        for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attribute : pe.getExercises().entrySet()) {
            for (Map.Entry<Integer, List<Integer>> skill : attribute.getValue().entrySet()) {
                Logcat.d(skill.getKey() + " " + skill.getValue().size());
                for (Integer exercise : skill.getValue()) {
                    PlannerEntryIdsDatabase peid = new PlannerEntryIdsDatabase(attribute.getKey(), skill.getKey(), exercise);
                    ped.getIds().add(peid);
                }                
            }
        }

        return ped;
    }

    public PlannerEntry createAppObject() {
        PlannerEntry pe = new PlannerEntry();
        pe.setId(getId());
        pe.setTitle(getTitle());
        pe.setStartDate(new DateTime(getStartDate()));
        pe.setRecurrence(Recurrence.fromValue(getRecurrence()));
        pe.setForever(isForever());
        pe.setLimit(getLimit());
        pe.setEndDate(new DateTime(getEndDate()));
        pe.setEvery(getEvery());
        
        pe.getWeekdays()[0] = isMonday();
        pe.getWeekdays()[1] = isTuesday();
        pe.getWeekdays()[2] = isWednesday();
        pe.getWeekdays()[3] = isThursday();
        pe.getWeekdays()[4] = isFriday();
        pe.getWeekdays()[5] = isSaturday();
        pe.getWeekdays()[6] = isSunday();

        pe.setDayOfMonth(isDayOfMonth());

        pe.setExercises(new LinkedHashMap<Integer, LinkedHashMap<Integer, List<Integer>>>());

        for (PlannerEntryIdsDatabase peid : getIds()) {
            if (pe.getExercises().containsKey(peid.getAttributeId())) {
                if (pe.getExercises().get(peid.getAttributeId()).containsKey(peid.getSkillId())) {
                    pe.getExercises().get(peid.getAttributeId()).get(peid.getSkillId()).add(peid.getExerciseId());
                } else {
                    pe.getExercises().get(peid.getAttributeId()).put(peid.getSkillId(), new ArrayList<>());
                    pe.getExercises().get(peid.getAttributeId()).get(peid.getSkillId()).add(peid.getExerciseId());
                }
            } else {
                pe.getExercises().put(peid.getAttributeId(), new LinkedHashMap<>());
                pe.getExercises().get(peid.getAttributeId()).put(peid.getSkillId(), new ArrayList<>());
                pe.getExercises().get(peid.getAttributeId()).get(peid.getSkillId()).add(peid.getExerciseId());
            }
        }

        return pe;
    }


}
