package com.judge.achieve.persistence.query;

import com.judge.achieve.model.ExerciseHistory;
import com.judge.achieve.persistence.model.ExerciseDatabase;
import com.judge.achieve.persistence.model.ExerciseHistoryDatabase;
import com.judge.achieve.utils.Logcat;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel on 02.08.2016.
 */
public class ExerciseHistoryQuery {

    public static void copyToRealm(Realm realm, ExerciseHistory eh, int exerciseId) {
        realm.beginTransaction();

        ExerciseHistoryDatabase ehd = ExerciseHistoryDatabase.mapToDatabase(eh);

        ehd = realm.copyToRealm(ehd);

        ExerciseDatabase ed = realm.where(ExerciseDatabase.class).equalTo("id", exerciseId).findFirst();
        ed.getStatistics().add(ehd);

        realm.commitTransaction();
    }

    public static void edit(Realm realm, ExerciseHistory eh) {
        ExerciseHistoryDatabase ehd = realm.where(ExerciseHistoryDatabase.class).equalTo("id", eh.getId()).findFirst();

        realm.beginTransaction();
        ehd.setCountForDay(eh.getCountForDay());
        ehd.setPoints(eh.getPoints());
        realm.commitTransaction();
    }

    public static void delete(Realm realm, int id) {
        Logcat.d("delete category");
        ExerciseHistoryDatabase ed = realm.where(ExerciseHistoryDatabase.class).equalTo("id", id).findFirst();

        if (ed != null) {
            realm.executeTransaction(realm1 -> ed.deleteFromRealm());
        }

    }

    public static void deleteAll(Realm realm, RealmList<ExerciseHistoryDatabase> list) {
        realm.executeTransaction(realm1 -> list.deleteAllFromRealm());
    }

    /*public static Exercise getById(Realm realm, int id) {
        ExerciseDatabase ex  =realm.where(ExerciseDatabase.class).equalTo("id", id).findFirst();
        return ex.createAppObject();
    }

    public static List<Exercise> getAll(Realm realm) {
        List<ExerciseDatabase> listDb = realm.where(ExerciseDatabase.class).findAll();
        List<Exercise> list = new ArrayList<>();
        for (ExerciseDatabase exd : listDb) {
            list.add(exd.createAppObject());
        }
        return list;
    }

    public static int copyToRealm(Realm realm, Exercise exercise, int subcategoryId) {
        realm.beginTransaction();

        ExerciseDatabase exd = ExerciseDatabase.mapToDatabase(exercise);

        int primaryKey = getNewPrimaryKey(realm);

        exd.setId(primaryKey);

        realm.copyToRealm(exd);

        SkillDatabase scd = realm.where(SkillDatabase.class).equalTo("id", subcategoryId).findFirst();
        scd.getExercises().add(exd);

        realm.commitTransaction();

        return primaryKey;
    }


    public static void edit(Realm realm, Exercise exercise) {
        ExerciseDatabase exd = realm.where(ExerciseDatabase.class).equalTo("id", exercise.getId()).findFirst();

        realm.beginTransaction();
        exd.setTitle(exercise.getTitle());
        exd.setPoints(exercise.getPoints());
        exd.setCountForDay(exercise.getCountForDay());
        exd.setDifficulty(exercise.getDifficulty().getValue());
        exd.setScope(exercise.getScope().getValue());
        realm.commitTransaction();
    }

    public static void edit(Realm realm, Exercise exercise, int initialSubcategoryId, int newSubcategoryId) {
        ExerciseDatabase exd = realm.where(ExerciseDatabase.class).equalTo("id", exercise.getId()).findFirst();

        realm.beginTransaction();

        SkillDatabase scd = realm.where(SkillDatabase.class).equalTo("id", initialSubcategoryId).findFirst();
        int position = -1;
        for (ExerciseDatabase exd2 : scd.getExercises()) {
            if (exd2.getId() == exd.getId())  {
                position = scd.getExercises().indexOf(exd2);
            }
        }
        if (position >= 0) {
            scd.getExercises().remove(position);
        }

        scd = realm.where(SkillDatabase.class).equalTo("id", newSubcategoryId).findFirst();
        scd.getExercises().add(exd);

        exd.setTitle(exercise.getTitle());
        exd.setPoints(exercise.getPoints());
        exd.setCountForDay(exercise.getCountForDay());
        exd.setDifficulty(exercise.getDifficulty().getValue());
        realm.commitTransaction();
    }

    public static void delete(Realm realm, int id) {
        Logcat.d("delete category");
        realm.beginTransaction();
        ExerciseDatabase ex = realm.where(ExerciseDatabase.class).equalTo("id", id).findFirst();
        if (ex != null) {
            ex.deleteFromRealm();
            realm.commitTransaction();
        } else {
            realm.cancelTransaction();
        }

    }*/

    public static int getNewPrimaryKey(Realm realm) {
        Number maxId = realm.where(ExerciseHistoryDatabase.class).max("id");
        int primaryKeyValue = maxId != null ? maxId.intValue() + 1 : 1;
        return primaryKeyValue;
    }
}
