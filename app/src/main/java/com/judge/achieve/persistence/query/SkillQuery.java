package com.judge.achieve.persistence.query;

import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.model.Skill;
import com.judge.achieve.persistence.model.AttributeDatabase;
import com.judge.achieve.persistence.model.SkillDatabase;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel on 02.08.2016.
 */
public class SkillQuery {

    public static Skill getById(Realm realm, int id) {
        SkillDatabase scd  =realm.where(SkillDatabase.class).equalTo("id", id).findFirst();
        if (scd == null) {
            return null;
        } else {
            return scd.createAppObject();
        }
    }

    public static List<Skill> getAll(Realm realm) {
        List<SkillDatabase> listDb = realm.where(SkillDatabase.class).findAll();
        List<Skill> list = new ArrayList<>();
        for (SkillDatabase skillDatabase : listDb) {
            list.add(skillDatabase.createAppObject());
        }
        return list;
    }


    public static int copyToRealm(Realm realm, Skill skill, int categoryId) {
        realm.beginTransaction();
        SkillDatabase scd = SkillDatabase.mapToDatabase(skill);

        int primaryKey = getNewPrimaryKey(realm);

        scd.setId(primaryKey);

        scd = realm.copyToRealm(scd);

        AttributeDatabase cd = realm.where(AttributeDatabase.class).equalTo("id", categoryId).findFirst();
        cd.getSkills().add(scd);

        realm.commitTransaction();

        return primaryKey;
    }

    public static void edit(Realm realm, Skill skill) {
        SkillDatabase scd = realm.where(SkillDatabase.class).equalTo("id", skill.getId()).findFirst();

        realm.beginTransaction();
        scd.setTitle(skill.getTitle());
        scd.setPoints(skill.getPoints());
        realm.commitTransaction();
    }

    public static void delete(Realm realm, int id) {
        Logcat.d("delete category");

        SkillDatabase sd = realm.where(SkillDatabase.class).equalTo("id", id).findFirst();

        ExerciseQuery.deleteAll(realm, sd.getExercises());

        if (sd != null) {
            realm.executeTransaction(realm1 -> sd.deleteFromRealm());
        }

        for (PlannerEntry e : PlannerEntryQuery.getAll(realm)) {
            int totalSkills = 0;
            for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attr : e.getExercises().entrySet()) {
                Iterator<Map.Entry<Integer, List<Integer>>> iterator =  attr.getValue().entrySet().iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().getKey() == id) {
                        Logcat.d("removing from entry " + e.getId() + " skill " + id);
                        iterator.remove();
                    }
                }
                totalSkills += attr.getValue().size();
            }
            if (totalSkills == 0) {
                PlannerEntryQuery.delete(realm, e.getId());
            } else {
                PlannerEntryQuery.edit(realm, e);
            }
        }

    }

    public static void deleteAll(Realm realm, RealmList<SkillDatabase> list) {
        for (SkillDatabase sd : list) {
            ExerciseQuery.deleteAll(realm, sd.getExercises());
        }
        realm.executeTransaction(realm1 -> list.deleteAllFromRealm());
    }

    public static int getNewPrimaryKey(Realm realm) {
        Number maxId = realm.where(SkillDatabase.class).max("id");
        int primaryKeyValue = maxId != null ? maxId.intValue() + 1 : 1;
        return primaryKeyValue;
    }
}
