package com.judge.achieve.persistence.query;

import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.ExerciseHistory;
import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.persistence.model.ExerciseDatabase;
import com.judge.achieve.persistence.model.ExerciseHistoryDatabase;
import com.judge.achieve.persistence.model.SkillDatabase;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel on 02.08.2016.
 */
public class ExerciseQuery {

    public static Exercise getById(Realm realm, int id) {
        ExerciseDatabase ex  =realm.where(ExerciseDatabase.class).equalTo("id", id).findFirst();
        if (ex == null) {
            return null;
        } else {
            return ex.createAppObject();
        }
    }

    public static List<Exercise> getAll(Realm realm) {
        List<ExerciseDatabase> listDb = realm.where(ExerciseDatabase.class).findAll();
        List<Exercise> list = new ArrayList<>();
        for (ExerciseDatabase exd : listDb) {
            list.add(exd.createAppObject());
        }
        return list;
    }

    public static int copyToRealm(Realm realm, Exercise e, int subcategoryId) {
        realm.beginTransaction();

        ExerciseDatabase ed = ExerciseDatabase.mapToDatabase(e);

        int primaryKey = getNewPrimaryKey(realm);

        ed.setId(primaryKey);

        ed = realm.copyToRealm(ed);

        SkillDatabase scd = realm.where(SkillDatabase.class).equalTo("id", subcategoryId).findFirst();

        scd.getExercises().add(ed);

        realm.commitTransaction();

        return primaryKey;
    }


    public static void edit(Realm realm, Exercise e) {
        ExerciseDatabase ed = realm.where(ExerciseDatabase.class).equalTo("id", e.getId()).findFirst();

        realm.beginTransaction();
        ed.setTitle(e.getTitle());
        ed.setCountForDay(e.getCountForDay());
        ed.setDescription(e.getDescription());
        ed.setDifficulty(e.getDifficulty().getValue());
        ed.setScope(e.getScope().getValue());
        if ((ed.getStatistics() != null) && (e.getStatistics() != null)) {
            if (ed.getStatistics().size() != e.getStatistics().size()) {
                // adding newly created history object
                ed.getStatistics().add(ExerciseHistoryDatabase.mapToDatabase(e.getStatistics().get(e.getStatistics().size() - 1)));
            } else if (!ed.getStatistics().isEmpty()) {
                // updating already existing history object
                ExerciseHistoryDatabase ehd = ed.getStatistics().get(ed.getStatistics().size() - 1);
                ExerciseHistory eh = e.getStatistics().get(e.getStatistics().size() - 1);

                ehd.setPoints(eh.getPoints());
                ehd.setCountForDay(eh.getCountForDay());

            }
        }
        realm.commitTransaction();
    }

    public static void edit(Realm realm, Exercise e, int initialSubcategoryId, int newSubcategoryId) {
        ExerciseDatabase ed = realm.where(ExerciseDatabase.class).equalTo("id", e.getId()).findFirst();

        realm.beginTransaction();

        SkillDatabase scd = realm.where(SkillDatabase.class).equalTo("id", initialSubcategoryId).findFirst();
        int position = -1;
        for (ExerciseDatabase exd2 : scd.getExercises()) {
            if (exd2.getId() == ed.getId())  {
                position = scd.getExercises().indexOf(exd2);
            }
        }
        if (position >= 0) {
            scd.getExercises().remove(position);
        }

        scd = realm.where(SkillDatabase.class).equalTo("id", newSubcategoryId).findFirst();
        scd.getExercises().add(ed);

        ed.setTitle(e.getTitle());
        ed.setDescription(e.getDescription());
        ed.setCountForDay(e.getCountForDay());
        ed.setDifficulty(e.getDifficulty().getValue());
        ed.setScope(e.getScope().getValue());
        realm.commitTransaction();
    }

    public static void delete(Realm realm, int id) {
        Logcat.d("delete category");

        ExerciseDatabase ed = realm.where(ExerciseDatabase.class).equalTo("id", id).findFirst();

        ExerciseHistoryQuery.deleteAll(realm, ed.getStatistics());

        if (ed != null) {
            realm.executeTransaction(realm1 -> ed.deleteFromRealm());
        }

        for (PlannerEntry e : PlannerEntryQuery.getAll(realm)) {
            int totalExercises = 0;
            for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> attr : e.getExercises().entrySet()) {
                for (Map.Entry<Integer, List<Integer>> skill : attr.getValue().entrySet()) {
                    Iterator<Integer> iterator = skill.getValue().iterator();
                    while (iterator.hasNext()) {
                        if (iterator.next() == id) {
                            Logcat.d("removing from entry " + e.getId() + " exercise " + id);
                            iterator.remove();
                        }
                    }
                    totalExercises += skill.getValue().size();
                }
            }
            if (totalExercises == 0) {
                PlannerEntryQuery.delete(realm, e.getId());
            } else {
                PlannerEntryQuery.edit(realm, e);
            }
        }

    }

    public static void deleteAll(Realm realm, RealmList<ExerciseDatabase> list) {
        for (ExerciseDatabase ed : list) {
            ExerciseHistoryQuery.deleteAll(realm, ed.getStatistics());
        }
        realm.executeTransaction(realm1 -> list.deleteAllFromRealm());
    }

    public static int getNewPrimaryKey(Realm realm) {
        Number maxId = realm.where(ExerciseDatabase.class).max("id");
        int primaryKeyValue = maxId != null ? maxId.intValue() + 1 : 1;
        return primaryKeyValue;
    }
}
