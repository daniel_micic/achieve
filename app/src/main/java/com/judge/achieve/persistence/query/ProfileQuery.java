package com.judge.achieve.persistence.query;

import com.judge.achieve.model.Profile;
import com.judge.achieve.persistence.model.ProfileDatabase;
import com.judge.achieve.utils.Logcat;

import io.realm.Realm;

/**
 * Created by Daniel on 02.08.2016.
 */
public class ProfileQuery {

    public static void copyToRealm(Realm realm, Profile profile) {
        realm.beginTransaction();
        ProfileDatabase pd = realm.copyToRealm(ProfileDatabase.mapToDatabase(profile));
        if (pd  == null) {
            Logcat.d("its null shit ...");
        } else {
            Logcat.d("its not null " + pd.getName());
        }
        realm.commitTransaction();
    }

    public static Profile get(Realm realm) {
        ProfileDatabase pd = realm.where(ProfileDatabase.class).findFirst();

        if (pd != null) {
            return pd.createAppObject();
        } else {
            return null;
        }
    }

    public static void edit(Realm realm, Profile profile) {
        Logcat.d("edit profile");
        ProfileDatabase pd = realm.where(ProfileDatabase.class).findFirst();

        realm.beginTransaction();
        pd.setName(profile.getName());
        pd.setLevel(profile.getLevel());
        pd.setPoints(profile.getPoints());
        pd.setProfilePic(profile.getProfilePic());
        realm.commitTransaction();
    }
}
