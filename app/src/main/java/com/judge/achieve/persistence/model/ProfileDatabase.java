package com.judge.achieve.persistence.model;

import com.judge.achieve.model.Profile;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Daniel on 23.07.2016.
 */
public class ProfileDatabase extends RealmObject{

    String name;
    int level;
    float points;
    String profilePic;

    RealmList<AttributeDatabase> categories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /*public RealmList<AttributeDatabase> getCategories() {
        return categories;
    }

    public void setCategories(RealmList<AttributeDatabase> categories) {
        this.categories = categories;
    }*/

    public static ProfileDatabase mapToDatabase(Profile p) {
        ProfileDatabase pd = new ProfileDatabase();
        pd.setName(p.getName());
        pd.setLevel(p.getLevel());
        pd.setPoints(p.getPoints());
        pd.setProfilePic(p.getProfilePic());
        /*pd.setCategories(new RealmList<AttributeDatabase>());

        /*for (Attribute c : p.getCategories()) {
            pd.getCategories().add(AttributeDatabase.mapToDatabase(c));
        }*/

        return pd;
    }

    public Profile createAppObject() {
        Profile p = new Profile();
        p.setName(getName());
        p.setLevel(getLevel());
        p.setPoints(getPoints());
        p.setProfilePic(getProfilePic());
        /*p.setCategories(new ArrayList<Attribute>());

        for (AttributeDatabase cd : getCategories()) {
            p.getCategories().add(cd.createAppObject());
        }*/

        return p;
    }
}
