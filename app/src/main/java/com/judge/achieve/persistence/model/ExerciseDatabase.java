package com.judge.achieve.persistence.model;

import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.common.enums.Scope;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.ExerciseHistory;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 23.07.2016.
 */
public class ExerciseDatabase extends RealmObject{

    @PrimaryKey
    int id;

    String title;
    float points;
    int difficulty;
    int scope;
    int countForDay;
    String description;

    RealmList<ExerciseHistoryDatabase> statistics;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = scope;
    }

    public int getCountForDay() {
        return countForDay;
    }

    public void setCountForDay(int countForDay) {
        this.countForDay = countForDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<ExerciseHistoryDatabase> getStatistics() {
        return statistics;
    }

    public void setStatistics(RealmList<ExerciseHistoryDatabase> statistics) {
        this.statistics = statistics;
    }

    public static ExerciseDatabase mapToDatabase(Exercise e) {
        ExerciseDatabase ed = new ExerciseDatabase();
        ed.setId(e.getId());
        ed.setTitle(e.getTitle());
        ed.setDifficulty(e.getDifficulty().getValue());
        ed.setScope(e.getScope().getValue());
        ed.setCountForDay(e.getCountForDay());
        ed.setDescription(e.getDescription());
        ed.setStatistics(new RealmList<>());

        for (ExerciseHistory eh : e.getStatistics()) {
            ExerciseHistoryDatabase ehd = ExerciseHistoryDatabase.mapToDatabase(eh);
            ed.getStatistics().add(ehd);
        }

        return ed;
    }

    public Exercise createAppObject() {
        Exercise e = new Exercise();
        e.setId(getId());
        e.setTitle(getTitle());
        e.setDifficulty(Difficulty.fromValue(getDifficulty()));
        e.setScope(Scope.fromValue(getScope()));
        e.setCountForDay(getCountForDay());
        e.setDescription(getDescription());
        e.setStatistics(new ArrayList<>());

        for (ExerciseHistoryDatabase ehd : getStatistics()) {
            ExerciseHistory eh = ehd.createAppObject();
            e.getStatistics().add(eh);
        }

        return e;
    }

}
