package com.judge.achieve.persistence.query;

import com.judge.achieve.model.PlannerEntry;
import com.judge.achieve.persistence.model.PlannerEntryDatabase;
import com.judge.achieve.persistence.model.PlannerEntryIdsDatabase;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel on 02.08.2016.
 */
public class PlannerEntryQuery {

    public static PlannerEntry getById(Realm realm, long id) {
        PlannerEntryDatabase pe  = realm.where(PlannerEntryDatabase.class).equalTo("id", id).findFirst();
        return pe.createAppObject();
    }

    public static List<PlannerEntry> getAll(Realm realm) {
        List<PlannerEntryDatabase> listDb = realm.where(PlannerEntryDatabase.class).findAll();
        List<PlannerEntry> list = new ArrayList<>();
        for (PlannerEntryDatabase ped : listDb) {
            list.add(ped.createAppObject());
        }
        return list;
    }

    public static long copyToRealm(Realm realm, PlannerEntry pe) {
        realm.beginTransaction();

        PlannerEntryDatabase ed = PlannerEntryDatabase.mapToDatabase(pe);

        long id = getNewPeidPrimaryKey(realm);
        for (PlannerEntryIdsDatabase peid : ed.getIds()) {
            peid.setId(id++);
        }
        Logcat.d(" ");

        long primaryKey = getNewPrimaryKey(realm);

        ed.setId(primaryKey);

        realm.copyToRealm(ed);

        realm.commitTransaction();

        return primaryKey;
    }

    public static long edit(Realm realm, PlannerEntry e) {
        delete(realm, e.getId());

        return PlannerEntryQuery.copyToRealm(realm, e);
    }

    public static void delete(Realm realm, long id) {
        Logcat.d("delete category");

        PlannerEntryDatabase ed = realm.where(PlannerEntryDatabase.class).equalTo("id", id).findFirst();

        if (ed != null) {
            if (ed.getIds() != null && !ed.getIds().isEmpty());
                realm.executeTransaction(realm1 -> ed.getIds().deleteAllFromRealm());

            realm.executeTransaction(realm1 -> ed.deleteFromRealm());
        }

    }

    public static void deleteAll(Realm realm, RealmList<PlannerEntryDatabase> list) {
        realm.executeTransaction(realm1 -> list.deleteAllFromRealm());
    }

    public static long getNewPrimaryKey(Realm realm) {
        Number maxId = realm.where(PlannerEntryDatabase.class).max("id");
        int primaryKeyValue = maxId != null ? maxId.intValue() + 1 : 1;
        return primaryKeyValue;
    }

    public static long getNewPeidPrimaryKey(Realm realm) {
        Number maxId = realm.where(PlannerEntryIdsDatabase.class).max("id");
        long primaryKeyValue = maxId != null ? maxId.longValue() + 1L : 1L;
        return primaryKeyValue;
    }
}
