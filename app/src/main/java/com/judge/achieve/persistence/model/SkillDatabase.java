package com.judge.achieve.persistence.model;

import com.judge.achieve.model.Attribute;
import com.judge.achieve.model.Exercise;
import com.judge.achieve.model.Skill;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 23.07.2016.
 */
public class SkillDatabase extends RealmObject{

    @PrimaryKey
    int id;

    String title;
    float points;

    RealmList<ExerciseDatabase> exercises;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public RealmList<ExerciseDatabase> getExercises() {
        return exercises;
    }

    public void setExercises(RealmList<ExerciseDatabase> exercises) {
        this.exercises = exercises;
    }


    public static SkillDatabase mapToDatabase(Skill s) {
        SkillDatabase sd = new SkillDatabase();
        sd.setId(s.getId());
        sd.setTitle(s.getTitle());
        sd.setPoints(s.getPoints());
        sd.setExercises(new RealmList<>());
        
        for (Exercise e : s.getExercises()) {
            ExerciseDatabase ed = ExerciseDatabase.mapToDatabase(e);
            sd.getExercises().add(ed);
        }

        return sd;
    }

    public Skill createAppObject() {
        Skill sc = new Skill();
        sc.setId(getId());
        sc.setTitle(getTitle());
        sc.setPoints(getPoints());
        sc.setExercises(new ArrayList<>());

        for (ExerciseDatabase ed : getExercises()) {
            Exercise e = ed.createAppObject();
            sc.getExercises().add(e);
        }

        return sc;
    }

    public Skill createAppObjectWithoutChildren() {
        Skill sc = new Skill();
        sc.setId(getId());
        sc.setTitle(getTitle());
        sc.setPoints(getPoints());
        sc.setExercises(new ArrayList<>());

        return sc;
    }
}
