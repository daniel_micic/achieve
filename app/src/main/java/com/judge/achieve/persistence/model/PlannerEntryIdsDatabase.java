package com.judge.achieve.persistence.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel on 15.02.2017.
 */

public class PlannerEntryIdsDatabase extends RealmObject {

    @PrimaryKey
    long id;
    Integer AttributeId;
    Integer SkillId;
    Integer ExerciseId;

    public PlannerEntryIdsDatabase() {
    }

    public PlannerEntryIdsDatabase(Integer attributeId, Integer skillId, Integer exerciseId) {
        AttributeId = attributeId;
        SkillId = skillId;
        ExerciseId = exerciseId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getAttributeId() {
        return AttributeId;
    }

    public void setAttributeId(Integer attributeId) {
        AttributeId = attributeId;
    }

    public Integer getSkillId() {
        return SkillId;
    }

    public void setSkillId(Integer skillId) {
        SkillId = skillId;
    }

    public Integer getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(Integer exerciseId) {
        ExerciseId = exerciseId;
    }
}
