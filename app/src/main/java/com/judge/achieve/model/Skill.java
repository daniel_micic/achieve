package com.judge.achieve.model;

import com.judge.achieve.common.C;
import com.judge.achieve.utils.Logcat;

import java.util.ArrayList;

/**
 * Created by Daniel on 23.07.2016.
 */
public class Skill {

    int id;
    String title;
    float points;

    ArrayList<Exercise> exercises;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(ArrayList<Exercise> exercises) {
        this.exercises = exercises;
    }

    public boolean addPoints(float points) {
        int progressOld = getProgress();
        Logcat.d("old level " + getProgress() + " " + points );
        this.points += points;
        int progressNew = getProgress();
        Logcat.d("new level " + getProgress() + " " + points );
        if (progressNew > progressOld)
            return true;
        else
            return false;
    }

    public int getProgress() {
        return (int) (points / C.BASE_LEVEL_XP);
    }
}
