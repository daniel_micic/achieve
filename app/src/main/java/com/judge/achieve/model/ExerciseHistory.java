package com.judge.achieve.model;

import org.joda.time.DateTime;

/**
 * Created by Daniel on 05.02.2017.
 */

public class ExerciseHistory implements Comparable<ExerciseHistory>{

    long id;
    DateTime date;
    int countForDay;
    float points;


    // TODO asi pridat ziskane pointy pre pripad neskorsej editacie Exercise - zmena Scope alebo Diff

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public int getCountForDay() {
        return countForDay;
    }

    public void setCountForDay(int countForDay) {
        this.countForDay = countForDay;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    @Override
    public int compareTo(ExerciseHistory eh) {
        return getDate().compareTo(eh.getDate());
    }
}
