package com.judge.achieve.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.judge.achieve.common.C;
import com.judge.achieve.common.enums.Difficulty;

/**
 * Created by Daniel on 05.01.2017.
 */

public class Goal implements Parcelable {

    int id;

    String title;
    String description;

    Difficulty difficulty;

    boolean finished = false;

    int attributeId = C.UNDEFINED;
    int skillId = C.UNDEFINED;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(int attributeId) {
        this.attributeId = attributeId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeInt(this.difficulty == null ? -1 : this.difficulty.ordinal());
        dest.writeByte(this.finished ? (byte) 1 : (byte) 0);
        dest.writeInt(this.attributeId);
        dest.writeInt(this.skillId);
    }

    public Goal() {
    }

    protected Goal(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        int tmpDifficulty = in.readInt();
        this.difficulty = tmpDifficulty == -1 ? null : Difficulty.values()[tmpDifficulty];
        this.finished = in.readByte() != 0;
        this.attributeId = in.readInt();
        this.skillId = in.readInt();
    }

    public static final Parcelable.Creator<Goal> CREATOR = new Parcelable.Creator<Goal>() {
        @Override
        public Goal createFromParcel(Parcel source) {
            return new Goal(source);
        }

        @Override
        public Goal[] newArray(int size) {
            return new Goal[size];
        }
    };
}



