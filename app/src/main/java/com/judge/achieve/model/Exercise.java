package com.judge.achieve.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.judge.achieve.common.enums.Difficulty;
import com.judge.achieve.common.enums.Scope;

import java.util.ArrayList;

/**
 * Created by Daniel on 23.07.2016.
 */
public class Exercise implements Parcelable {

    int id;
    String title;
    Difficulty difficulty;
    Scope scope;
    int countForDay;
    String description;

    ArrayList<ExerciseHistory> statistics;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public int getCountForDay() {
        return countForDay;
    }

    public void setCountForDay(int countForDay) {
        this.countForDay = countForDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<ExerciseHistory> getStatistics() {
        return statistics;
    }

    public void setStatistics(ArrayList<ExerciseHistory> statistics) {
        this.statistics = statistics;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeInt(this.difficulty == null ? -1 : this.difficulty.ordinal());
        dest.writeInt(this.scope == null ? -1 : this.scope.ordinal());
        dest.writeInt(this.countForDay);
        dest.writeString(this.description);
    }

    public Exercise() {
    }

    protected Exercise(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        int tmpDifficulty = in.readInt();
        this.difficulty = tmpDifficulty == -1 ? null : Difficulty.values()[tmpDifficulty];
        int tmpScope = in.readInt();
        this.scope = tmpScope == -1 ? null : Scope.values()[tmpScope];
        this.countForDay = in.readInt();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<Exercise> CREATOR = new Parcelable.Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel source) {
            return new Exercise(source);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };

    public float getPointsForExercise() {
        float x;
        switch(getDifficulty()) {
            case EASY:
                x = 1f;
                break;
            default:
            case NORMAL:
                x = 2f;
                break;
            case HARD:
                x = 4f;
                break;
        }

        switch (getScope()) {
            case SMALL:
                x *= 1f;
                break;
            default:
            case MEDIUM:
                x *= 5f;
                break;
            case LARGE:
                x *= 10f;
                break;
        }

        return x;
    }
}
