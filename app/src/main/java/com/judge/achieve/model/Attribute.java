package com.judge.achieve.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.judge.achieve.common.enums.Difficulty;

import java.util.ArrayList;

/**
 * Created by Daniel on 23.07.2016.
 */
public class Attribute implements Parcelable {

    int id;
    String title;
    float points;
    Difficulty difficulty;

    //for color
    int position;

    ArrayList<Skill> skills;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public void addPoints(float points) {
        this.points += points;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getProgress() {
        int progress = 0;
        for (Skill skill : getSkills()) {
            progress += skill.getProgress();
        }
        return (int)points / 50;
        //return progress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeFloat(this.points);
        dest.writeInt(this.difficulty == null ? -1 : this.difficulty.ordinal());
    }

    public Attribute() {
    }

    protected Attribute(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.points = in.readFloat();
        int tmpDifficulty = in.readInt();
        this.difficulty = tmpDifficulty == -1 ? null : Difficulty.values()[tmpDifficulty];
    }

    public static final Parcelable.Creator<Attribute> CREATOR = new Parcelable.Creator<Attribute>() {
        @Override
        public Attribute createFromParcel(Parcel source) {
            return new Attribute(source);
        }

        @Override
        public Attribute[] newArray(int size) {
            return new Attribute[size];
        }
    };
}
