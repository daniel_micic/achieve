package com.judge.achieve.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.judge.achieve.common.C;
import com.judge.achieve.utils.Logcat;

/**
 * Created by Daniel on 23.07.2016.
 */
public class Profile implements Parcelable {

    String name;
    int level;
    float points;
    String profilePic;

    //ArrayList<Attribute> categories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /*public ArrayList<Attribute> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Attribute> categories) {
        this.categories = categories;
    }*/

    public float getNextLevelPoints() {
        return (float) (C.BASE_LEVEL_XP * Math.pow((double)(level), C.LEVELING_FACTOR));
    }

    // calculate level progress
    public float getProgress() {
        return points / getNextLevelPoints();
    }

    // calculate level progress
    public int getProgressInPercentage() {
        Logcat.d(getNextLevelPoints());
        return (int)(points / getNextLevelPoints() * 100);
    }

    public boolean addPoints(float points) {
        setPoints(getPoints() + points);
        if (getPoints() >= getNextLevelPoints()) {
            setPoints(getPoints() - getNextLevelPoints());
            setLevel(getLevel() + 1);
            return true;
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.level);
        dest.writeFloat(this.points);
        dest.writeString(this.profilePic);
    }

    public Profile() {
    }

    protected Profile(Parcel in) {
        this.name = in.readString();
        this.level = in.readInt();
        this.points = in.readFloat();
        this.profilePic = in.readString();
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
