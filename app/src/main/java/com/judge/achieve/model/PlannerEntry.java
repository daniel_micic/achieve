package com.judge.achieve.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import com.judge.achieve.common.enums.Recurrence;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Daniel on 15.02.2017.
 */

public class PlannerEntry implements Comparable<PlannerEntry>,Parcelable {

    long id;
    String title = "";
    // Start & recurrence
    DateTime startDate = DateTime.now();
    Recurrence recurrence = Recurrence.NEVER;

    // Until
    boolean forever = false;
    int limit = -1;
    DateTime endDate = new DateTime().withDate(1970,1,1);

    // Every X hours. days etc
    int every = 1;

    // Weekly
    boolean[] weekdays = new boolean[7];

    //Monthly - Day of month vs Specific day of month (3rd Wednesday)
    boolean dayOfMonth = true;

    //            AttributeId            SkillId       ExerciseId
    LinkedHashMap<Integer, LinkedHashMap<Integer, List<Integer>>> exercises;

    public PlannerEntry() {
        Arrays.fill(weekdays, Boolean.FALSE);
    }

    public PlannerEntry(PlannerEntry e, DateTime startDate) {
        this.id = e.getId();
        this.startDate = startDate;
        this.recurrence = e.getRecurrence();
        this.forever = e.isForever();
        this.limit = e.getLimit();
        this.endDate = e.getEndDate();
        this.every = e.getEvery();
        this.weekdays = e.getWeekdays();
        this.dayOfMonth = e.isDayOfMonth();
        this.exercises = e.getExercises();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public Recurrence getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }

    public boolean isForever() {
        return forever;
    }

    public void setForever(boolean forever) {
        this.forever = forever;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public int getEvery() {
        return every;
    }

    public void setEvery(int every) {
        this.every = every;
    }

    public boolean[] getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(boolean[] weekdays) {
        this.weekdays = weekdays;
    }

    public boolean isDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(boolean dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public LinkedHashMap<Integer, LinkedHashMap<Integer, List<Integer>>> getExercises() {
        return exercises;
    }

    public void setExercises(LinkedHashMap<Integer, LinkedHashMap<Integer, List<Integer>>> exercises) {
        this.exercises = exercises;
    }

    public HashMap<Integer, List<Integer>> getSkillsForAttribute(Integer attributeId) {
        return exercises.get(attributeId);
    }

    public List<Pair<Integer, Pair<Integer, List<Integer>>>> getEntryForList() {
        // RecyclerView needs list not hashmap
        // Exercises in Entry RecyclerView are grouped by Skills - ie. each skill has its own item in EntryGroupAdapter
        List<Pair<Integer, Pair<Integer, List<Integer>>>> attributes = new ArrayList();
        for (Map.Entry<Integer, LinkedHashMap<Integer, List<Integer>>> entry : exercises.entrySet()) {
            for (Map.Entry<Integer, List<Integer>> entryInner : entry.getValue().entrySet()) {
                attributes.add(new Pair<>(entry.getKey(), new Pair<>(entryInner.getKey(), entryInner.getValue())));
            }
        }
        return attributes;

    }

    @Override
    public int compareTo(PlannerEntry entry) {
        if (getStartDate().isEqual(entry.getStartDate()))
            return 0;
        else if (getStartDate().isAfter(entry.getStartDate()))
            return 1;
        else
            return -1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.startDate.getMillis());
        dest.writeInt(this.recurrence == null ? -1 : this.recurrence.ordinal());
        dest.writeByte(this.forever ? (byte) 1 : (byte) 0);
        dest.writeInt(this.limit);
        dest.writeSerializable(this.endDate);
        dest.writeInt(this.every);
        dest.writeBooleanArray(this.weekdays);
        dest.writeByte(this.dayOfMonth ? (byte) 1 : (byte) 0);
        dest.writeSerializable(this.exercises);
    }

    protected PlannerEntry(Parcel in) {
        this.id = in.readLong();
        this.startDate = new DateTime(in.readLong());
        int tmpRecurrence = in.readInt();
        this.recurrence = tmpRecurrence == -1 ? null : Recurrence.values()[tmpRecurrence];
        this.forever = in.readByte() != 0;
        this.limit = in.readInt();
        this.endDate = (DateTime) in.readSerializable();
        this.every = in.readInt();
        this.weekdays = in.createBooleanArray();
        this.dayOfMonth = in.readByte() != 0;
        this.exercises = (LinkedHashMap<Integer, LinkedHashMap<Integer, List<Integer>>>) in.readSerializable();
    }

    public static final Parcelable.Creator<PlannerEntry> CREATOR = new Parcelable.Creator<PlannerEntry>() {
        @Override
        public PlannerEntry createFromParcel(Parcel source) {
            return new PlannerEntry(source);
        }

        @Override
        public PlannerEntry[] newArray(int size) {
            return new PlannerEntry[size];
        }
    };
}
