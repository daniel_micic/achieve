package com.judge.achieve.common.enums;

/**
 * Created by Daniel on 01.10.2016.
 */

public enum Difficulty {
    EASY(1),
    NORMAL(2),
    HARD(3),
    UNDEFINED(4);

    private int value;

    Difficulty(int value) {
        this.value = value;
    }

    public static Difficulty fromValue(int value) {
        for (Difficulty e : values()) {
            if (e.value == value)
                return e;
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}

