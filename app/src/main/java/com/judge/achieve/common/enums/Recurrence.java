package com.judge.achieve.common.enums;

/**
 * Created by Daniel on 15.02.2017.
 */

public enum Recurrence {
    NEVER(0),
    HOURLY(1),
    DAILY(2),
    WEEKLY(3),
    MONTHLY(4),
    YEARLY(5);

    private int value;

    Recurrence(int value) {
        this.value = value;
    }

    public static Recurrence fromValue(int value) {
        for (Recurrence r : values()) {
            if (r.value == value)
                return r;
        }
        return null;
    }

    public int getValue() {
        return value;
    }

}
