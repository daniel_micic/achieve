package com.judge.achieve.common.enums;

/**
 * Created by Daniel on 23.05.2016.
 */
public enum ExerciseType {
    PRIMITIVE(100),
    SET(200),
    COUNT(300);

    private int value;

    ExerciseType(int value) {
        this.value = value;
    }

    public static ExerciseType fromValue(int value) {
        for (ExerciseType e : values()) {
            if (e.value == value)
                return e;
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}

