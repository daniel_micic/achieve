package com.judge.achieve.common;

import com.judge.achieve.R;

/**
 * Created by Daniel on 23.07.2016.
 */
public class C {

    //Integers
    public final static int RESULT_OK = 1;
    public final static int RESULT_FAIL = 2;
    public final static int RESULT_CREATED = 3;
    public final static int RESULT_EDITED = 4;

    //Modes
    public final static int MODE_EDIT = 0;
    public final static int MODE_CREATE = 1;
    public final static int MODE_DELETE = 2;
    public final static int MODE_NEW_CATEGORY = 3;

    public final static int UNDEFINED = -1;
    public final static int ALL = -2;

    public final static int PRIMITIVE = 101;
    public final static int SET = 102;
    public final static int COUNT = 103;

    public final static int BASE_LEVEL_XP = 50;
    public final static float LEVELING_FACTOR = 2f;

    public final static int[] CATEGORY_COLORS = new int[]{
            R.color.attribute1,
            R.color.attribute2,
            R.color.attribute3,
            R.color.attribute4
    };

    //Strngs
    public final static String PHYSICAL = "Physical";
    public final static String INTELLIGENCE = "Intelligence";
    public final static String SPIRIT = "Spirit";

    //SharedPreferences
    public final static String PREF_FIRST_RUN = "first_run";
    public final static String PREF_DATE = "date";
    public final static String PREF_ALARMS = "alarms";

    //Extras
    public final static String EXTRA_POSITION = "extra_position";
    public final static String EXTRA_MODE = "extra_mode";
    public final static String EXTRA_ATTRIBUTE_ID = "extra_att_id";
    public final static String EXTRA_SKILL_ID = "extra_skill_id";
    public final static String EXTRA_EXERCISE_ID = "extra_exer_id";
    public final static String EXTRA_GOAL_ID = "extra_goal_id";
    public final static String EXTRA_ENTRY_ID = "extra_ent_id";
    public final static String EXTRA_GOAL_ENTRY_ANIM = "extra_goal_anim";
    public final static String EXTRA_CALENDAR_DATE = "extra_cal_date";
    public final static String EXTRA_EXER_PICKER_TYPE = "extra_picker_type_id";;
    public final static String EXTRA_SELECTED_DAY = "extra_selected_day";;

    //Saved Instance
    public final static String SAVED_INSTANCE_MODE = "saved_mode";
    public final static String SAVED_INSTANCE_DIFFICULTY = "saved_diff";
    public final static String SAVED_INSTANCE_ID = "saved_id";

    public final static String SAVED_INSTANCE_PROFILE = "saved_prof";


    public final static String SAVED_INSTANCE_CATEGORY = "saved_cat";
    public final static String SAVED_INSTANCE_CATEGORY_ID = "saved_cat_id";

    public final static String SAVED_INSTANCE_SUBCATEGORY = "saved_subcat";
    public final static String SAVED_INSTANCE_SUBCATEGORY_ID = "saved_subcat_id";
    public final static String SAVED_INSTANCE_SUBCATEGORY_INITIAL_ID = "saved_subcat_initial_id";
    public final static String SAVED_INSTANCE_SUBCATEGORY_TITLE = "saved_subcat_title";
    public final static String SAVED_INSTANCE_IS_NEW_SUBCATEGORY = "saved_is_new_subcat";

    public final static String SAVED_INSTANCE_EXERCISE = "saved_exer";
    public final static String SAVED_INSTANCE_EXERCISE_ID = "saved_exer_id";

    public final static String SAVED_INSTANCE_GOAL_ID = "saved_goal_id";
    public final static String SAVED_INSTANCE_GOAL = "saved_goal";

    public final static String SAVED_INSTANCE_EBTRY = "saved_entry";
    public final static String SAVED_INSTANCE_ENTRY_ID = "saved_entry_id";

    public final static String iap_full_access = "full_access";
    public final static String iap_attributes = "unlimited_attributes";
    public final static String iap_exercises = "unlimited_exercises";
    public final static String iap_statistics = "statistics";
    public final static String iap_planner = "planner";






}

