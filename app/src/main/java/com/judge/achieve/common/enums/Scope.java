package com.judge.achieve.common.enums;

/**
 * Created by Daniel on 01.10.2016.
 */

public enum Scope {
    SMALL(1),
    MEDIUM(2),
    LARGE(3),
    UNDEFINED(4);

    private int value;

    Scope(int value) {
        this.value = value;
    }

    public static Scope fromValue(int value) {
        for (Scope e : values()) {
            if (e.value == value)
                return e;
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}

