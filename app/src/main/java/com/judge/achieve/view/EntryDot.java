package com.judge.achieve.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

import com.judge.achieve.R;
import com.judge.achieve.utils.ResourceUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daniel on 25.02.2017.
 */

public class EntryDot extends View {

    HashMap<Integer, Boolean> colors;

    public EntryDot(Context context, HashMap<Integer, Boolean> colors) {
        super(context);
        this.colors = colors;

    }

    @Override
    protected void onDraw(Canvas canvas) {

        RectF oval = new RectF(
                0,
                0,
                ResourceUtil.getDimension(R.dimen.default_size),
                ResourceUtil.getDimension(R.dimen.default_size)
        );

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        int startAngle;
        int sweepAngle;

        switch (colors.size()) {
            default:
            case 1:
                startAngle = 0;
                sweepAngle = 360;
                break;
            case 2:
                startAngle = 0;
                sweepAngle = 180;
            break;
            case 3:
                startAngle = 0;
                sweepAngle = 120;
                break;
            case 4:
                startAngle = 0;
                sweepAngle = 90;
                break;
        }

        for (Map.Entry<Integer, Boolean> entry : colors.entrySet()) {
            paint.setColor(ResourceUtil.getCategoryColor(entry.getKey()));
            canvas.drawArc(
                    oval,
                    startAngle - 90,
                    sweepAngle,
                    true,
                    paint);
            startAngle += sweepAngle;
            if (startAngle == 360) {
                break;
            }
        }

    }

}
