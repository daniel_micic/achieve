package com.judge.achieve.view;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Daniel on 05.10.2016.
 */

public class Toolbar extends android.support.v7.widget.Toolbar {

    private Integer offset = null;

    public Toolbar(Context context) {
        super(context);
    }

    public Toolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Toolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void offsetTopAndBottom(int offset) {
        super.offsetTopAndBottom(offset);
        if (this.offset == null)
            this.offset = offset;
    }

    @Override
    public int getTitleMarginTop() {
        return offset == null ? super.getTitleMarginTop() : -getTop() + offset + super.getTitleMarginTop();
    }

    @Override
    public int getTitleMarginBottom() {
        return offset == null ? super.getTitleMarginBottom() : getTop() - offset + super.getTitleMarginBottom();
    }

}